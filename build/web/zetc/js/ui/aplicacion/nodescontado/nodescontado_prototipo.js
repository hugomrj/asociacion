

function NoDescontado(){
    
   this.tipo = "nodescontado";   
   this.recurso = "nodescontados";   
   this.value = 0;
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
      
      
   this.titulosin = "No descontados"
   this.tituloplu = "No descontados"   
      
   this.campoid=  'id';
   this.tablacampos =  [ 'socio.socio', 'socio.nombre_apellido',  'monto' , 'interes',
   'suma', 'pagado', 'saldo'];
   this.etiquetas =  [ 'Socio', 'Nombre Apellido', 'Monto',  'Interes', 'Sub Total', 'Pagado', 'Saldo'   ];

   this.tablaformat =  ['N', 'C', 'N', 'N', 'N', 'N', 'N'];   
      
      
   this.tbody_id = "nodescontado-tb";
      
   this.botones_lista = [ this.new ] ;
   this.botones_form = "nodescontado-acciones";   
   
   this.parent = null;
   
   

}




NoDescontado.prototype.new = function( obj  ) {                



    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);        
    
        var fecha = new Date(); //Fecha actual
        var mes = fecha.getMonth()+1; //obteniendo mes
        var dia = fecha.getDate(); //obteniendo dia
        var ano = fecha.getFullYear(); //obteniendo año
        if(dia<10)
          dia='0'+dia; //agrega cero si el menor de 10
        if(mes<10)
          mes='0'+mes //agrega cero si el menor de 10
        var f =   ano+"-"+mes+"-"+dia;      
    
    
    
    var line_interes  = document.getElementById('line_interes');
    line_interes.style.display = "none";
        
    var line_suma  = document.getElementById('line_suma');
    line_suma.style.display = "none";
    
    var line_pagado  = document.getElementById('line_pagado');
    line_pagado.style.display = "none";
    
    var line_saldo  = document.getElementById('line_saldo');
    line_saldo.style.display = "none";
    
    
    
    
    var nodescontado_interes = document.getElementById('nodescontado_interes');   
    nodescontado_interes.disabled = true;


    var nodescontado_agno = document.getElementById('nodescontado_agno');
    nodescontado_agno.value = obj.agno;
    

    var nodescontado_mes = document.getElementById('nodescontado_mes');
    nodescontado_mes.value = obj.mes;
    

    // aca tengo que poner año y mes
    
    
};






NoDescontado.prototype.form_validar = function() {    

     
    var nodescontado_socio = document.getElementById('nodescontado_socio');        
    if (parseInt(NumQP(nodescontado_socio.value)) <= 0 )         
    {
        msg.error.mostrar("Falta seleccionar socio");    
        nodescontado_socio.focus();
        nodescontado_socio.select();                
        return false;
    }        
   
     
     
    var nodescontado_monto = document.getElementById('nodescontado_monto');        
    if (parseInt(NumQP(nodescontado_monto.value)) <= 0 )         
    {
        msg.error.mostrar("Falta monto del credito");    
        nodescontado_monto.focus();
        nodescontado_monto.select();                
        return false;
    }        
   
    
    return true;
};







NoDescontado.prototype.form_ini = function() {    
    

    var nodescontado_socio = document.getElementById('nodescontado_socio');            
    nodescontado_socio.onblur  = function() {                  
         nodescontado_socio.value = fmtNum(nodescontado_socio.value);      
         nodescontado_socio.value = NumQP(nodescontado_socio.value);      
    
        var  id = (nodescontado_socio.value );
        
        ajax.url = html.url.absolute()+'/api/socios/'+id;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();      

        if (ajax.state == 200)
        {            
            var oJson = JSON.parse(datajson) ;      
            
            document.getElementById('cliente_descripcion').innerHTML = 
                    oJson['nombre_apellido']; 
        }
        else{
            document.getElementById('cliente_descripcion').innerHTML = "";
        }   
    
     };      
    nodescontado_socio.onblur();          


    var ico_more_nodescontado_socio = document.getElementById('ico-more-nodescontado_socio');              
    ico_more_nodescontado_socio.onclick = function(  )
    {  
            var obj = new Socio();                 
            
            obj.acctionresul = function(id) {    
                nodescontado_socio.value = id; 
                nodescontado_socio.onblur(); 
                
                
                var nodescontado_monto = document.getElementById('nodescontado_monto')
                nodescontado_monto.focus();
                nodescontado_monto.select();
              
                
            };               
            modal.ancho = 900;
            busqueda.modal.objeto(obj);
    };       
    
    

    var nodescontado_monto = document.getElementById('nodescontado_monto');            
    nodescontado_monto.onblur  = function() {                  
         nodescontado_monto.value = fmtNum(nodescontado_monto.value);      
    };      
    nodescontado_monto.onblur();          
 
 
    var nodescontado_interes = document.getElementById('nodescontado_interes');            
    nodescontado_interes.onblur  = function() {                  
         nodescontado_interes.value = fmtNum(nodescontado_interes.value);      
    };      
    nodescontado_interes.onblur();          
       
    
    var nodescontado_suma = document.getElementById('nodescontado_suma');            
    nodescontado_suma.onblur  = function() {                  
         nodescontado_suma.value = fmtNum(nodescontado_suma.value);      
    };      
    nodescontado_suma.onblur();          
 
 
     var nodescontado_pagado = document.getElementById('nodescontado_pagado');            
    nodescontado_pagado.onblur  = function() {                  
         nodescontado_pagado.value = fmtNum(nodescontado_pagado.value);      
    };      
    nodescontado_pagado.onblur();          
 
 
     var nodescontado_saldo = document.getElementById('nodescontado_saldo');            
    nodescontado_saldo.onblur  = function() {                  
         nodescontado_saldo.value = fmtNum(nodescontado_saldo.value);      
    };      
    nodescontado_saldo.onblur();          
 
 
 
};








NoDescontado.prototype.form_edit = function( obj  ) {                


    var nodescontado_socio = document.getElementById('nodescontado_socio');            
    nodescontado_socio.disabled = true;


    var nodescontado_monto = document.getElementById('nodescontado_monto');            
    nodescontado_monto.disabled = true;


    var nodescontado_interes = document.getElementById('nodescontado_interes');            
    nodescontado_interes.disabled = true;
    
    
    var nodescontado_suma = document.getElementById('nodescontado_suma');            
    nodescontado_suma.disabled = true;
    
    
    
    var nodescontado_saldo = document.getElementById('nodescontado_saldo');            
    nodescontado_saldo.disabled = true;
    
    
    form.ocultar_foreign();
    
    
    
    
};





NoDescontado.prototype.form_del = function( obj  ) {                
 
};








NoDescontado.prototype.post_form_id = function( obj  ) {                
    
    //alert("post_form_id");
    
};








NoDescontado.prototype.pre_lista= function( obj  ) {                
  
        var selmes=document.getElementById('archivoxls_sel_mes');

        selmes.children[obj.mes -1].selected=true;
  
        document.getElementById('agno').value = new Date().getFullYear();  
 
};




NoDescontado.prototype.post_lista= function( obj  ) {                

        var selmes =  document.getElementById('archivoxls_sel_mes');
        selmes.onchange = function(){
    
            var mes = selmes.options[selmes.selectedIndex].value;                  
            obj.mes = mes;

                paginacion.pagina =1;
                reflex.cabdet_paginacion(obj, paginacion.pagina);    
            
        };
 
        //botonees de lista
        boton.objeto = ""+obj.tipo;
        
        document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new_file();
    
    
            var btn_nodescontado_nuevo = document.getElementById('btn_nodescontado_nuevo');
            btn_nodescontado_nuevo.addEventListener('click',
                function(event) {     
                    
                        obj.agno = document.getElementById('agno').value;
                        var mes = selmes.options[selmes.selectedIndex].value;                  
                        obj.mes = mes;
                        obj.new(obj);
                },
                false
            );   

    
    
    
            var btn_nodescontado_excel = document.getElementById('btn_nodescontado_excel');
            btn_nodescontado_excel.addEventListener('click',
                function(event) {     
                    
                    
                    var aa =  document.getElementById( 'agno' );                  
                    var mm = document.getElementById( 'archivoxls_sel_mes' );                  
                    
                    
                    ajax.url = html.url.absolute()+"/nodescontado/lista.xls"
                        +"?aa="+aa.value
                        +"&mm="+mm.value;
                    
                    
                    ajax.req.open("POST", ajax.url, true);
                    ajax.req.responseType = 'blob';
                    
                    ajax.req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    
                    ajax.req.onload = function (e) {
                        if (ajax.req.readyState === 4 && ajax.req.status === 200) {
                    
                            var contenidoEnBlob = ajax.req.response;
                            var link = document.createElement('a');
                            link.href = (window.URL || window.webkitURL).createObjectURL(contenidoEnBlob);
                            
                            link.download = "no_descontados_"+aa.value+"_"+mm.value+".xls";
                            
                            var clicEvent = new MouseEvent('click', {
                                'view': window,
                                'bubbles': true,
                                'cancelable': true
                            });
                    
                            link.dispatchEvent(clicEvent);
                            ajax.headers.getResponse();                    
                            
                        }
                        else 
                        {
                            if (ajax.req.status === 401){
                                    ajax.state = ajax.req.status;                   
                                    html.url.redirect(ajax.state);
                            }
                            else{
                                alert(" No es posible acceder al archivo");
                            }
                            
                        }
                    };                    
                    
                    ajax.headers.setRequest();                      
                    ajax.req.send();
                    
                    
                },
                false
            );   
    
    
    
    
            totales_pie(obj);
    
    
};






NoDescontado.prototype.main_list = function( obj  ) {        
    
    //busqueda.botoncompuesta = true;
        
        document.getElementById( obj.dom ).innerHTML =  "<article id=\"arti_cab\"></article><article id=\"arti_det\"></article>";
        reflex.interfase.cabdet( obj );
        
};






NoDescontado.prototype.get_api= function( obj  ) {                
 
        var restapi = "";
        
        var mes=  obj.mes;
    
        var agno = document.getElementById('agno').value ; 
    
        restapi = "nodescontados/mes/"+mes+"/agno/"+agno;
        return restapi;

};


