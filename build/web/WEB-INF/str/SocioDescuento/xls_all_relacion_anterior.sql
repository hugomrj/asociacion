
select * from 
( SELECT  
  socios.socio,    socios.cedula,    socios.nombre_apellido,    concepto_servicios.descripcion servicio_descripcion,  
  socios_descuentos.monto_descuento credito, 0 credito_saldo,    socios_descuentos.monto_descuento,    
  socios_descuentos.mes,    socios_descuentos.agno,    socios_descuentos.fecha , relacion_laboral  
FROM  
  aplicacion.socios,    aplicacion.socios_descuentos,    aplicacion.concepto_servicios 
WHERE  
  socios.socio = socios_descuentos.socio AND 
  concepto_servicios.servicio = socios_descuentos.servicio 
  and agno = v0 
  and mes =  v1 
  and  relacion_laboral = v2   

  union 

  select  
    socios.socio,   socios.cedula,   socios.nombre_apellido,   'Orden de compra' as servicio_descripcion,   
    sum(ordenes_compras.credito) credito, sum(ordenes_compras_pagos.credito_saldo) credito_saldo,       
    sum(ordenes_compras_pagos.monto_cuota) monto_descuento,  mes,  agno,  
   max(ordenes_compras_pagos.fecha) fecha , relacion_laboral  
FROM  
  aplicacion.ordenes_compras_pagos,   aplicacion.ordenes_compras,   aplicacion.socios  
WHERE  
  ordenes_compras_pagos.ordencompra = ordenes_compras.ordencompra AND  
  socios.socio = ordenes_compras.socio   
        and ordenes_compras_pagos.agno = v0     
        and ordenes_compras_pagos.mes = v1      
        and relacion_laboral = v2   
group by socios.socio,   socios.cedula,   socios.nombre_apellido,   servicio_descripcion, 
mes,  agno,  relacion_laboral  
) as t1 
order by   socio  
