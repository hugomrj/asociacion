select socio, cedula , nombre_apellido, ' ' servicio_descripcion, sum(credito) credito, 
sum(credito_saldo) credito_saldo, sum(monto_descuento) monto_descuento , 
mes, agno, relacion_laboral, direccion_sueldo  
from   
( SELECT  
  socios.socio,    socios.cedula,    socios.nombre_apellido,    concepto_servicios.descripcion servicio_descripcion,  
  socios_descuentos.monto_descuento credito, 0 credito_saldo,    socios_descuentos.monto_descuento,    
  socios_descuentos.mes,    socios_descuentos.agno,    socios_descuentos.fecha , relacion_laboral, 
  direccion_sueldo  
FROM  
  aplicacion.socios,    aplicacion.socios_descuentos,    aplicacion.concepto_servicios  
WHERE  
  socios.socio = socios_descuentos.socio AND 
  concepto_servicios.servicio = socios_descuentos.servicio 
  and agno = v0 
  and mes =  v1 
  and  relacion_laboral = v2    
  and  direccion_sueldo = v3 
  union 
  select  
    socios.socio,   socios.cedula,   socios.nombre_apellido,   'Orden de compra' as servicio_descripcion,   
    sum(ordenes_compras.credito) credito, sum(ordenes_compras_pagos.credito_saldo) credito_saldo,       
    sum(ordenes_compras_pagos.monto_cuota) monto_descuento,  mes,  agno,  
   max(ordenes_compras_pagos.fecha) fecha , relacion_laboral, direccion_sueldo  
FROM  
  aplicacion.ordenes_compras_pagos,   aplicacion.ordenes_compras,   aplicacion.socios  
WHERE   
  ordenes_compras_pagos.ordencompra = ordenes_compras.ordencompra AND  
  socios.socio = ordenes_compras.socio   
        and ordenes_compras_pagos.agno = v0     
        and ordenes_compras_pagos.mes = v1      
        and relacion_laboral = v2  
        and direccion_sueldo = v3   
    and (ordenes_compras_pagos.id_cierre is null or ordenes_compras_pagos.id_cierre > 1 ) 

group by socios.socio,   socios.cedula,   socios.nombre_apellido,   servicio_descripcion,  
mes,  agno,  relacion_laboral, direccion_sueldo 
union  
  select   
    socios.socio,   socios.cedula,   socios.nombre_apellido,   servicio.descripcion as servicio_descripcion,   
    sum(ordenes_compras_pagos_seguro.monto_seguro) credito, 0 credito_saldo,       
    sum(ordenes_compras_pagos_seguro.monto_seguro) monto_descuento,  
    ordenes_compras_pagos_seguro.mes,  ordenes_compras_pagos_seguro.agno,  
   max(ordenes_compras_pagos_seguro.fecha) fecha , relacion_laboral, direccion_sueldo  
FROM  
  aplicacion.socios, aplicacion.ordenes_compras_pagos_seguro,  
  ( 
  	SELECT descripcion  
	FROM aplicacion.concepto_servicios 
	where servicio = 3 
  ) as servicio  
WHERE  
  socios.socio = ordenes_compras_pagos_seguro.socio    
        and ordenes_compras_pagos_seguro.agno = v0       
        and ordenes_compras_pagos_seguro.mes = v1       
        and relacion_laboral = v2   
        and direccion_sueldo = v3   
group by socios.socio,   socios.cedula,   socios.nombre_apellido,   servicio_descripcion,  
ordenes_compras_pagos_seguro.mes,  ordenes_compras_pagos_seguro.agno,  relacion_laboral 
union  
SELECT   
  socios.socio,    socios.cedula,    socios.nombre_apellido,    concepto_servicios.descripcion servicio_descripcion,   
  (socios_descuentos_servicios.monto_descuento * servicios_temporal_descuentos.cuotas ) credito, 
  (socios_descuentos_servicios.monto_descuento * (servicios_temporal_descuentos.cuotas - servicios_temporal_descuentos.cuota)) credito_saldo,  
  socios_descuentos_servicios.monto_descuento,    
  socios_descuentos_servicios.mes,    socios_descuentos_servicios.agno,    socios_descuentos_servicios.fecha , 
  relacion_laboral, direccion_sueldo  
FROM  
  aplicacion.socios,    aplicacion.socios_descuentos_servicios,    aplicacion.concepto_servicios, 
  aplicacion.servicios_temporal_descuentos  
WHERE   
  socios.socio = socios_descuentos_servicios.socio AND 
  concepto_servicios.servicio = socios_descuentos_servicios.servicio and 
  servicios_temporal_descuentos.id = socios_descuentos_servicios.id_cab 
  and socios_descuentos_servicios.agno = v0  
  and socios_descuentos_servicios.mes =  v1  
  and  relacion_laboral = v2 
  and  direccion_sueldo = v3  
) as t1 
group by socio, cedula , nombre_apellido, mes, agno, relacion_laboral, direccion_sueldo  
order by   cedula  

