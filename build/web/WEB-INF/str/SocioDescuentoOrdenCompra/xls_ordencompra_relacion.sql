SELECT  
    socios.socio,   socios.cedula,   socios.nombre_apellido,   'Orden de compra' as servicio_descripcion,   
    sum(ordenes_compras.credito) credito,   
    sum(ordenes_compras_pagos.credito_saldo) credito_saldo,       
    sum(ordenes_compras_pagos.monto_cuota) monto_descuento, 
     mes,  agno,  
   max(ordenes_compras_pagos.fecha) fecha , relacion_laboral  
FROM  
  aplicacion.ordenes_compras_pagos,   aplicacion.ordenes_compras,   aplicacion.socios  
WHERE  
  ordenes_compras_pagos.ordencompra = ordenes_compras.ordencompra AND  
  socios.socio = ordenes_compras.socio  
 
        and ordenes_compras_pagos.agno = v0     
        and ordenes_compras_pagos.mes = v1      
        and relacion_laboral = v2   

group by socios.socio,   socios.cedula,   socios.nombre_apellido,   servicio_descripcion, 
mes,  agno,  relacion_laboral  
  
order by fecha desc  , socio, credito 

