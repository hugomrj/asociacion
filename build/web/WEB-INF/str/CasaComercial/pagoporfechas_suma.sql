
SELECT  
sum(credito) suma_credito, sum(monto_cuota) suma_pagado,  
sum(credito_saldo) suma_saldo  
from ( 
SELECT   
ordenes_compras_pagos.id, ordenes_compras_pagos.fecha, socios.socio,   
socios.nombre_apellido, ordenes_compras.credito,  
ordenes_compras.cantidad_cuotas,  
ordenes_compras_pagos.numero_cuota, ordenes_compras_pagos.monto_cuota,    
ordenes_compras_pagos.credito_saldo, ordenes_compras.ordencompra          
from aplicacion.ordenes_compras inner join aplicacion.ordenes_compras_pagos  
on (ordenes_compras.ordencompra = ordenes_compras_pagos.ordencompra )  
inner join aplicacion.casas_comerciales  
on (ordenes_compras.casacomercial = casas_comerciales.casacomercial)  
inner join aplicacion.socios   
on (socios.socio = ordenes_compras.socio)  
where ordenes_compras_pagos.fecha between 'v0' and 'v1'  
and ordenes_compras.casacomercial  = v2  
order by ordenes_compras_pagos.fecha   
) as t  







