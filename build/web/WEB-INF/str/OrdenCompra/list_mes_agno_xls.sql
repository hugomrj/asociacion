


SELECT 
ordencompra, fecha, socios.socio, cedula, nombre_apellido, nombre,  
credito, cantidad_cuotas, monto_cuota, cuotas_pagadas, credito_saldo 

FROM  
  aplicacion.ordenes_compras,  
  aplicacion.socios,  
  aplicacion.casas_comerciales 
WHERE 
  ordenes_compras.socio = socios.socio AND 
  casas_comerciales.casacomercial = ordenes_compras.casacomercial 
  and extract(month from fecha) = v0       
and extract(year from fecha) = v1    

order by fecha desc, socios.socio  



