

function TipoDescuento(){
    
   this.tipo = "tipodescuento";   
   this.recurso = "tiposdecuentos";   
   this.value = 0;
   this.from_descrip = "descripcion";
   this.json_descrip = "descripcion";
   
   this.dom="";
   this.carpeta=  "/configuracion";   
      
   
   
   this.titulosin = "Tipo Descuento"
   this.tituloplu = "Tipos Descuentos"   
      
   this.campoid=  'tipo_descuento';
   this.tablacampos =  ['tipo_descuento', 'descripcion'];
   this.etiquetas =  ['Tipo descuento', 'Descripcion'];
   
   this.tbody_id = "tipodescuento-tb";
      
   this.botones_lista = [ this.new ] ;
   this.botones_form = "tipodescuento-acciones";   
   
   this.parent = null;
   
}







TipoDescuento.prototype.new = function( obj  ) {                

    reflex.form(obj);
    reflex.acciones.button_add(obj);          
    

    
};






TipoDescuento.prototype.form_validar = function() {    
    return true;
};




