

function OrdenCompraPago(){
    
   this.tipo = "ordencomprapago";   
   this.recurso = "ordenescompraspagos";   
   this.value = 0;
   
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
      
      
   this.titulosin = "Orden Compra Pago"
   this.tituloplu = "Ordenes Compras Pagos"   
      
   this.campoid=  'id';
   this.tablacampos =  [  'numero_cuota', 'fecha',  'monto_cuota' , 'credito_saldo' ];
   this.etiquetas =  [ 'Nro Cuota', 'Fecha',  'Monto Cuota', 'Credito Saldo'  ];


   this.tablaformat =  ['N', 'D','N', 'N'  ];   
      
      
   this.tbody_id = "ordencomprapago-tb";
      
   //this.botones_lista = [ this.new ] ;
   this.botones_form = "ordencompracompra-acciones";   
   

}







OrdenCompraPago.prototype.form_validar = function() {    
                  
         
    var ordencomprapago_mes = document.getElementById('ordencomprapago_mes');        
    
    if (!( parseInt(NumQP(ordencomprapago_mes.value)) > 0
            &&  parseInt(NumQP(ordencomprapago_mes.value)) < 13  ))    {
    
        msg.error.mostrar("El mes de mes debe estar entre 1 y 12");    
        ordencomprapago_mes.focus();
        ordencomprapago_mes.select();                
        return false;
    
    }        
      
    
    
    return true;
};





