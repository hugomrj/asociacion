--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.22
-- Dumped by pg_dump version 9.5.22

-- Started on 2020-10-09 22:42:34 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 28764)
-- Name: aplicacion; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA aplicacion;


ALTER SCHEMA aplicacion OWNER TO postgres;

--
-- TOC entry 8 (class 2615 OID 28765)
-- Name: sistema; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sistema;


ALTER SCHEMA sistema OWNER TO postgres;

--
-- TOC entry 1 (class 3079 OID 12437)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2420 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 222 (class 1255 OID 28766)
-- Name: insertar_socio_descuento_fijo(); Type: FUNCTION; Schema: aplicacion; Owner: postgres
--

CREATE FUNCTION aplicacion.insertar_socio_descuento_fijo() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE         


BEGIN


	INSERT INTO aplicacion.socios_descuentos(
		    socio, fecha, servicio, monto_descuento, mes, agno)
	SELECT socio, NEW.fecha, NEW.servicio, NEW.monto, NEW.mes, NEW.agno
	  FROM aplicacion.socios;
		


    

   RETURN NEW;
 
END;
$$;


ALTER FUNCTION aplicacion.insertar_socio_descuento_fijo() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 183 (class 1259 OID 28767)
-- Name: casas_comerciales; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.casas_comerciales (
    casacomercial integer NOT NULL,
    nombre character varying,
    direccion character varying,
    telefono character varying,
    email character varying
);


ALTER TABLE aplicacion.casas_comerciales OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 28773)
-- Name: casas_comerciales_casacomercial_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.casas_comerciales_casacomercial_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.casas_comerciales_casacomercial_seq OWNER TO postgres;

--
-- TOC entry 2421 (class 0 OID 0)
-- Dependencies: 184
-- Name: casas_comerciales_casacomercial_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.casas_comerciales_casacomercial_seq OWNED BY aplicacion.casas_comerciales.casacomercial;


--
-- TOC entry 185 (class 1259 OID 28775)
-- Name: cierre_mensual; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.cierre_mensual (
    id integer NOT NULL,
    fecha date,
    mes integer,
    agno integer
);


ALTER TABLE aplicacion.cierre_mensual OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 28778)
-- Name: cierre_mensual_id_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.cierre_mensual_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.cierre_mensual_id_seq OWNER TO postgres;

--
-- TOC entry 2422 (class 0 OID 0)
-- Dependencies: 186
-- Name: cierre_mensual_id_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.cierre_mensual_id_seq OWNED BY aplicacion.cierre_mensual.id;


--
-- TOC entry 187 (class 1259 OID 28780)
-- Name: concepto_servicios; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.concepto_servicios (
    servicio integer NOT NULL,
    descripcion character varying,
    tipo_descuento integer,
    monto_fijo bigint,
    cantidad integer,
    actual integer
);


ALTER TABLE aplicacion.concepto_servicios OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 28786)
-- Name: concepto_servicios_servicio_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.concepto_servicios_servicio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.concepto_servicios_servicio_seq OWNER TO postgres;

--
-- TOC entry 2423 (class 0 OID 0)
-- Dependencies: 188
-- Name: concepto_servicios_servicio_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.concepto_servicios_servicio_seq OWNED BY aplicacion.concepto_servicios.servicio;


--
-- TOC entry 189 (class 1259 OID 28788)
-- Name: ordenes_compras; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.ordenes_compras (
    ordencompra integer NOT NULL,
    casacomercial integer NOT NULL,
    socio integer NOT NULL,
    credito bigint,
    cantidad_cuotas integer NOT NULL,
    monto_cuota bigint NOT NULL,
    cuotas_pagadas integer DEFAULT 0 NOT NULL,
    credito_saldo bigint,
    fecha date,
    user_add character varying
);


ALTER TABLE aplicacion.ordenes_compras OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 28795)
-- Name: ordenes_compras_ordencompra_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.ordenes_compras_ordencompra_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.ordenes_compras_ordencompra_seq OWNER TO postgres;

--
-- TOC entry 2424 (class 0 OID 0)
-- Dependencies: 190
-- Name: ordenes_compras_ordencompra_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.ordenes_compras_ordencompra_seq OWNED BY aplicacion.ordenes_compras.ordencompra;


--
-- TOC entry 191 (class 1259 OID 28797)
-- Name: ordenes_compras_pagos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.ordenes_compras_pagos (
    id integer NOT NULL,
    ordencompra integer,
    fecha date,
    numero_cuota integer,
    monto_cuota bigint,
    credito_saldo bigint,
    mes integer,
    agno integer,
    id_cierre integer
);


ALTER TABLE aplicacion.ordenes_compras_pagos OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 28800)
-- Name: ordenes_compras_pagos_id_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.ordenes_compras_pagos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.ordenes_compras_pagos_id_seq OWNER TO postgres;

--
-- TOC entry 2425 (class 0 OID 0)
-- Dependencies: 192
-- Name: ordenes_compras_pagos_id_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.ordenes_compras_pagos_id_seq OWNED BY aplicacion.ordenes_compras_pagos.id;


--
-- TOC entry 193 (class 1259 OID 28802)
-- Name: ordenes_compras_pagos_seguro; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.ordenes_compras_pagos_seguro (
    id integer NOT NULL,
    fecha date,
    monto_seguro integer,
    mes integer,
    agno integer,
    socio integer
);


ALTER TABLE aplicacion.ordenes_compras_pagos_seguro OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 28805)
-- Name: ordenes_compras_pagos_seguro_id_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.ordenes_compras_pagos_seguro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.ordenes_compras_pagos_seguro_id_seq OWNER TO postgres;

--
-- TOC entry 2426 (class 0 OID 0)
-- Dependencies: 194
-- Name: ordenes_compras_pagos_seguro_id_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.ordenes_compras_pagos_seguro_id_seq OWNED BY aplicacion.ordenes_compras_pagos_seguro.id;


--
-- TOC entry 195 (class 1259 OID 28807)
-- Name: relacion_laboral; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.relacion_laboral (
    relacion_laboral integer NOT NULL,
    descripcion character varying
);


ALTER TABLE aplicacion.relacion_laboral OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 28813)
-- Name: servicios_fijos_descuentos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.servicios_fijos_descuentos (
    id integer NOT NULL,
    servicio integer,
    fecha date,
    mes integer,
    agno integer,
    monto bigint DEFAULT 0
);


ALTER TABLE aplicacion.servicios_fijos_descuentos OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 28817)
-- Name: servicio_fijo_proceso_id_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.servicio_fijo_proceso_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.servicio_fijo_proceso_id_seq OWNER TO postgres;

--
-- TOC entry 2427 (class 0 OID 0)
-- Dependencies: 197
-- Name: servicio_fijo_proceso_id_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.servicio_fijo_proceso_id_seq OWNED BY aplicacion.servicios_fijos_descuentos.id;


--
-- TOC entry 198 (class 1259 OID 28819)
-- Name: servicios_temporal_descuentos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.servicios_temporal_descuentos (
    id integer NOT NULL,
    servicio integer,
    fecha date,
    mes integer,
    agno integer,
    cuotas integer,
    cuota integer,
    monto bigint DEFAULT 0
);


ALTER TABLE aplicacion.servicios_temporal_descuentos OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 28823)
-- Name: servicios_temporal_descuentos_id_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.servicios_temporal_descuentos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.servicios_temporal_descuentos_id_seq OWNER TO postgres;

--
-- TOC entry 2428 (class 0 OID 0)
-- Dependencies: 199
-- Name: servicios_temporal_descuentos_id_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.servicios_temporal_descuentos_id_seq OWNED BY aplicacion.servicios_temporal_descuentos.id;


--
-- TOC entry 200 (class 1259 OID 28825)
-- Name: socios; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.socios (
    socio integer NOT NULL,
    cedula integer,
    nombre_apellido character varying,
    telefono character varying,
    celular character varying,
    direccion_particular character varying,
    direccion_laboral character varying,
    dependencia character varying,
    relacion_laboral integer,
    ingreso_funcionario date,
    estado integer DEFAULT 0
);


ALTER TABLE aplicacion.socios OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 28832)
-- Name: socios_descuentos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.socios_descuentos (
    id integer NOT NULL,
    socio integer,
    fecha date,
    servicio integer,
    monto_descuento bigint,
    mes integer,
    agno integer
);


ALTER TABLE aplicacion.socios_descuentos OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 28835)
-- Name: socios_descuentos_id_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.socios_descuentos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.socios_descuentos_id_seq OWNER TO postgres;

--
-- TOC entry 2429 (class 0 OID 0)
-- Dependencies: 202
-- Name: socios_descuentos_id_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.socios_descuentos_id_seq OWNED BY aplicacion.socios_descuentos.id;


--
-- TOC entry 203 (class 1259 OID 28837)
-- Name: socios_descuentos_servicios; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.socios_descuentos_servicios (
    id integer NOT NULL,
    socio integer,
    fecha date,
    servicio integer,
    monto_descuento bigint,
    mes integer,
    agno integer,
    id_cab integer
);


ALTER TABLE aplicacion.socios_descuentos_servicios OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 28840)
-- Name: socios_descuentos_servicios_id_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.socios_descuentos_servicios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.socios_descuentos_servicios_id_seq OWNER TO postgres;

--
-- TOC entry 2430 (class 0 OID 0)
-- Dependencies: 204
-- Name: socios_descuentos_servicios_id_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.socios_descuentos_servicios_id_seq OWNED BY aplicacion.socios_descuentos_servicios.id;


--
-- TOC entry 205 (class 1259 OID 28842)
-- Name: socios_socio_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.socios_socio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.socios_socio_seq OWNER TO postgres;

--
-- TOC entry 2431 (class 0 OID 0)
-- Dependencies: 205
-- Name: socios_socio_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.socios_socio_seq OWNED BY aplicacion.socios.socio;


--
-- TOC entry 206 (class 1259 OID 28844)
-- Name: tipo_descuentos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.tipo_descuentos (
    tipo_descuento integer NOT NULL,
    descripcion character varying
);


ALTER TABLE aplicacion.tipo_descuentos OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 28850)
-- Name: tipo_descuentos_tipo_descuento_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.tipo_descuentos_tipo_descuento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.tipo_descuentos_tipo_descuento_seq OWNER TO postgres;

--
-- TOC entry 2432 (class 0 OID 0)
-- Dependencies: 207
-- Name: tipo_descuentos_tipo_descuento_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.tipo_descuentos_tipo_descuento_seq OWNED BY aplicacion.tipo_descuentos.tipo_descuento;


--
-- TOC entry 208 (class 1259 OID 28852)
-- Name: roles; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.roles (
    rol integer NOT NULL,
    nombre_rol character varying(140)
);


ALTER TABLE sistema.roles OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 28855)
-- Name: roles_rol_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.roles_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.roles_rol_seq OWNER TO postgres;

--
-- TOC entry 2433 (class 0 OID 0)
-- Dependencies: 209
-- Name: roles_rol_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.roles_rol_seq OWNED BY sistema.roles.rol;


--
-- TOC entry 210 (class 1259 OID 28857)
-- Name: roles_x_selectores; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.roles_x_selectores (
    id integer NOT NULL,
    rol integer,
    selector integer
);


ALTER TABLE sistema.roles_x_selectores OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 28860)
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.roles_x_selectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.roles_x_selectores_id_seq OWNER TO postgres;

--
-- TOC entry 2434 (class 0 OID 0)
-- Dependencies: 211
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.roles_x_selectores_id_seq OWNED BY sistema.roles_x_selectores.id;


--
-- TOC entry 212 (class 1259 OID 28862)
-- Name: selectores; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.selectores (
    id integer NOT NULL,
    superior integer,
    descripcion character varying,
    ord integer,
    link character varying
);


ALTER TABLE sistema.selectores OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 28868)
-- Name: selectores_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.selectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.selectores_id_seq OWNER TO postgres;

--
-- TOC entry 2435 (class 0 OID 0)
-- Dependencies: 213
-- Name: selectores_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.selectores_id_seq OWNED BY sistema.selectores.id;


--
-- TOC entry 214 (class 1259 OID 28870)
-- Name: selectores_x_webservice; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.selectores_x_webservice (
    id integer NOT NULL,
    selector integer,
    wservice integer
);


ALTER TABLE sistema.selectores_x_webservice OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 28873)
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.selectores_x_webservice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.selectores_x_webservice_id_seq OWNER TO postgres;

--
-- TOC entry 2436 (class 0 OID 0)
-- Dependencies: 215
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.selectores_x_webservice_id_seq OWNED BY sistema.selectores_x_webservice.id;


--
-- TOC entry 216 (class 1259 OID 28875)
-- Name: usuarios; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.usuarios (
    usuario integer NOT NULL,
    cuenta character varying(100),
    clave character varying(150),
    token_iat character varying
);


ALTER TABLE sistema.usuarios OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 28881)
-- Name: usuarios_usuario_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.usuarios_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.usuarios_usuario_seq OWNER TO postgres;

--
-- TOC entry 2437 (class 0 OID 0)
-- Dependencies: 217
-- Name: usuarios_usuario_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.usuarios_usuario_seq OWNED BY sistema.usuarios.usuario;


--
-- TOC entry 218 (class 1259 OID 28883)
-- Name: usuarios_x_roles; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.usuarios_x_roles (
    id integer NOT NULL,
    usuario integer,
    rol integer
);


ALTER TABLE sistema.usuarios_x_roles OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 28886)
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.usuarios_x_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.usuarios_x_roles_id_seq OWNER TO postgres;

--
-- TOC entry 2438 (class 0 OID 0)
-- Dependencies: 219
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.usuarios_x_roles_id_seq OWNED BY sistema.usuarios_x_roles.id;


--
-- TOC entry 220 (class 1259 OID 28888)
-- Name: webservice; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.webservice (
    wservice integer NOT NULL,
    path character varying,
    nombre character varying
);


ALTER TABLE sistema.webservice OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 28894)
-- Name: webservice_wservice_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.webservice_wservice_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.webservice_wservice_seq OWNER TO postgres;

--
-- TOC entry 2439 (class 0 OID 0)
-- Dependencies: 221
-- Name: webservice_wservice_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.webservice_wservice_seq OWNED BY sistema.webservice.wservice;


--
-- TOC entry 2185 (class 2604 OID 28896)
-- Name: casacomercial; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.casas_comerciales ALTER COLUMN casacomercial SET DEFAULT nextval('aplicacion.casas_comerciales_casacomercial_seq'::regclass);


--
-- TOC entry 2186 (class 2604 OID 28897)
-- Name: id; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.cierre_mensual ALTER COLUMN id SET DEFAULT nextval('aplicacion.cierre_mensual_id_seq'::regclass);


--
-- TOC entry 2187 (class 2604 OID 28898)
-- Name: servicio; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.concepto_servicios ALTER COLUMN servicio SET DEFAULT nextval('aplicacion.concepto_servicios_servicio_seq'::regclass);


--
-- TOC entry 2189 (class 2604 OID 28899)
-- Name: ordencompra; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.ordenes_compras ALTER COLUMN ordencompra SET DEFAULT nextval('aplicacion.ordenes_compras_ordencompra_seq'::regclass);


--
-- TOC entry 2190 (class 2604 OID 28900)
-- Name: id; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.ordenes_compras_pagos ALTER COLUMN id SET DEFAULT nextval('aplicacion.ordenes_compras_pagos_id_seq'::regclass);


--
-- TOC entry 2191 (class 2604 OID 28901)
-- Name: id; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.ordenes_compras_pagos_seguro ALTER COLUMN id SET DEFAULT nextval('aplicacion.ordenes_compras_pagos_seguro_id_seq'::regclass);


--
-- TOC entry 2193 (class 2604 OID 28902)
-- Name: id; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.servicios_fijos_descuentos ALTER COLUMN id SET DEFAULT nextval('aplicacion.servicio_fijo_proceso_id_seq'::regclass);


--
-- TOC entry 2195 (class 2604 OID 28903)
-- Name: id; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.servicios_temporal_descuentos ALTER COLUMN id SET DEFAULT nextval('aplicacion.servicios_temporal_descuentos_id_seq'::regclass);


--
-- TOC entry 2197 (class 2604 OID 28904)
-- Name: socio; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.socios ALTER COLUMN socio SET DEFAULT nextval('aplicacion.socios_socio_seq'::regclass);


--
-- TOC entry 2198 (class 2604 OID 28905)
-- Name: id; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.socios_descuentos ALTER COLUMN id SET DEFAULT nextval('aplicacion.socios_descuentos_id_seq'::regclass);


--
-- TOC entry 2199 (class 2604 OID 28906)
-- Name: id; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.socios_descuentos_servicios ALTER COLUMN id SET DEFAULT nextval('aplicacion.socios_descuentos_servicios_id_seq'::regclass);


--
-- TOC entry 2200 (class 2604 OID 28907)
-- Name: tipo_descuento; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.tipo_descuentos ALTER COLUMN tipo_descuento SET DEFAULT nextval('aplicacion.tipo_descuentos_tipo_descuento_seq'::regclass);


--
-- TOC entry 2201 (class 2604 OID 28908)
-- Name: rol; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles ALTER COLUMN rol SET DEFAULT nextval('sistema.roles_rol_seq'::regclass);


--
-- TOC entry 2202 (class 2604 OID 28909)
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores ALTER COLUMN id SET DEFAULT nextval('sistema.roles_x_selectores_id_seq'::regclass);


--
-- TOC entry 2203 (class 2604 OID 28910)
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores ALTER COLUMN id SET DEFAULT nextval('sistema.selectores_id_seq'::regclass);


--
-- TOC entry 2204 (class 2604 OID 28911)
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores_x_webservice ALTER COLUMN id SET DEFAULT nextval('sistema.selectores_x_webservice_id_seq'::regclass);


--
-- TOC entry 2205 (class 2604 OID 28912)
-- Name: usuario; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios ALTER COLUMN usuario SET DEFAULT nextval('sistema.usuarios_usuario_seq'::regclass);


--
-- TOC entry 2206 (class 2604 OID 28913)
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles ALTER COLUMN id SET DEFAULT nextval('sistema.usuarios_x_roles_id_seq'::regclass);


--
-- TOC entry 2207 (class 2604 OID 28914)
-- Name: wservice; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.webservice ALTER COLUMN wservice SET DEFAULT nextval('sistema.webservice_wservice_seq'::regclass);


--
-- TOC entry 2373 (class 0 OID 28767)
-- Dependencies: 183
-- Data for Name: casas_comerciales; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.casas_comerciales (casacomercial, nombre, direccion, telefono, email) FROM stdin;
1	TM Comercial			
2	Itermed			
3	Nepar SRL			
4	New Travel			
6	Z y M calzados			
7	Grupo Odontologico			
8	Punto Farma			
9	J & A Electrodomésticos			
5	SDM Consulting	29 de setiembre 883 c/ Obreros	+595982910211	sdm@sdmconsulting.com.py
10	J&J COMERCIAL			
11	H24 COMERCIAL	8 DE SETIIEMBRE ESQUINA SPANOVICH	0991646801	brunetto0@gmail.com
12	PYMAX SA	Paraguari esquina Republica de colombia 	021440019	
13	Credito Express			
14	G. A Créditos	Benjamín Constant  casi 15 de agosto	(0971)242 -268	
15	CASA MONEY S.A. 	AVDA VENCERAS DEL CHACO C/ ALEJ. RAVIZZA	(0983)212-345	casamoneypy@gmail.com
16	INDIO PY		 (0986)537-429	
17	SOL MARTINA	sgto penayo 241 c/ dr zarate	0992440892	veramarisol633@gmail.com
\.


--
-- TOC entry 2440 (class 0 OID 0)
-- Dependencies: 184
-- Name: casas_comerciales_casacomercial_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.casas_comerciales_casacomercial_seq', 17, true);


--
-- TOC entry 2375 (class 0 OID 28775)
-- Dependencies: 185
-- Data for Name: cierre_mensual; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.cierre_mensual (id, fecha, mes, agno) FROM stdin;
1	2020-02-18	2	2020
2	2020-03-19	3	2020
3	2020-06-15	6	2020
12	2020-07-16	7	2020
13	2020-08-14	8	2020
15	2020-09-11	9	2020
\.


--
-- TOC entry 2441 (class 0 OID 0)
-- Dependencies: 186
-- Name: cierre_mensual_id_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.cierre_mensual_id_seq', 18, true);


--
-- TOC entry 2377 (class 0 OID 28780)
-- Dependencies: 187
-- Data for Name: concepto_servicios; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.concepto_servicios (servicio, descripcion, tipo_descuento, monto_fijo, cantidad, actual) FROM stdin;
2	Cuota social	1	20000	\N	\N
3	Seguro de vida	2	10000	\N	\N
4	Obsequio de fin de año	3	30000	3	0
\.


--
-- TOC entry 2442 (class 0 OID 0)
-- Dependencies: 188
-- Name: concepto_servicios_servicio_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.concepto_servicios_servicio_seq', 4, true);


--
-- TOC entry 2379 (class 0 OID 28788)
-- Dependencies: 189
-- Data for Name: ordenes_compras; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.ordenes_compras (ordencompra, casacomercial, socio, credito, cantidad_cuotas, monto_cuota, cuotas_pagadas, credito_saldo, fecha, user_add) FROM stdin;
364	1	35	1056000	12	88000	0	1056000	2020-10-09	natalia
355	15	337	1319560	10	131956	0	1319560	2020-10-06	natalia
362	1	166	3180000	12	265000	0	3180000	2020-10-07	natalia
238	1	343	600000	2	300000	2	0	2020-08-12	
356	15	146	1979333	10	197933	0	1979333	2020-10-06	natalia
363	16	166	360000	3	120000	0	360000	2020-10-07	natalia
107	1	22	660000	3	220000	3	0	2020-06-10	
357	15	35	1933333	10	193333	0	1933333	2020-10-06	natalia
197	12	328	4050000	10	405000	3	2835000	2020-07-14	
187	12	351	4050000	10	405000	3	2835000	2020-07-14	
358	15	37	1319560	10	131956	0	1319560	2020-10-06	natalia
30	1	171	1430000	10	143000	4	1000	2020-05-13	
233	1	244	1900000	10	190000	2	1520000	2020-08-06	
195	12	128	4050000	10	405000	3	2835000	2020-07-14	
305	14	87	1200000	4	300000	1	900000	2020-09-01	natalia
276	1	96	1800000	9	200000	1	1600000	2020-08-20	natalia
175	5	376	1380000	6	230000	3	690000	2020-07-08	
204	12	395	1350000	10	135000	3	945000	2020-07-15	
151	12	236	4050000	10	405000	3	2835000	2020-06-19	
162	12	83	1350000	10	135000	3	945000	2020-06-24	
253	12	291	2025000	10	202500	1	1822500	2020-08-18	natalia
119	3	74	1040000	10	104000	3	728000	2020-06-16	
122	12	178	1350000	10	135000	3	945000	2020-06-19	
62	1	2	4290000	10	429000	4	2574000	2020-05-18	
180	12	39	2700000	10	270000	3	1890000	2020-07-14	
114	1	54	1430000	10	143000	3	1001000	2020-06-10	
359	14	156	2860000	10	286000	0	2860000	2020-10-06	natalia
191	12	241	3630000	6	605000	3	1815000	2020-07-14	
156	12	52	2025000	10	202500	3	1417500	2020-06-24	
11	5	233	2244000	12	187000	6	1122000	2020-03-11	
249	12	404	4050000	10	405000	1	3645000	2020-08-18	natalia
269	14	410	1430000	10	143000	1	1287000	2020-08-20	natalia
194	12	347	2700000	10	270000	3	1890000	2020-07-14	
135	12	92	2700000	10	270000	3	1890000	2020-06-19	
224	1	6	600000	2	300000	2	0	2020-07-22	
143	11	253	2500000	10	250000	3	1750000	2020-06-19	
98	3	152	1000000	10	100000	3	700000	2020-06-02	
193	12	60	3375000	10	337500	3	2362500	2020-07-14	
271	10	412	2860000	10	286000	1	2574000	2020-08-20	natalia
243	12	146	4050000	10	405000	1	3645000	2020-08-18	natalia
167	12	90	1350000	10	135000	3	945000	2020-06-24	
250	12	274	605000	6	100833	1	504167	2020-08-18	natalia
116	1	86	7140000	10	714000	3	4998000	2020-06-11	
64	1	6	1430000	10	143000	4	858000	2020-05-19	
235	1	398	1716000	10	171600	2	1372800	2020-08-06	
145	12	154	2700000	10	270000	3	1890000	2020-06-19	
207	12	353	1350000	10	135000	3	945000	2020-07-15	
198	1	96	2300000	10	230000	3	1610000	2020-07-14	
104	5	257	4460000	10	446000	3	3122000	2020-06-09	
163	12	390	1350000	10	135000	3	945000	2020-06-24	
6	5	67	2227000	12	185583	6	1113502	2020-03-11	
102	1	68	2860000	10	286000	3	2002000	2020-06-05	
361	1	427	3180000	12	265000	0	3180000	2020-10-06	natalia
155	12	232	4050000	10	405000	3	2835000	2020-06-24	
149	12	389	1350000	10	135000	3	945000	2020-06-19	
205	12	2	3375000	10	337500	3	2362500	2020-07-15	
33	1	57	1332000	6	222000	4	444000	2020-05-13	
109	1	130	4290000	10	429000	3	3003000	2020-06-10	
51	1	102	1430000	10	143000	4	858000	2020-05-13	
239	12	283	3375000	10	337500	1	3037500	2020-08-18	natalia
31	1	49	1430000	10	143000	4	858000	2020-05-13	
288	12	109	2700000	10	270000	1	2430000	2020-08-26	natalia
203	12	388	4050000	10	405000	3	2835000	2020-07-15	
219	12	5	675000	10	67500	3	472500	2020-07-15	
12	1	154	2700000	15	180000	6	1620000	2020-03-12	
273	1	52	2000000	10	200000	1	1800000	2020-08-20	bruno
296	14	75	1100000	2	550000	1	550000	2020-08-28	natalia
282	1	68	2700000	10	270000	1	2430000	2020-08-25	natalia
10	1	165	2700000	15	180000	6	1620000	2020-03-11	
42	1	100	2860000	10	286000	4	1716000	2020-05-13	
18	5	70	2227500	12	185625	6	1113750	2020-03-13	
134	12	77	1350000	10	135000	3	945000	2020-06-19	
59	1	141	1430000	10	143000	4	858000	2020-05-18	
139	12	117	2700000	10	270000	3	1890000	2020-06-19	
146	12	326	4050000	10	405000	3	2835000	2020-06-19	
192	12	345	4050000	10	405000	3	2835000	2020-07-14	
29	1	152	2860000	10	286000	4	1716000	2020-05-13	
69	1	115	2150000	10	215000	4	1290000	2020-05-22	
71	1	42	2860000	10	286000	4	1716000	2020-05-22	
76	1	239	1430000	10	143000	4	858000	2020-05-22	
79	1	114	2860000	10	286000	4	1716000	2020-05-22	
81	1	172	1430000	10	143000	4	858000	2020-05-22	
90	1	213	7140000	10	714000	4	4284000	2020-05-22	
93	5	49	1896000	8	237000	4	948000	2020-05-25	
97	11	74	2280000	10	228000	4	1368000	2020-05-29	
159	12	230	1350000	10	135000	3	945000	2020-06-24	
248	12	336	2700000	10	270000	1	2430000	2020-08-18	natalia
267	14	60	4290000	10	429000	1	3861000	2020-08-19	natalia
186	12	14	675000	10	67500	3	472500	2020-07-14	
234	1	294	4752000	12	396000	2	3960000	2020-08-06	
177	12	70	2700000	10	270000	3	1890000	2020-07-14	
141	12	109	1350000	10	135000	3	945000	2020-06-19	
171	12	2	2700000	10	270000	3	1890000	2020-06-24	
227	1	74	630000	10	63000	2	504000	2020-08-04	
247	12	403	605000	6	100833	1	504167	2020-08-18	natalia
280	10	156	2860000	10	286000	1	2574000	2020-08-21	natalia
138	12	211	2700000	10	270000	3	1890000	2020-06-19	
297	14	409	1100000	2	550000	1	550000	2020-08-28	natalia
16	1	48	2700000	12	225000	6	1350000	2020-03-13	
54	1	160	2860000	10	286000	4	1716000	2020-05-13	
274	13	414	360000	2	180000	1	180000	2020-08-20	natalia
47	1	89	2860000	10	286000	4	1716000	2020-05-13	
103	5	256	824000	8	103000	3	515000	2020-06-09	
115	1	9	4290000	10	429000	3	3003000	2020-06-11	
148	12	98	1350000	10	135000	3	945000	2020-06-19	
236	1	289	2628000	12	219000	2	2190000	2020-08-11	
208	12	290	1210000	6	201667	3	604999	2020-07-15	
46	1	25	2150000	10	215000	4	1290000	2020-05-13	
294	14	400	1550000	10	155000	1	1395000	2020-08-28	natalia
36	1	211	2150000	10	215000	4	1290000	2020-05-13	
15	5	206	2227500	12	185625	6	1113750	2020-03-13	
174	11	218	1900000	10	190000	3	1330000	2020-07-02	
304	14	204	1290000	6	215000	1	1075000	2020-08-28	natalia
125	12	171	1350000	10	135000	3	945000	2020-06-19	
140	12	35	2700000	10	270000	3	1890000	2020-06-19	
153	12	87	4050000	10	405000	3	2835000	2020-06-22	
200	5	226	2210000	10	221000	3	1547000	2020-07-14	
257	12	407	2700000	10	270000	1	2430000	2020-08-18	natalia
40	1	39	1430000	10	143000	4	858000	2020-05-13	
56	1	189	1430000	10	143000	4	858000	2020-05-13	
182	12	284	1350000	10	135000	3	945000	2020-07-14	
222	12	216	2700000	10	270000	3	1890000	2020-07-15	
277	1	154	1800000	10	180000	1	1620000	2020-08-20	bruno
123	12	123	2700000	10	270000	3	1890000	2020-06-19	
275	14	96	2150000	10	215000	1	1935000	2020-08-20	natalia
23	10	39	1430000	10	143000	4	858000	2020-05-08	
44	1	123	1430000	10	143000	4	858000	2020-05-13	
58	1	122	1430000	10	143000	4	858000	2020-05-13	
189	12	283	2420000	6	403333	3	1210001	2020-07-14	
290	12	160	4050000	10	405000	1	3645000	2020-08-26	natalia
111	1	128	1430000	10	143000	3	1001000	2020-06-10	
206	12	333	4050000	10	405000	3	2835000	2020-07-15	
237	3	343	1400000	10	140000	2	1120000	2020-08-12	
226	1	146	1430000	10	143000	2	1144000	2020-07-24	
13	5	153	2227500	12	185625	6	1113750	2020-03-13	
22	10	70	2150000	10	215000	4	1290000	2020-05-08	
45	1	74	2150000	10	215000	4	1290000	2020-05-13	
225	3	123	1000000	10	100000	2	800000	2020-07-23	
199	5	90	4680000	9	520000	3	3120000	2020-07-14	
303	14	19	1380000	6	230000	1	1150000	2020-08-28	natalia
293	12	428	1350000	10	135000	1	1215000	2020-08-27	natalia
60	1	96	5720000	10	572000	4	3432000	2020-05-18	
165	12	121	1350000	10	135000	3	945000	2020-06-24	
105	5	46	2400000	8	300000	3	1500000	2020-06-10	
21	10	48	2150000	10	215000	4	1290000	2020-05-08	
152	12	389	675000	10	67500	3	472500	2020-06-22	
124	12	70	2700000	10	270000	3	1890000	2020-06-19	
245	12	401	4050000	10	405000	1	3645000	2020-08-18	natalia
181	12	394	4050000	10	405000	3	2835000	2020-07-14	
43	1	159	1430000	10	143000	4	858000	2020-05-13	
220	3	2	1000000	10	100000	3	700000	2020-07-15	
259	12	52	4050000	10	405000	1	3645000	2020-08-18	natalia
166	12	210	2700000	10	270000	3	1890000	2020-06-24	
298	14	429	1380000	6	230000	1	1150000	2020-08-28	natalia
112	1	343	1430000	10	143000	3	1001000	2020-06-10	
254	12	388	1350000	10	135000	1	1215000	2020-08-18	natalia
201	12	119	2700000	10	270000	3	1890000	2020-07-15	
176	12	172	1350000	10	135000	3	945000	2020-07-14	
354	17	326	2388000	12	199000	0	2388000	2020-10-05	natalia
129	12	23	1350000	10	135000	3	945000	2020-06-19	
246	12	402	3630000	6	605000	1	3025000	2020-08-18	natalia
61	1	243	1430000	10	143000	4	858000	2020-05-18	
70	1	56	3570000	10	357000	4	2142000	2020-05-22	
72	1	218	2860000	10	286000	4	1716000	2020-05-22	
73	1	206	2150000	10	215000	4	1290000	2020-05-22	
75	1	136	2150000	10	215000	4	1290000	2020-05-22	
83	1	120	1430000	10	143000	4	858000	2020-05-22	
86	1	104	1430000	10	143000	4	858000	2020-05-22	
91	1	235	2860000	10	286000	4	1716000	2020-05-22	
92	1	255	1430000	10	143000	4	858000	2020-05-22	
169	12	74	2025000	10	202500	3	1417500	2020-06-24	
14	5	234	2227500	12	185625	3	1670625	2020-03-13	
221	12	69	605000	6	100833	3	302501	2020-07-15	
147	12	60	4050000	10	405000	3	2835000	2020-06-19	
121	12	91	4050000	10	405000	3	2835000	2020-06-19	
210	12	92	1350000	10	135000	3	945000	2020-07-15	
258	12	221	4050000	10	405000	1	3645000	2020-08-18	natalia
161	12	57	1350000	10	135000	3	945000	2020-06-24	
19	10	102	1430000	10	143000	4	858000	2020-05-08	
108	3	48	800000	10	80000	3	560000	2020-06-10	
157	12	214	1350000	10	135000	3	945000	2020-06-24	
154	11	133	1900000	10	190000	3	1330000	2020-06-22	
215	12	192	4050000	10	405000	3	2835000	2020-07-15	
35	1	139	2150000	10	215000	4	1290000	2020-05-13	
37	1	212	2860000	10	286000	4	1716000	2020-05-13	
173	11	122	2500000	10	250000	3	1750000	2020-06-30	
32	1	236	2860000	10	286000	4	1716000	2020-05-13	
188	12	336	4050000	10	405000	3	2835000	2020-07-14	
164	12	229	2025000	10	202500	3	1417500	2020-06-24	
306	1	289	4056000	12	338000	1	3718000	2020-09-08	bruno
278	10	236	1430000	10	143000	1	1287000	2020-08-20	natalia
133	12	4	1350000	10	135000	3	945000	2020-06-19	
289	12	154	1350000	10	135000	1	1215000	2020-08-26	natalia
279	14	340	2860000	10	286000	1	2574000	2020-08-21	natalia
100	11	152	2500000	10	250000	3	1750000	2020-06-02	
232	1	245	2000000	10	200000	2	1600000	2020-08-06	
158	12	108	1350000	10	135000	3	945000	2020-06-24	
113	1	159	2150000	10	215000	3	1505000	2020-06-10	
172	3	35	1000000	10	100000	3	700000	2020-06-26	
9	1	90	1350000	10	135000	6	540000	2020-03-11	
24	10	78	4290000	10	429000	4	2574000	2020-05-08	
184	12	165	1350000	10	135000	3	945000	2020-07-14	
260	1	22	360000	2	180000	1	180000	2020-08-18	natalia
170	12	10	2025000	10	202500	3	1417500	2020-06-24	
118	3	48	800000	10	80000	3	560000	2020-06-11	
244	10	399	1210000	6	201667	1	1008333	2020-08-18	natalia
130	12	48	2700000	10	270000	3	1890000	2020-06-19	
272	14	413	2860000	10	286000	1	2574000	2020-08-20	natalia
38	1	98	2860000	10	286000	4	1716000	2020-05-13	
300	14	421	1290000	6	215000	1	1075000	2020-08-28	natalia
283	1	119	2650000	10	265000	1	2385000	2020-08-25	bruno
217	12	182	2700000	10	270000	3	1890000	2020-07-15	
231	1	241	2800000	10	280000	2	2240000	2020-08-04	
212	12	10	1350000	10	135000	3	945000	2020-07-15	
256	12	130	1210000	6	201667	1	1008333	2020-08-18	natalia
216	12	15	2025000	10	202500	3	1417500	2020-07-15	
263	1	358	1250000	10	125000	1	1125000	2020-08-19	natalia
202	12	174	2025000	10	202500	3	1417500	2020-07-15	
266	14	230	1430000	10	143000	1	1287000	2020-08-19	natalia
144	12	139	2700000	10	270000	3	1890000	2020-06-19	
7	1	90	1800000	12	150000	6	900000	2020-03-11	
211	12	343	1350000	10	135000	3	945000	2020-07-15	
261	12	288	605000	6	100833	1	504167	2020-08-18	natalia
318	12	432	1210000	6	201667	0	1210000	2020-09-18	natalia
312	12	77	1350000	10	135000	0	1350000	2020-09-18	natalia
313	12	394	2700000	10	270000	0	2700000	2020-09-18	natalia
251	12	405	1210000	6	201667	1	1008333	2020-08-18	natalia
106	1	39	3570000	10	357000	3	2499000	2020-06-10	
120	3	166	1800000	10	180000	3	1260000	2020-06-19	
285	12	427	2700000	10	270000	1	2430000	2020-08-26	natalia
8	1	54	8565000	15	571000	6	5139000	2020-03-11	
264	14	176	2860000	10	286000	1	2574000	2020-08-19	natalia
179	12	280	4050000	10	405000	3	2835000	2020-07-14	
209	12	230	1350000	10	135000	3	945000	2020-07-15	
110	1	37	2160000	10	216000	3	1512000	2020-06-10	
99	3	96	640000	10	64000	3	448000	2020-06-02	
48	1	60	2860000	10	286000	4	1716000	2020-05-13	
28	1	221	1430000	10	143000	4	858000	2020-05-13	
262	12	255	2700000	10	270000	1	2430000	2020-08-18	natalia
299	14	70	1430000	10	143000	1	1287000	2020-08-28	natalia
270	14	411	4290000	10	429000	1	3861000	2020-08-20	natalia
287	12	81	4050000	10	405000	1	3645000	2020-08-26	natalia
268	14	409	2860000	10	286000	1	2574000	2020-08-20	natalia
214	12	35	2700000	10	270000	3	1890000	2020-07-15	
52	5	239	2300000	10	230000	4	1380000	2020-05-13	
55	1	240	1430000	10	143000	4	858000	2020-05-13	
65	5	240	4620000	10	462000	4	2772000	2020-05-20	
68	5	67	900000	6	150000	4	300000	2020-05-22	
80	1	199	2860000	10	286000	4	1716000	2020-05-22	
85	1	34	3570000	10	357000	4	2142000	2020-05-22	
87	1	77	1430000	10	143000	4	858000	2020-05-22	
88	1	182	3570000	10	357000	4	2142000	2020-05-22	
94	5	182	2360000	10	236000	4	1416000	2020-05-25	
95	3	99	680000	10	68000	4	408000	2020-05-28	
96	3	74	780000	10	78000	4	468000	2020-05-28	
127	12	78	4050000	10	405000	3	2835000	2020-06-19	
101	3	96	1050000	10	105000	3	735000	2020-06-03	
20	10	154	2860000	10	286000	4	1716000	2020-05-08	
25	10	22	1430000	10	143000	4	858000	2020-05-08	
26	10	90	2860000	10	286000	4	1716000	2020-05-11	
213	12	396	2025000	10	202500	3	1417500	2020-07-15	
137	11	77	3800000	10	380000	3	2660000	2020-06-19	
117	3	122	1600000	10	160000	3	1120000	2020-06-11	
185	12	171	4050000	10	405000	3	2835000	2020-07-14	
265	14	408	2860000	10	286000	1	2574000	2020-08-19	natalia
292	12	426	4050000	10	405000	1	3645000	2020-08-26	natalia
168	12	33	2025000	10	202500	3	1417500	2020-06-24	
136	12	93	2700000	10	270000	3	1890000	2020-06-19	
196	12	202	2420000	6	403333	3	1210001	2020-07-14	
218	12	211	4050000	10	405000	3	2835000	2020-07-15	
27	10	235	4290000	10	429000	4	2574000	2020-05-11	
286	12	235	4050000	10	405000	1	3645000	2020-08-26	natalia
329	1	92	1800000	10	180000	0	1800000	2020-09-21	natalia
310	12	120	1350000	10	135000	0	1350000	2020-09-18	natalia
339	3	123	780000	10	78000	0	780000	2020-09-23	natalia
330	15	96	3958670	10	395867	0	3958670	2020-09-21	natalia
309	12	359	1350000	10	135000	0	1350000	2020-09-18	natalia
349	3	415	690000	10	69000	0	690000	2020-09-28	natalia
341	1	166	3504000	12	292000	0	3504000	2020-09-23	natalia
307	10	340	1430000	10	143000	0	1430000	2020-09-16	bruno
335	13	290	2400000	4	600000	0	2400000	2020-09-22	natalia
351	1	70	2040000	12	170000	0	2040000	2020-10-01	natalia
230	1	241	1430000	10	143000	5	715000	2020-08-04	
316	12	104	2025000	10	202500	0	2025000	2020-09-18	natalia
336	13	193	4200000	10	420000	0	4200000	2020-09-22	natalia
352	3	152	780000	10	78000	0	780000	2020-10-01	natalia
325	12	338	2700000	10	270000	0	2700000	2020-09-18	natalia
334	13	241	600000	2	300000	0	600000	2020-09-22	natalia
321	10	433	1380000	6	230000	0	1380000	2020-09-18	natalia
324	12	434	4050000	10	405000	0	4050000	2020-09-18	natalia
338	10	403	1550000	10	155000	0	1550000	2020-09-23	natalia
350	1	86	3000000	12	250000	0	3000000	2020-10-01	natalia
315	12	124	2700000	10	270000	0	2700000	2020-09-18	natalia
343	1	56	3900000	15	260000	0	3900000	2020-09-25	natalia
346	1	241	300000	6	50000	0	300000	2020-09-28	natalia
337	13	60	2900000	10	290000	0	2900000	2020-09-22	natalia
311	1	120	3480000	12	290000	0	3480000	2020-09-18	natalia
347	3	241	1260000	10	126000	0	1260000	2020-09-28	natalia
314	12	404	3375000	10	337500	0	3375000	2020-09-18	natalia
342	1	14	2000000	10	200000	0	2000000	2020-09-25	natalia
320	12	123	1350000	10	135000	0	1350000	2020-09-18	natalia
323	12	73	2700000	10	270000	0	2700000	2020-09-18	natalia
322	12	392	2700000	10	270000	0	2700000	2020-09-18	natalia
348	3	395	750000	10	75000	0	750000	2020-09-28	natalia
332	15	289	1319560	10	131956	0	1319560	2020-09-21	natalia
333	15	2	3958670	10	395867	0	3958670	2020-09-21	natalia
327	12	181	2700000	10	270000	0	2700000	2020-09-18	natalia
340	3	154	980000	10	98000	0	980000	2020-09-23	natalia
344	16	166	360000	3	120000	0	360000	2020-09-25	natalia
319	12	4	1350000	10	135000	0	1350000	2020-09-18	natalia
317	12	343	1350000	10	135000	0	1350000	2020-09-18	natalia
326	12	435	2700000	10	270000	0	2700000	2020-09-18	natalia
345	1	6	600000	2	300000	0	600000	2020-09-28	natalia
308	12	187	2700000	10	270000	0	2700000	2020-09-18	natalia
331	15	395	3958670	10	395867	0	3958670	2020-09-21	natalia
353	14	414	4290000	10	429000	0	4290000	2020-10-02	natalia
328	1	425	2000000	10	200000	0	2000000	2020-09-18	bruno
126	12	82	2700000	10	270000	3	1890000	2020-06-19	
228	1	388	1430000	10	143000	4	858000	2020-08-04	
252	12	406	2700000	10	270000	1	2430000	2020-08-18	natalia
39	1	135	1430000	10	143000	4	858000	2020-05-13	
132	12	194	2700000	10	270000	3	1890000	2020-06-19	
131	12	113	2700000	10	270000	3	1890000	2020-06-19	
178	12	19	2700000	10	270000	3	1890000	2020-07-14	
255	12	241	3630000	6	605000	1	3025000	2020-08-18	natalia
241	12	400	4050000	10	405000	1	3645000	2020-08-18	natalia
17	1	39	3324000	12	277000	6	1662000	2020-03-13	
142	12	72	4050000	10	405000	3	2835000	2020-06-19	
160	12	45	2700000	10	270000	3	1890000	2020-06-24	
284	12	21	2700000	10	270000	1	2430000	2020-08-26	natalia
50	5	238	2300000	10	230000	4	1380000	2020-05-13	
281	14	415	630000	6	105000	1	525000	2020-08-21	natalia
229	1	388	2800000	10	280000	4	1680000	2020-08-04	
301	14	2	1430000	10	143000	1	1287000	2020-08-28	natalia
67	5	242	3912000	12	326000	4	2608000	2020-05-21	
82	1	187	2860000	10	286000	4	1716000	2020-05-22	
89	1	254	2860000	10	286000	4	1716000	2020-05-22	
150	10	159	660000	3	220000	3	0	2020-06-19	
183	12	29	1210000	6	201667	6	0	2020-07-14	
223	1	343	600000	2	300000	2	0	2020-07-22	
78	1	130	1430000	10	143000	4	858000	2020-05-22	
41	1	5	1430000	10	143000	4	858000	2020-05-13	
49	1	237	1430000	10	143000	4	858000	2020-05-13	
57	1	241	1430000	10	143000	4	858000	2020-05-13	
66	1	70	2280000	12	190000	4	1520000	2020-05-20	
84	1	253	1430000	10	143000	4	858000	2020-05-22	
\.


--
-- TOC entry 2443 (class 0 OID 0)
-- Dependencies: 190
-- Name: ordenes_compras_ordencompra_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.ordenes_compras_ordencompra_seq', 364, true);


--
-- TOC entry 2381 (class 0 OID 28797)
-- Dependencies: 191
-- Data for Name: ordenes_compras_pagos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.ordenes_compras_pagos (id, ordencompra, fecha, numero_cuota, monto_cuota, credito_saldo, mes, agno, id_cierre) FROM stdin;
1	30	2020-05-31	1	143000	1287000	5	2020	\N
2	13	2020-03-31	1	185625	2041875	3	2020	\N
3	13	2020-04-30	2	185625	1856250	4	2020	\N
4	13	2020-05-31	3	185625	1670625	5	2020	\N
5	6	2020-04-30	1	185583	2041417	4	2020	\N
6	6	2020-05-11	2	185583	1855834	5	2020	\N
7	64	2020-05-31	1	143000	1287000	5	2020	\N
8	25	2020-05-31	1	143000	1287000	5	2020	\N
9	46	2020-05-31	1	215000	1935000	5	2020	\N
10	40	2020-05-31	1	143000	1287000	5	2020	\N
11	23	2020-05-31	1	143000	1287000	5	2020	\N
12	17	2020-03-31	1	277000	3047000	3	2020	\N
13	17	2020-04-30	2	277000	2770000	4	2020	\N
14	17	2020-05-31	3	277000	2493000	5	2020	\N
15	21	2020-05-31	1	215000	1935000	5	2020	\N
16	16	2020-03-31	1	225000	2475000	3	2020	\N
17	16	2020-04-30	2	225000	2250000	4	2020	\N
18	16	2020-05-31	3	225000	2025000	5	2020	\N
19	31	2020-05-31	1	143000	1287000	5	2020	\N
20	8	2020-03-31	1	571000	7994000	3	2020	\N
21	8	2020-04-30	2	571000	7423000	4	2020	\N
22	8	2020-05-31	3	571000	6852000	5	2020	\N
23	33	2020-05-31	1	222000	1110000	5	2020	\N
24	48	2020-05-31	1	286000	2574000	5	2020	\N
25	32	2020-05-31	1	286000	2574000	5	2020	\N
26	6	2020-05-31	3	185583	1670251	5	2020	\N
28	22	2020-05-31	1	215000	1935000	5	2020	\N
29	18	2020-03-31	1	185625	2041875	3	2020	\N
30	18	2020-04-30	2	185625	1856250	4	2020	\N
31	18	2020-05-31	3	185625	1670625	5	2020	\N
32	45	2020-05-31	1	215000	1935000	5	2020	\N
33	24	2020-05-31	1	429000	3861000	5	2020	\N
35	47	2020-05-31	1	286000	2574000	5	2020	\N
36	9	2020-03-31	1	135000	1215000	3	2020	\N
37	9	2020-04-30	2	135000	1080000	4	2020	\N
38	9	2020-05-31	3	135000	945000	5	2020	\N
39	7	2020-03-31	1	150000	1650000	3	2020	\N
40	7	2020-04-30	2	150000	1500000	4	2020	\N
41	7	2020-05-31	3	150000	1350000	5	2020	\N
42	26	2020-05-31	1	286000	2574000	5	2020	\N
43	60	2020-05-31	1	572000	5148000	5	2020	\N
44	38	2020-05-31	1	286000	2574000	5	2020	\N
45	42	2020-05-31	1	286000	2574000	5	2020	\N
46	19	2020-05-31	1	143000	1287000	5	2020	\N
47	51	2020-05-31	1	143000	1287000	5	2020	\N
48	58	2020-05-31	1	143000	1287000	5	2020	\N
49	44	2020-05-31	1	143000	1287000	5	2020	\N
50	39	2020-05-31	1	143000	1287000	5	2020	\N
51	35	2020-05-31	1	215000	1935000	5	2020	\N
52	59	2020-05-31	1	143000	1287000	5	2020	\N
53	29	2020-05-31	1	286000	2574000	5	2020	\N
54	12	2020-03-31	1	180000	2520000	3	2020	\N
55	12	2020-04-30	2	180000	2340000	4	2020	\N
56	12	2020-05-31	3	180000	2160000	5	2020	\N
57	20	2020-05-31	1	286000	2574000	5	2020	\N
58	43	2020-05-31	1	143000	1287000	5	2020	\N
59	54	2020-05-31	1	286000	2574000	5	2020	\N
60	11	2020-03-31	1	187000	2057000	3	2020	\N
61	11	2020-04-30	2	187000	1870000	4	2020	\N
62	11	2020-05-31	3	187000	1683000	5	2020	\N
63	50	2020-05-31	1	230000	2070000	5	2020	\N
64	10	2020-03-31	1	180000	2520000	3	2020	\N
65	10	2020-04-30	2	180000	2340000	4	2020	\N
66	10	2020-05-31	3	180000	2160000	5	2020	\N
67	27	2020-05-31	1	429000	3861000	5	2020	\N
68	62	2020-05-31	1	429000	3861000	5	2020	\N
69	56	2020-05-31	1	143000	1287000	5	2020	\N
70	15	2020-03-31	1	185625	2041875	3	2020	\N
71	15	2020-04-30	2	185625	1856250	4	2020	\N
72	15	2020-05-31	3	185625	1670625	5	2020	\N
73	36	2020-05-31	1	215000	1935000	5	2020	\N
74	37	2020-05-31	1	286000	2574000	5	2020	\N
75	28	2020-05-31	1	143000	1287000	5	2020	\N
2305	78	2020-06-15	1	143000	1287000	6	2020	3
2313	49	2020-06-15	1	143000	1287000	6	2020	3
2321	55	2020-06-15	1	143000	1287000	6	2020	3
2329	61	2020-06-15	1	143000	1287000	6	2020	3
2337	66	2020-06-15	1	190000	2090000	6	2020	3
2345	68	2020-06-15	1	150000	750000	6	2020	3
2353	70	2020-06-15	1	357000	3213000	6	2020	3
2361	72	2020-06-15	1	286000	2574000	6	2020	3
2369	75	2020-06-15	1	215000	1935000	6	2020	3
2377	79	2020-06-15	1	286000	2574000	6	2020	3
2385	81	2020-06-15	1	143000	1287000	6	2020	3
2393	83	2020-06-15	1	143000	1287000	6	2020	3
2401	85	2020-06-15	1	357000	3213000	6	2020	3
2409	87	2020-06-15	1	143000	1287000	6	2020	3
2417	89	2020-06-15	1	286000	2574000	6	2020	3
2425	91	2020-06-15	1	286000	2574000	6	2020	3
2433	93	2020-06-15	1	237000	1659000	6	2020	3
2441	95	2020-06-15	1	68000	612000	6	2020	3
2449	97	2020-06-15	1	228000	2052000	6	2020	3
600	115	2020-08-14	2	429000	3432000	8	2020	13
601	116	2020-08-14	2	714000	5712000	8	2020	13
602	117	2020-08-14	2	160000	1280000	8	2020	13
2306	78	2020-07-16	2	143000	1144000	7	2020	12
2314	49	2020-07-16	2	143000	1144000	7	2020	12
2322	55	2020-07-16	2	143000	1144000	7	2020	12
2330	61	2020-07-16	2	143000	1144000	7	2020	12
2338	66	2020-07-16	2	190000	1900000	7	2020	12
2346	68	2020-07-16	2	150000	600000	7	2020	12
2354	70	2020-07-16	2	357000	2856000	7	2020	12
2362	72	2020-07-16	2	286000	2288000	7	2020	12
2370	75	2020-07-16	2	215000	1720000	7	2020	12
2378	79	2020-07-16	2	286000	2288000	7	2020	12
2386	81	2020-07-16	2	143000	1144000	7	2020	12
2394	83	2020-07-16	2	143000	1144000	7	2020	12
2402	85	2020-07-16	2	357000	2856000	7	2020	12
2410	87	2020-07-16	2	143000	1144000	7	2020	12
2418	89	2020-07-16	2	286000	2288000	7	2020	12
2426	91	2020-07-16	2	286000	2288000	7	2020	12
491	230	2020-08-11	1	143000	1287000	4	2020	\N
492	230	2020-08-11	2	143000	1144000	5	2020	\N
493	230	2020-08-11	3	143000	1001000	6	2020	\N
494	228	2020-08-12	1	143000	1287000	7	2020	\N
495	228	2020-08-12	2	143000	1144000	6	2020	\N
496	228	2020-08-12	3	143000	1001000	5	2020	\N
497	229	2020-08-12	1	280000	2520000	5	2020	\N
498	229	2020-08-12	2	280000	2240000	6	2020	\N
2434	93	2020-07-16	2	237000	1422000	7	2020	12
2442	95	2020-07-16	2	68000	544000	7	2020	12
2450	97	2020-07-16	2	228000	1824000	7	2020	12
603	118	2020-08-14	2	80000	640000	8	2020	13
604	119	2020-08-14	2	104000	832000	8	2020	13
605	120	2020-08-14	2	180000	1440000	8	2020	13
606	222	2020-08-14	2	270000	2160000	8	2020	13
607	121	2020-08-14	2	405000	3240000	8	2020	13
608	122	2020-08-14	2	135000	1080000	8	2020	13
609	123	2020-08-14	2	270000	2160000	8	2020	13
610	124	2020-08-14	2	270000	2160000	8	2020	13
611	125	2020-08-14	2	135000	1080000	8	2020	13
612	126	2020-08-14	2	270000	2160000	8	2020	13
613	127	2020-08-14	2	405000	3240000	8	2020	13
614	48	2020-08-14	3	286000	2002000	8	2020	13
615	32	2020-08-14	3	286000	2002000	8	2020	13
616	142	2020-08-14	2	405000	3240000	8	2020	13
617	143	2020-08-14	2	250000	2000000	8	2020	13
618	144	2020-08-14	2	270000	2160000	8	2020	13
619	145	2020-08-14	2	270000	2160000	8	2020	13
620	146	2020-08-14	2	405000	3240000	8	2020	13
621	147	2020-08-14	2	405000	3240000	8	2020	13
622	148	2020-08-14	2	135000	1080000	8	2020	13
623	149	2020-08-14	2	135000	1080000	8	2020	13
624	150	2020-08-14	2	220000	220000	8	2020	13
625	151	2020-08-14	2	405000	3240000	8	2020	13
626	152	2020-08-14	2	67500	540000	8	2020	13
627	153	2020-08-14	2	405000	3240000	8	2020	13
628	15	2020-08-14	5	185625	1299375	8	2020	13
629	36	2020-08-14	3	215000	1505000	8	2020	13
630	37	2020-08-14	3	286000	2002000	8	2020	13
631	28	2020-08-14	3	143000	1001000	8	2020	13
632	18	2020-08-14	5	185625	1299375	8	2020	13
633	154	2020-08-14	2	190000	1520000	8	2020	13
634	155	2020-08-14	2	405000	3240000	8	2020	13
635	156	2020-08-14	2	202500	1620000	8	2020	13
636	157	2020-08-14	2	135000	1080000	8	2020	13
646	168	2020-08-14	2	202500	1620000	8	2020	13
647	169	2020-08-14	2	202500	1620000	8	2020	13
648	170	2020-08-14	2	202500	1620000	8	2020	13
649	171	2020-08-14	2	270000	2160000	8	2020	13
650	172	2020-08-14	2	100000	800000	8	2020	13
651	54	2020-08-14	3	286000	2002000	8	2020	13
652	50	2020-08-14	3	230000	1610000	8	2020	13
653	10	2020-08-14	5	180000	1800000	8	2020	13
654	27	2020-08-14	3	429000	3003000	8	2020	13
655	62	2020-08-14	3	429000	3003000	8	2020	13
656	56	2020-08-14	3	143000	1001000	8	2020	13
657	173	2020-08-14	2	250000	2000000	8	2020	13
660	40	2020-08-14	3	143000	1001000	8	2020	13
661	51	2020-08-14	3	143000	1001000	8	2020	13
662	175	2020-08-14	2	230000	920000	8	2020	13
663	176	2020-08-14	2	135000	1080000	8	2020	13
664	177	2020-08-14	2	270000	2160000	8	2020	13
665	178	2020-08-14	2	270000	2160000	8	2020	13
666	179	2020-08-14	2	405000	3240000	8	2020	13
667	180	2020-08-14	2	270000	2160000	8	2020	13
668	181	2020-08-14	2	405000	3240000	8	2020	13
669	182	2020-08-14	2	135000	1080000	8	2020	13
670	183	2020-08-14	2	201667	806666	8	2020	13
671	184	2020-08-14	2	135000	1080000	8	2020	13
672	185	2020-08-14	2	405000	3240000	8	2020	13
673	204	2020-08-14	2	135000	1080000	8	2020	13
674	205	2020-08-14	2	337500	2700000	8	2020	13
675	206	2020-08-14	2	405000	3240000	8	2020	13
676	207	2020-08-14	2	135000	1080000	8	2020	13
677	208	2020-08-14	2	201667	806666	8	2020	13
678	209	2020-08-14	2	135000	1080000	8	2020	13
679	210	2020-08-14	2	135000	1080000	8	2020	13
680	211	2020-08-14	2	135000	1080000	8	2020	13
681	212	2020-08-14	2	135000	1080000	8	2020	13
2307	78	2020-08-14	3	143000	1001000	8	2020	13
2315	49	2020-08-14	3	143000	1001000	8	2020	13
2323	55	2020-08-14	3	143000	1001000	8	2020	13
2331	61	2020-08-14	3	143000	1001000	8	2020	13
2339	66	2020-08-14	3	190000	1710000	8	2020	13
2347	68	2020-08-14	3	150000	450000	8	2020	13
2355	70	2020-08-14	3	357000	2499000	8	2020	13
2363	72	2020-08-14	3	286000	2002000	8	2020	13
2371	75	2020-08-14	3	215000	1505000	8	2020	13
2379	79	2020-08-14	3	286000	2002000	8	2020	13
2387	81	2020-08-14	3	143000	1001000	8	2020	13
2395	83	2020-08-14	3	143000	1001000	8	2020	13
1966	183	2020-10-06	4	201667	403332	10	2020	\N
1967	183	2020-10-06	5	201667	201665	10	2020	\N
1968	183	2020-10-06	6	201665	0	10	2020	\N
2403	85	2020-08-14	3	357000	2499000	8	2020	13
2411	87	2020-08-14	3	143000	1001000	8	2020	13
2419	89	2020-08-14	3	286000	2002000	8	2020	13
2427	91	2020-08-14	3	286000	2002000	8	2020	13
2435	93	2020-08-14	3	237000	1185000	8	2020	13
2443	95	2020-08-14	3	68000	476000	8	2020	13
2451	97	2020-08-14	3	228000	1596000	8	2020	13
682	213	2020-08-14	2	202500	1620000	8	2020	13
683	214	2020-08-14	2	270000	2160000	8	2020	13
684	215	2020-08-14	2	405000	3240000	8	2020	13
685	216	2020-08-14	2	202500	1620000	8	2020	13
686	217	2020-08-14	2	270000	2160000	8	2020	13
687	218	2020-08-14	2	405000	3240000	8	2020	13
688	219	2020-08-14	2	67500	540000	8	2020	13
689	220	2020-08-14	2	100000	800000	8	2020	13
694	14	2020-08-14	2	185625	1856250	8	2020	13
698	60	2020-08-14	3	572000	4004000	8	2020	13
699	38	2020-08-14	3	286000	2002000	8	2020	13
700	42	2020-08-14	3	286000	2002000	8	2020	13
701	19	2020-08-14	3	143000	1001000	8	2020	13
702	58	2020-08-14	3	143000	1001000	8	2020	13
703	44	2020-08-14	3	143000	1001000	8	2020	13
704	39	2020-08-14	3	143000	1001000	8	2020	13
705	35	2020-08-14	3	215000	1505000	8	2020	13
706	59	2020-08-14	3	143000	1001000	8	2020	13
707	29	2020-08-14	3	286000	2002000	8	2020	13
708	12	2020-08-14	5	180000	1800000	8	2020	13
709	20	2020-08-14	3	286000	2002000	8	2020	13
710	43	2020-08-14	3	143000	1001000	8	2020	13
711	223	2020-08-14	1	300000	300000	8	2020	13
712	224	2020-08-14	1	300000	300000	8	2020	13
713	225	2020-08-14	1	100000	900000	8	2020	13
714	226	2020-08-14	1	143000	1287000	8	2020	13
715	174	2020-08-14	2	190000	1520000	8	2020	13
716	227	2020-08-14	1	63000	567000	8	2020	13
717	232	2020-08-14	1	200000	1800000	8	2020	13
718	233	2020-08-14	1	190000	1710000	8	2020	13
719	234	2020-08-14	1	396000	4356000	8	2020	13
720	235	2020-08-14	1	171600	1544400	8	2020	13
721	236	2020-08-14	1	219000	2409000	8	2020	13
722	231	2020-08-14	1	280000	2520000	8	2020	13
723	230	2020-08-14	4	143000	858000	8	2020	13
724	237	2020-08-14	1	140000	1260000	8	2020	13
725	238	2020-08-14	1	300000	300000	8	2020	13
726	229	2020-08-14	3	280000	1960000	8	2020	13
1016	247	2020-09-11	1	100833	504167	9	2020	15
1017	268	2020-09-11	1	286000	2574000	9	2020	15
1018	272	2020-09-11	1	286000	2574000	9	2020	15
1019	276	2020-09-11	1	200000	1600000	9	2020	15
1020	280	2020-09-11	1	286000	2574000	9	2020	15
1021	241	2020-09-11	1	405000	3645000	9	2020	15
1022	245	2020-09-11	1	405000	3645000	9	2020	15
1023	249	2020-09-11	1	405000	3645000	9	2020	15
1024	253	2020-09-11	1	202500	1822500	9	2020	15
1025	257	2020-09-11	1	270000	2430000	9	2020	15
1026	265	2020-09-11	1	286000	2574000	9	2020	15
1027	269	2020-09-11	1	143000	1287000	9	2020	15
1028	273	2020-09-11	1	200000	1800000	9	2020	15
1029	277	2020-09-11	1	180000	1620000	9	2020	15
1030	281	2020-09-11	1	105000	525000	9	2020	15
1031	195	2020-09-11	3	405000	2835000	9	2020	15
1043	40	2020-09-11	4	143000	858000	9	2020	15
1044	56	2020-09-11	4	143000	858000	9	2020	15
1045	122	2020-09-11	3	135000	945000	9	2020	15
1046	182	2020-09-11	3	135000	945000	9	2020	15
1047	62	2020-09-11	4	429000	2574000	9	2020	15
1048	222	2020-09-11	3	270000	1890000	9	2020	15
1049	123	2020-09-11	3	270000	1890000	9	2020	15
2308	78	2020-09-11	4	143000	858000	9	2020	15
2316	49	2020-09-11	4	143000	858000	9	2020	15
2324	55	2020-09-11	4	143000	858000	9	2020	15
2332	61	2020-09-11	4	143000	858000	9	2020	15
2340	66	2020-09-11	4	190000	1520000	9	2020	15
2348	68	2020-09-11	4	150000	300000	9	2020	15
2356	70	2020-09-11	4	357000	2142000	9	2020	15
2364	72	2020-09-11	4	286000	1716000	9	2020	15
2372	75	2020-09-11	4	215000	1290000	9	2020	15
2380	79	2020-09-11	4	286000	1716000	9	2020	15
2388	81	2020-09-11	4	143000	858000	9	2020	15
2396	83	2020-09-11	4	143000	858000	9	2020	15
2404	85	2020-09-11	4	357000	2142000	9	2020	15
2412	87	2020-09-11	4	143000	858000	9	2020	15
2420	89	2020-09-11	4	286000	1716000	9	2020	15
2428	91	2020-09-11	4	286000	1716000	9	2020	15
2436	93	2020-09-11	4	237000	948000	9	2020	15
2444	95	2020-09-11	4	68000	408000	9	2020	15
2452	97	2020-09-11	4	228000	1368000	9	2020	15
1051	180	2020-09-11	3	270000	1890000	9	2020	15
1052	23	2020-09-11	4	143000	858000	9	2020	15
1053	127	2020-09-11	3	405000	2835000	9	2020	15
1054	101	2020-09-11	3	105000	735000	9	2020	15
1055	20	2020-09-11	4	286000	1716000	9	2020	15
1056	44	2020-09-11	4	143000	858000	9	2020	15
1058	25	2020-09-11	4	143000	858000	9	2020	15
1059	58	2020-09-11	4	143000	858000	9	2020	15
1060	26	2020-09-11	4	286000	1716000	9	2020	15
1061	189	2020-09-11	3	403333	1210001	9	2020	15
1062	137	2020-09-11	3	380000	2660000	9	2020	15
1063	117	2020-09-11	3	160000	1120000	9	2020	15
1064	13	2020-09-11	6	185625	1113750	9	2020	15
1066	22	2020-09-11	4	215000	1290000	9	2020	15
1069	45	2020-09-11	4	215000	1290000	9	2020	15
1070	238	2020-09-11	2	300000	0	9	2020	15
1071	27	2020-09-11	4	429000	2574000	9	2020	15
1072	225	2020-09-11	2	100000	800000	9	2020	15
1073	199	2020-09-11	3	520000	3120000	9	2020	15
1074	230	2020-09-11	5	143000	715000	9	2020	15
1075	155	2020-09-11	3	405000	2835000	9	2020	15
1076	60	2020-09-11	4	572000	3432000	9	2020	15
1077	165	2020-09-11	3	135000	945000	9	2020	15
1078	149	2020-09-11	3	135000	945000	9	2020	15
1079	105	2020-09-11	3	300000	1500000	9	2020	15
1081	21	2020-09-11	4	215000	1290000	9	2020	15
1083	152	2020-09-11	3	67500	472500	9	2020	15
1085	124	2020-09-11	3	270000	1890000	9	2020	15
1086	205	2020-09-11	3	337500	2362500	9	2020	15
1087	197	2020-09-11	3	405000	2835000	9	2020	15
1088	229	2020-09-11	4	280000	1680000	9	2020	15
1089	187	2020-09-11	3	405000	2835000	9	2020	15
1090	114	2020-09-11	3	143000	1001000	9	2020	15
1091	181	2020-09-11	3	405000	2835000	9	2020	15
1092	43	2020-09-11	4	143000	858000	9	2020	15
1093	191	2020-09-11	3	605000	1815000	9	2020	15
1094	156	2020-09-11	3	202500	1417500	9	2020	15
1095	220	2020-09-11	3	100000	700000	9	2020	15
1096	11	2020-09-11	6	187000	1122000	9	2020	15
1097	166	2020-09-11	3	270000	1890000	9	2020	15
1098	194	2020-09-11	3	270000	1890000	9	2020	15
1099	135	2020-09-11	3	270000	1890000	9	2020	15
1100	126	2020-09-11	3	270000	1890000	9	2020	15
1101	228	2020-09-11	4	143000	858000	9	2020	15
1102	39	2020-09-11	4	143000	858000	9	2020	15
1103	132	2020-09-11	3	270000	1890000	9	2020	15
1104	112	2020-09-11	3	143000	1001000	9	2020	15
1105	131	2020-09-11	3	270000	1890000	9	2020	15
1106	201	2020-09-11	3	270000	1890000	9	2020	15
1108	176	2020-09-11	3	135000	945000	9	2020	15
1111	178	2020-09-11	3	270000	1890000	9	2020	15
1113	169	2020-09-11	3	202500	1417500	9	2020	15
1114	14	2020-09-11	3	185625	1670625	9	2020	15
1115	107	2020-09-11	3	220000	0	9	2020	15
1116	17	2020-09-11	6	277000	1662000	9	2020	15
1118	142	2020-09-11	3	405000	2835000	9	2020	15
1119	224	2020-09-11	2	300000	0	9	2020	15
1120	221	2020-09-11	3	100833	302501	9	2020	15
1121	147	2020-09-11	3	405000	2835000	9	2020	15
1123	160	2020-09-11	3	270000	1890000	9	2020	15
1124	121	2020-09-11	3	405000	2835000	9	2020	15
1125	50	2020-09-11	4	230000	1380000	9	2020	15
1126	33	2020-09-11	4	222000	444000	9	2020	15
1127	210	2020-09-11	3	135000	945000	9	2020	15
1128	161	2020-09-11	3	135000	945000	9	2020	15
1129	19	2020-09-11	4	143000	858000	9	2020	15
1130	108	2020-09-11	3	80000	560000	9	2020	15
1131	157	2020-09-11	3	135000	945000	9	2020	15
2309	41	2020-06-15	1	143000	1287000	6	2020	3
2317	52	2020-06-15	1	230000	2070000	6	2020	3
2325	57	2020-06-15	1	143000	1287000	6	2020	3
2333	65	2020-06-15	1	462000	4158000	6	2020	3
2341	67	2020-06-15	1	326000	3586000	6	2020	3
2349	69	2020-06-15	1	215000	1935000	6	2020	3
2357	71	2020-06-15	1	286000	2574000	6	2020	3
2365	73	2020-06-15	1	215000	1935000	6	2020	3
2373	76	2020-06-15	1	143000	1287000	6	2020	3
2381	80	2020-06-15	1	286000	2574000	6	2020	3
2389	82	2020-06-15	1	286000	2574000	6	2020	3
2397	84	2020-06-15	1	143000	1287000	6	2020	3
2405	86	2020-06-15	1	143000	1287000	6	2020	3
2413	88	2020-06-15	1	357000	3213000	6	2020	3
2421	90	2020-06-15	1	714000	6426000	6	2020	3
2429	92	2020-06-15	1	143000	1287000	6	2020	3
2437	94	2020-06-15	1	236000	2124000	6	2020	3
2445	96	2020-06-15	1	78000	702000	6	2020	3
312	199	2020-07-16	1	520000	4160000	7	2020	12
313	200	2020-07-16	1	221000	1989000	7	2020	12
314	201	2020-07-16	1	270000	2430000	7	2020	12
315	202	2020-07-16	1	202500	1822500	7	2020	12
316	203	2020-07-16	1	405000	3645000	7	2020	12
317	221	2020-07-16	1	100833	504167	7	2020	12
336	98	2020-07-16	1	100000	900000	7	2020	12
337	99	2020-07-16	1	64000	576000	7	2020	12
338	101	2020-07-16	1	105000	945000	7	2020	12
339	102	2020-07-16	1	286000	2574000	7	2020	12
340	103	2020-07-16	1	103000	721000	7	2020	12
341	104	2020-07-16	1	446000	4014000	7	2020	12
342	105	2020-07-16	1	300000	2100000	7	2020	12
343	106	2020-07-16	1	357000	3213000	7	2020	12
344	107	2020-07-16	1	220000	440000	7	2020	12
345	108	2020-07-16	1	80000	720000	7	2020	12
346	109	2020-07-16	1	429000	3861000	7	2020	12
347	129	2020-07-16	1	135000	1215000	7	2020	12
348	130	2020-07-16	1	270000	2430000	7	2020	12
349	131	2020-07-16	1	270000	2430000	7	2020	12
350	132	2020-07-16	1	270000	2430000	7	2020	12
351	133	2020-07-16	1	135000	1215000	7	2020	12
352	134	2020-07-16	1	135000	1215000	7	2020	12
353	135	2020-07-16	1	270000	2430000	7	2020	12
354	136	2020-07-16	1	270000	2430000	7	2020	12
355	137	2020-07-16	1	380000	3420000	7	2020	12
356	138	2020-07-16	1	270000	2430000	7	2020	12
357	139	2020-07-16	1	270000	2430000	7	2020	12
358	141	2020-07-16	1	135000	1215000	7	2020	12
359	110	2020-07-16	1	216000	1944000	7	2020	12
360	111	2020-07-16	1	143000	1287000	7	2020	12
361	112	2020-07-16	1	143000	1287000	7	2020	12
362	113	2020-07-16	1	215000	1935000	7	2020	12
363	114	2020-07-16	1	143000	1287000	7	2020	12
364	115	2020-07-16	1	429000	3861000	7	2020	12
365	116	2020-07-16	1	714000	6426000	7	2020	12
366	117	2020-07-16	1	160000	1440000	7	2020	12
367	118	2020-07-16	1	80000	720000	7	2020	12
368	119	2020-07-16	1	104000	936000	7	2020	12
369	120	2020-07-16	1	180000	1620000	7	2020	12
370	222	2020-07-16	1	270000	2430000	7	2020	12
371	121	2020-07-16	1	405000	3645000	7	2020	12
372	122	2020-07-16	1	135000	1215000	7	2020	12
373	123	2020-07-16	1	270000	2430000	7	2020	12
374	124	2020-07-16	1	270000	2430000	7	2020	12
375	125	2020-07-16	1	135000	1215000	7	2020	12
376	126	2020-07-16	1	270000	2430000	7	2020	12
377	127	2020-07-16	1	405000	3645000	7	2020	12
378	48	2020-07-16	2	286000	2288000	7	2020	12
379	32	2020-07-16	2	286000	2288000	7	2020	12
380	6	2020-07-16	4	185583	1484668	7	2020	12
381	22	2020-07-16	2	215000	1720000	7	2020	12
382	45	2020-07-16	2	215000	1720000	7	2020	12
383	24	2020-07-16	2	429000	3432000	7	2020	12
385	47	2020-07-16	2	286000	2288000	7	2020	12
386	9	2020-07-16	4	135000	810000	7	2020	12
387	7	2020-07-16	4	150000	1200000	7	2020	12
388	26	2020-07-16	2	286000	2288000	7	2020	12
389	11	2020-07-16	4	187000	1496000	7	2020	12
390	142	2020-07-16	1	405000	3645000	7	2020	12
391	143	2020-07-16	1	250000	2250000	7	2020	12
392	144	2020-07-16	1	270000	2430000	7	2020	12
393	145	2020-07-16	1	270000	2430000	7	2020	12
394	146	2020-07-16	1	405000	3645000	7	2020	12
395	147	2020-07-16	1	405000	3645000	7	2020	12
396	148	2020-07-16	1	135000	1215000	7	2020	12
397	149	2020-07-16	1	135000	1215000	7	2020	12
398	150	2020-07-16	1	220000	440000	7	2020	12
399	151	2020-07-16	1	405000	3645000	7	2020	12
400	152	2020-07-16	1	67500	607500	7	2020	12
2310	41	2020-07-16	2	143000	1144000	7	2020	12
2318	52	2020-07-16	2	230000	1840000	7	2020	12
2326	57	2020-07-16	2	143000	1144000	7	2020	12
2334	65	2020-07-16	2	462000	3696000	7	2020	12
2342	67	2020-07-16	2	326000	3260000	7	2020	12
2350	69	2020-07-16	2	215000	1720000	7	2020	12
2358	71	2020-07-16	2	286000	2288000	7	2020	12
2366	73	2020-07-16	2	215000	1720000	7	2020	12
2374	76	2020-07-16	2	143000	1144000	7	2020	12
2382	80	2020-07-16	2	286000	2288000	7	2020	12
2390	82	2020-07-16	2	286000	2288000	7	2020	12
2398	84	2020-07-16	2	143000	1144000	7	2020	12
2406	86	2020-07-16	2	143000	1144000	7	2020	12
2414	88	2020-07-16	2	357000	2856000	7	2020	12
2422	90	2020-07-16	2	714000	5712000	7	2020	12
2430	92	2020-07-16	2	143000	1144000	7	2020	12
2438	94	2020-07-16	2	236000	1888000	7	2020	12
2446	96	2020-07-16	2	78000	624000	7	2020	12
401	153	2020-07-16	1	405000	3645000	7	2020	12
402	154	2020-07-16	1	190000	1710000	7	2020	12
403	155	2020-07-16	1	405000	3645000	7	2020	12
404	156	2020-07-16	1	202500	1822500	7	2020	12
405	157	2020-07-16	1	135000	1215000	7	2020	12
406	158	2020-07-16	1	135000	1215000	7	2020	12
407	159	2020-07-16	1	135000	1215000	7	2020	12
408	160	2020-07-16	1	270000	2430000	7	2020	12
409	161	2020-07-16	1	135000	1215000	7	2020	12
420	54	2020-07-16	2	286000	2288000	7	2020	12
421	50	2020-07-16	2	230000	1840000	7	2020	12
422	10	2020-07-16	4	180000	1980000	7	2020	12
423	27	2020-07-16	2	429000	3432000	7	2020	12
424	62	2020-07-16	2	429000	3432000	7	2020	12
425	56	2020-07-16	2	143000	1144000	7	2020	12
426	15	2020-07-16	4	185625	1485000	7	2020	12
427	36	2020-07-16	2	215000	1720000	7	2020	12
428	37	2020-07-16	2	286000	2288000	7	2020	12
429	28	2020-07-16	2	143000	1144000	7	2020	12
430	30	2020-07-16	2	143000	1144000	7	2020	12
431	18	2020-07-16	4	185625	1485000	7	2020	12
432	173	2020-07-16	1	250000	2250000	7	2020	12
433	174	2020-07-16	1	190000	1710000	7	2020	12
434	175	2020-07-16	1	230000	1150000	7	2020	12
435	176	2020-07-16	1	135000	1215000	7	2020	12
436	177	2020-07-16	1	270000	2430000	7	2020	12
437	178	2020-07-16	1	270000	2430000	7	2020	12
438	179	2020-07-16	1	405000	3645000	7	2020	12
439	180	2020-07-16	1	270000	2430000	7	2020	12
440	181	2020-07-16	1	405000	3645000	7	2020	12
441	182	2020-07-16	1	135000	1215000	7	2020	12
442	183	2020-07-16	1	201667	1008333	7	2020	12
443	184	2020-07-16	1	135000	1215000	7	2020	12
444	185	2020-07-16	1	405000	3645000	7	2020	12
445	204	2020-07-16	1	135000	1215000	7	2020	12
446	205	2020-07-16	1	337500	3037500	7	2020	12
447	206	2020-07-16	1	405000	3645000	7	2020	12
448	207	2020-07-16	1	135000	1215000	7	2020	12
449	208	2020-07-16	1	201667	1008333	7	2020	12
450	209	2020-07-16	1	135000	1215000	7	2020	12
451	210	2020-07-16	1	135000	1215000	7	2020	12
452	211	2020-07-16	1	135000	1215000	7	2020	12
453	212	2020-07-16	1	135000	1215000	7	2020	12
454	213	2020-07-16	1	202500	1822500	7	2020	12
455	214	2020-07-16	1	270000	2430000	7	2020	12
456	215	2020-07-16	1	405000	3645000	7	2020	12
457	216	2020-07-16	1	202500	1822500	7	2020	12
458	217	2020-07-16	1	270000	2430000	7	2020	12
459	218	2020-07-16	1	405000	3645000	7	2020	12
460	219	2020-07-16	1	67500	607500	7	2020	12
461	220	2020-07-16	1	100000	900000	7	2020	12
466	14	2020-07-16	1	185625	2041875	7	2020	12
471	40	2020-07-16	2	143000	1144000	7	2020	12
472	51	2020-07-16	2	143000	1144000	7	2020	12
474	60	2020-07-16	2	572000	4576000	7	2020	12
475	38	2020-07-16	2	286000	2288000	7	2020	12
476	42	2020-07-16	2	286000	2288000	7	2020	12
477	19	2020-07-16	2	143000	1144000	7	2020	12
478	58	2020-07-16	2	143000	1144000	7	2020	12
479	44	2020-07-16	2	143000	1144000	7	2020	12
480	39	2020-07-16	2	143000	1144000	7	2020	12
481	35	2020-07-16	2	215000	1720000	7	2020	12
482	59	2020-07-16	2	143000	1144000	7	2020	12
483	29	2020-07-16	2	286000	2288000	7	2020	12
484	12	2020-07-16	4	180000	1980000	7	2020	12
485	20	2020-07-16	2	286000	2288000	7	2020	12
486	43	2020-07-16	2	143000	1144000	7	2020	12
502	30	2020-08-14	3	1000000	144000	8	2020	13
503	46	2020-08-14	3	215000	1505000	8	2020	13
504	23	2020-08-14	3	143000	1001000	8	2020	13
2311	41	2020-08-14	3	143000	1001000	8	2020	13
2319	52	2020-08-14	3	230000	1610000	8	2020	13
2327	57	2020-08-14	3	143000	1001000	8	2020	13
2335	65	2020-08-14	3	462000	3234000	8	2020	13
2343	67	2020-08-14	3	326000	2934000	8	2020	13
2351	69	2020-08-14	3	215000	1505000	8	2020	13
2359	71	2020-08-14	3	286000	2002000	8	2020	13
2367	73	2020-08-14	3	215000	1505000	8	2020	13
2375	76	2020-08-14	3	143000	1001000	8	2020	13
2383	80	2020-08-14	3	286000	2002000	8	2020	13
2391	82	2020-08-14	3	286000	2002000	8	2020	13
2399	84	2020-08-14	3	143000	1001000	8	2020	13
2407	86	2020-08-14	3	143000	1001000	8	2020	13
2415	88	2020-08-14	3	357000	2499000	8	2020	13
2423	90	2020-08-14	3	714000	4998000	8	2020	13
2431	92	2020-08-14	3	143000	1001000	8	2020	13
2439	94	2020-08-14	3	236000	1652000	8	2020	13
2447	96	2020-08-14	3	78000	546000	8	2020	13
275	13	2020-07-16	4	185625	1485000	7	2020	12
276	64	2020-07-16	2	143000	1144000	7	2020	12
278	25	2020-07-16	2	143000	1144000	7	2020	12
279	46	2020-07-16	2	215000	1720000	7	2020	12
280	23	2020-07-16	2	143000	1144000	7	2020	12
281	17	2020-07-16	4	277000	2216000	7	2020	12
282	21	2020-07-16	2	215000	1720000	7	2020	12
283	16	2020-07-16	4	225000	1800000	7	2020	12
284	31	2020-07-16	2	143000	1144000	7	2020	12
285	8	2020-07-16	4	571000	6281000	7	2020	12
286	33	2020-07-16	2	222000	888000	7	2020	12
297	100	2020-07-16	1	250000	2250000	7	2020	12
298	140	2020-07-16	1	270000	2430000	7	2020	12
299	163	2020-07-16	1	135000	1215000	7	2020	12
300	186	2020-07-16	1	67500	607500	7	2020	12
301	187	2020-07-16	1	405000	3645000	7	2020	12
505	17	2020-08-14	5	277000	1939000	8	2020	13
506	21	2020-08-14	3	215000	1505000	8	2020	13
507	16	2020-08-14	5	225000	1575000	8	2020	13
508	31	2020-08-14	3	143000	1001000	8	2020	13
509	8	2020-08-14	5	571000	5710000	8	2020	13
510	33	2020-08-14	3	222000	666000	8	2020	13
514	13	2020-08-14	5	185625	1299375	8	2020	13
515	64	2020-08-14	3	143000	1001000	8	2020	13
516	25	2020-08-14	3	143000	1001000	8	2020	13
523	100	2020-08-14	2	250000	2000000	8	2020	13
524	140	2020-08-14	2	270000	2160000	8	2020	13
525	163	2020-08-14	2	135000	1080000	8	2020	13
526	186	2020-08-14	2	67500	540000	8	2020	13
527	187	2020-08-14	2	405000	3240000	8	2020	13
528	188	2020-08-14	2	405000	3240000	8	2020	13
529	189	2020-08-14	2	403333	1613334	8	2020	13
540	201	2020-08-14	2	270000	2160000	8	2020	13
541	202	2020-08-14	2	202500	1620000	8	2020	13
542	203	2020-08-14	2	405000	3240000	8	2020	13
543	221	2020-08-14	2	100833	403334	8	2020	13
561	98	2020-08-14	2	100000	800000	8	2020	13
562	99	2020-08-14	2	64000	512000	8	2020	13
563	101	2020-08-14	2	105000	840000	8	2020	13
564	102	2020-08-14	2	286000	2288000	8	2020	13
565	103	2020-08-14	2	103000	618000	8	2020	13
566	104	2020-08-14	2	446000	3568000	8	2020	13
302	188	2020-07-16	1	405000	3645000	7	2020	12
303	189	2020-07-16	1	403333	2016667	7	2020	12
304	191	2020-07-16	1	605000	3025000	7	2020	12
305	192	2020-07-16	1	405000	3645000	7	2020	12
306	193	2020-07-16	1	337500	3037500	7	2020	12
307	194	2020-07-16	1	270000	2430000	7	2020	12
308	195	2020-07-16	1	405000	3645000	7	2020	12
309	196	2020-07-16	1	403333	2016667	7	2020	12
310	197	2020-07-16	1	405000	3645000	7	2020	12
311	198	2020-07-16	1	230000	2070000	7	2020	12
410	162	2020-07-16	1	135000	1215000	7	2020	12
411	164	2020-07-16	1	202500	1822500	7	2020	12
412	165	2020-07-16	1	135000	1215000	7	2020	12
413	166	2020-07-16	1	270000	2430000	7	2020	12
414	167	2020-07-16	1	135000	1215000	7	2020	12
415	168	2020-07-16	1	202500	1822500	7	2020	12
416	169	2020-07-16	1	202500	1822500	7	2020	12
417	170	2020-07-16	1	202500	1822500	7	2020	12
418	171	2020-07-16	1	270000	2430000	7	2020	12
419	172	2020-07-16	1	100000	900000	7	2020	12
530	191	2020-08-14	2	605000	2420000	8	2020	13
531	192	2020-08-14	2	405000	3240000	8	2020	13
532	193	2020-08-14	2	337500	2700000	8	2020	13
533	194	2020-08-14	2	270000	2160000	8	2020	13
534	195	2020-08-14	2	405000	3240000	8	2020	13
535	196	2020-08-14	2	403333	1613334	8	2020	13
536	197	2020-08-14	2	405000	3240000	8	2020	13
537	198	2020-08-14	2	230000	1840000	8	2020	13
538	199	2020-08-14	2	520000	3640000	8	2020	13
539	200	2020-08-14	2	221000	1768000	8	2020	13
637	158	2020-08-14	2	135000	1080000	8	2020	13
638	159	2020-08-14	2	135000	1080000	8	2020	13
639	160	2020-08-14	2	270000	2160000	8	2020	13
640	161	2020-08-14	2	135000	1080000	8	2020	13
641	162	2020-08-14	2	135000	1080000	8	2020	13
642	164	2020-08-14	2	202500	1620000	8	2020	13
643	165	2020-08-14	2	135000	1080000	8	2020	13
644	166	2020-08-14	2	270000	2160000	8	2020	13
645	167	2020-08-14	2	135000	1080000	8	2020	13
1033	15	2020-09-11	6	185625	1113750	9	2020	15
1034	174	2020-09-11	3	190000	1330000	9	2020	15
1035	204	2020-09-11	3	135000	945000	9	2020	15
1036	125	2020-09-11	3	135000	945000	9	2020	15
1037	151	2020-09-11	3	405000	2835000	9	2020	15
1038	140	2020-09-11	3	270000	1890000	9	2020	15
1039	162	2020-09-11	3	135000	945000	9	2020	15
1040	119	2020-09-11	3	104000	728000	9	2020	15
1041	153	2020-09-11	3	405000	2835000	9	2020	15
1042	200	2020-09-11	3	221000	1547000	9	2020	15
1132	154	2020-09-11	3	190000	1330000	9	2020	15
1134	109	2020-09-11	3	429000	3003000	9	2020	15
1135	51	2020-09-11	4	143000	858000	9	2020	15
1136	215	2020-09-11	3	405000	2835000	9	2020	15
1137	31	2020-09-11	4	143000	858000	9	2020	15
1138	35	2020-09-11	4	215000	1290000	9	2020	15
1157	18	2020-09-11	6	185625	1113750	9	2020	15
1158	134	2020-09-11	3	135000	945000	9	2020	15
1159	59	2020-09-11	4	143000	858000	9	2020	15
1161	139	2020-09-11	3	270000	1890000	9	2020	15
1162	146	2020-09-11	3	405000	2835000	9	2020	15
1163	192	2020-09-11	3	405000	2835000	9	2020	15
1164	143	2020-09-11	3	250000	1750000	9	2020	15
1165	98	2020-09-11	3	100000	700000	9	2020	15
1166	193	2020-09-11	3	337500	2362500	9	2020	15
1167	133	2020-09-11	3	135000	945000	9	2020	15
1168	150	2020-09-11	3	220000	0	9	2020	15
1169	100	2020-09-11	3	250000	1750000	9	2020	15
1170	232	2020-09-11	2	200000	1600000	9	2020	15
1171	167	2020-09-11	3	135000	945000	9	2020	15
1172	158	2020-09-11	3	135000	945000	9	2020	15
1173	113	2020-09-11	3	215000	1505000	9	2020	15
1174	116	2020-09-11	3	714000	4998000	9	2020	15
1175	172	2020-09-11	3	100000	700000	9	2020	15
1176	9	2020-09-11	6	135000	540000	9	2020	15
1177	183	2020-09-11	3	201667	604999	9	2020	15
1178	24	2020-09-11	4	429000	2574000	9	2020	15
1179	184	2020-09-11	3	135000	945000	9	2020	15
1180	64	2020-09-11	4	143000	858000	9	2020	15
1181	170	2020-09-11	3	202500	1417500	9	2020	15
1182	129	2020-09-11	3	135000	945000	9	2020	15
1183	106	2020-09-11	3	357000	2499000	9	2020	15
1184	120	2020-09-11	3	180000	1260000	9	2020	15
1185	171	2020-09-11	3	270000	1890000	9	2020	15
1186	227	2020-09-11	2	63000	504000	9	2020	15
1187	8	2020-09-11	6	571000	5139000	9	2020	15
1188	138	2020-09-11	3	270000	1890000	9	2020	15
1190	16	2020-09-11	6	225000	1350000	9	2020	15
1191	179	2020-09-11	3	405000	2835000	9	2020	15
1192	54	2020-09-11	4	286000	1716000	9	2020	15
1193	47	2020-09-11	4	286000	1716000	9	2020	15
1194	103	2020-09-11	3	103000	515000	9	2020	15
1195	209	2020-09-11	3	135000	945000	9	2020	15
1196	115	2020-09-11	3	429000	3003000	9	2020	15
1197	148	2020-09-11	3	135000	945000	9	2020	15
1199	235	2020-09-11	2	171600	1372800	9	2020	15
1203	118	2020-09-11	3	80000	560000	9	2020	15
1204	145	2020-09-11	3	270000	1890000	9	2020	15
1205	130	2020-09-11	3	270000	1890000	9	2020	15
1206	38	2020-09-11	4	286000	1716000	9	2020	15
1207	223	2020-09-11	2	300000	0	9	2020	15
1208	207	2020-09-11	3	135000	945000	9	2020	15
1209	198	2020-09-11	3	230000	1610000	9	2020	15
1210	104	2020-09-11	3	446000	3122000	9	2020	15
1211	163	2020-09-11	3	135000	945000	9	2020	15
1212	217	2020-09-11	3	270000	1890000	9	2020	15
1213	231	2020-09-11	2	280000	2240000	9	2020	15
1214	6	2020-09-11	6	185583	1113502	9	2020	15
1215	102	2020-09-11	3	286000	2002000	9	2020	15
1216	212	2020-09-11	3	135000	945000	9	2020	15
1218	29	2020-09-11	4	286000	1716000	9	2020	15
1219	159	2020-09-11	3	135000	945000	9	2020	15
1221	246	2020-09-11	1	605000	3025000	9	2020	15
1222	250	2020-09-11	1	100833	504167	9	2020	15
1223	254	2020-09-11	1	135000	1215000	9	2020	15
1224	258	2020-09-11	1	405000	3645000	9	2020	15
1225	290	2020-09-11	1	405000	3645000	9	2020	15
1226	292	2020-09-11	1	405000	3645000	9	2020	15
1227	293	2020-09-11	1	135000	1215000	9	2020	15
1228	294	2020-09-11	1	155000	1395000	9	2020	15
1229	296	2020-09-11	1	550000	550000	9	2020	15
1230	297	2020-09-11	1	550000	550000	9	2020	15
1231	298	2020-09-11	1	230000	1150000	9	2020	15
1232	299	2020-09-11	1	143000	1287000	9	2020	15
1233	300	2020-09-11	1	215000	1075000	9	2020	15
1143	37	2020-09-11	4	286000	1716000	9	2020	15
1144	203	2020-09-11	3	405000	2835000	9	2020	15
1146	173	2020-09-11	3	250000	1750000	9	2020	15
1148	219	2020-09-11	3	67500	472500	9	2020	15
1149	32	2020-09-11	4	286000	1716000	9	2020	15
1150	12	2020-09-11	6	180000	1620000	9	2020	15
1151	188	2020-09-11	3	405000	2835000	9	2020	15
1152	10	2020-09-11	6	180000	1620000	9	2020	15
1154	42	2020-09-11	4	286000	1716000	9	2020	15
1155	164	2020-09-11	3	202500	1417500	9	2020	15
1234	301	2020-09-11	1	143000	1287000	9	2020	15
1235	303	2020-09-11	1	230000	1150000	9	2020	15
1236	304	2020-09-11	1	215000	1075000	9	2020	15
1237	305	2020-09-11	1	300000	900000	9	2020	15
1238	306	2020-09-11	1	338000	3718000	9	2020	15
1239	251	2020-09-11	1	201667	1008333	9	2020	15
1240	255	2020-09-11	1	605000	3025000	9	2020	15
1241	259	2020-09-11	1	405000	3645000	9	2020	15
1242	263	2020-09-11	1	125000	1125000	9	2020	15
1243	267	2020-09-11	1	429000	3861000	9	2020	15
1244	271	2020-09-11	1	286000	2574000	9	2020	15
1245	275	2020-09-11	1	215000	1935000	9	2020	15
1246	279	2020-09-11	1	286000	2574000	9	2020	15
1252	264	2020-09-11	1	286000	2574000	9	2020	15
1253	236	2020-09-11	2	219000	2190000	9	2020	15
1254	110	2020-09-11	3	216000	1512000	9	2020	15
1255	208	2020-09-11	3	201667	604999	9	2020	15
1256	175	2020-09-11	3	230000	690000	9	2020	15
1257	211	2020-09-11	3	135000	945000	9	2020	15
1258	46	2020-09-11	4	215000	1290000	9	2020	15
1259	99	2020-09-11	3	64000	448000	9	2020	15
1260	48	2020-09-11	4	286000	1716000	9	2020	15
1261	28	2020-09-11	4	143000	858000	9	2020	15
1262	239	2020-09-11	1	337500	3037500	9	2020	15
1263	243	2020-09-11	1	405000	3645000	9	2020	15
1264	261	2020-09-11	1	100833	504167	9	2020	15
1266	36	2020-09-11	4	215000	1290000	9	2020	15
1267	262	2020-09-11	1	270000	2430000	9	2020	15
1268	266	2020-09-11	1	143000	1287000	9	2020	15
1269	270	2020-09-11	1	429000	3861000	9	2020	15
1270	274	2020-09-11	1	180000	180000	9	2020	15
1271	278	2020-09-11	1	143000	1287000	9	2020	15
567	105	2020-08-14	2	300000	1800000	8	2020	13
568	106	2020-08-14	2	357000	2856000	8	2020	13
569	107	2020-08-14	2	220000	220000	8	2020	13
570	108	2020-08-14	2	80000	640000	8	2020	13
571	109	2020-08-14	2	429000	3432000	8	2020	13
572	6	2020-08-14	5	185583	1299085	8	2020	13
573	22	2020-08-14	3	215000	1505000	8	2020	13
574	45	2020-08-14	3	215000	1505000	8	2020	13
575	24	2020-08-14	3	429000	3003000	8	2020	13
576	47	2020-08-14	3	286000	2002000	8	2020	13
577	9	2020-08-14	5	135000	675000	8	2020	13
578	7	2020-08-14	5	150000	1050000	8	2020	13
579	26	2020-08-14	3	286000	2002000	8	2020	13
580	11	2020-08-14	5	187000	1309000	8	2020	13
581	129	2020-08-14	2	135000	1080000	8	2020	13
582	130	2020-08-14	2	270000	2160000	8	2020	13
583	131	2020-08-14	2	270000	2160000	8	2020	13
584	132	2020-08-14	2	270000	2160000	8	2020	13
585	133	2020-08-14	2	135000	1080000	8	2020	13
586	134	2020-08-14	2	135000	1080000	8	2020	13
589	135	2020-08-14	2	270000	2160000	8	2020	13
590	136	2020-08-14	2	270000	2160000	8	2020	13
591	137	2020-08-14	2	380000	3040000	8	2020	13
592	138	2020-08-14	2	270000	2160000	8	2020	13
593	139	2020-08-14	2	270000	2160000	8	2020	13
594	141	2020-08-14	2	135000	1080000	8	2020	13
595	110	2020-08-14	2	216000	1728000	8	2020	13
596	111	2020-08-14	2	143000	1144000	8	2020	13
597	112	2020-08-14	2	143000	1144000	8	2020	13
598	113	2020-08-14	2	215000	1720000	8	2020	13
599	114	2020-08-14	2	143000	1144000	8	2020	13
1247	244	2020-09-11	1	201667	1008333	9	2020	15
1248	248	2020-09-11	1	270000	2430000	9	2020	15
1249	252	2020-09-11	1	270000	2430000	9	2020	15
1250	256	2020-09-11	1	201667	1008333	9	2020	15
1251	260	2020-09-11	1	180000	180000	9	2020	15
1272	282	2020-09-11	1	270000	2430000	9	2020	15
1273	283	2020-09-11	1	265000	2385000	9	2020	15
1274	284	2020-09-11	1	270000	2430000	9	2020	15
1275	285	2020-09-11	1	270000	2430000	9	2020	15
1276	286	2020-09-11	1	405000	3645000	9	2020	15
1277	287	2020-09-11	1	405000	3645000	9	2020	15
1278	288	2020-09-11	1	270000	2430000	9	2020	15
1279	289	2020-09-11	1	135000	1215000	9	2020	15
1280	30	2020-09-11	4	143000	1000	9	2020	15
1283	214	2020-09-11	3	270000	1890000	9	2020	15
1284	213	2020-09-11	3	202500	1417500	9	2020	15
1285	185	2020-09-11	3	405000	2835000	9	2020	15
1286	233	2020-09-11	2	190000	1520000	9	2020	15
1287	111	2020-09-11	3	143000	1001000	9	2020	15
1288	206	2020-09-11	3	405000	2835000	9	2020	15
1289	237	2020-09-11	2	140000	1120000	9	2020	15
1290	168	2020-09-11	3	202500	1417500	9	2020	15
1291	136	2020-09-11	3	270000	1890000	9	2020	15
1293	226	2020-09-11	2	143000	1144000	9	2020	15
1294	196	2020-09-11	3	403333	1210001	9	2020	15
1295	218	2020-09-11	3	405000	2835000	9	2020	15
1297	186	2020-09-11	3	67500	472500	9	2020	15
1298	216	2020-09-11	3	202500	1417500	9	2020	15
1299	234	2020-09-11	2	396000	3960000	9	2020	15
1300	177	2020-09-11	3	270000	1890000	9	2020	15
1301	202	2020-09-11	3	202500	1417500	9	2020	15
1302	141	2020-09-11	3	135000	945000	9	2020	15
1303	144	2020-09-11	3	270000	1890000	9	2020	15
1304	7	2020-09-11	6	150000	900000	9	2020	15
2312	41	2020-09-11	4	143000	858000	9	2020	15
2320	52	2020-09-11	4	230000	1380000	9	2020	15
2328	57	2020-09-11	4	143000	858000	9	2020	15
2336	65	2020-09-11	4	462000	2772000	9	2020	15
2344	67	2020-09-11	4	326000	2608000	9	2020	15
2352	69	2020-09-11	4	215000	1290000	9	2020	15
2360	71	2020-09-11	4	286000	1716000	9	2020	15
2368	73	2020-09-11	4	215000	1290000	9	2020	15
2376	76	2020-09-11	4	143000	858000	9	2020	15
2384	80	2020-09-11	4	286000	1716000	9	2020	15
2392	82	2020-09-11	4	286000	1716000	9	2020	15
2400	84	2020-09-11	4	143000	858000	9	2020	15
2408	86	2020-09-11	4	143000	858000	9	2020	15
2416	88	2020-09-11	4	357000	2142000	9	2020	15
2424	90	2020-09-11	4	714000	4284000	9	2020	15
2432	92	2020-09-11	4	143000	858000	9	2020	15
2440	94	2020-09-11	4	236000	1416000	9	2020	15
2448	96	2020-09-11	4	78000	468000	9	2020	15
\.


--
-- TOC entry 2444 (class 0 OID 0)
-- Dependencies: 192
-- Name: ordenes_compras_pagos_id_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.ordenes_compras_pagos_id_seq', 2452, true);


--
-- TOC entry 2383 (class 0 OID 28802)
-- Dependencies: 193
-- Data for Name: ordenes_compras_pagos_seguro; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.ordenes_compras_pagos_seguro (id, fecha, monto_seguro, mes, agno, socio) FROM stdin;
290	2020-09-11	10000	9	2020	403
291	2020-09-11	10000	9	2020	409
292	2020-09-11	10000	9	2020	413
293	2020-09-11	10000	9	2020	96
294	2020-09-11	10000	9	2020	156
295	2020-09-11	10000	9	2020	400
296	2020-09-11	10000	9	2020	401
297	2020-09-11	10000	9	2020	404
298	2020-09-11	10000	9	2020	291
299	2020-09-11	10000	9	2020	407
300	2020-09-11	10000	9	2020	408
301	2020-09-11	10000	9	2020	410
302	2020-09-11	10000	9	2020	52
303	2020-09-11	10000	9	2020	154
304	2020-09-11	10000	9	2020	415
305	2020-09-11	10000	9	2020	128
306	2020-09-11	10000	9	2020	182
307	2020-09-11	10000	9	2020	206
308	2020-09-11	10000	9	2020	218
309	2020-09-11	10000	9	2020	395
310	2020-09-11	10000	9	2020	171
311	2020-09-11	10000	9	2020	236
312	2020-09-11	10000	9	2020	35
313	2020-09-11	10000	9	2020	83
314	2020-09-11	10000	9	2020	74
315	2020-09-11	10000	9	2020	87
316	2020-09-11	10000	9	2020	226
317	2020-09-11	10000	9	2020	39
318	2020-09-11	10000	9	2020	189
319	2020-09-11	10000	9	2020	178
320	2020-09-11	10000	9	2020	284
321	2020-09-11	10000	9	2020	2
322	2020-09-11	10000	9	2020	216
323	2020-09-11	10000	9	2020	123
324	2020-09-11	10000	9	2020	255
325	2020-09-11	10000	9	2020	78
326	2020-09-11	10000	9	2020	187
327	2020-09-11	10000	9	2020	22
328	2020-09-11	10000	9	2020	122
329	2020-09-11	10000	9	2020	90
330	2020-09-11	10000	9	2020	283
331	2020-09-11	10000	9	2020	77
332	2020-09-11	10000	9	2020	153
333	2020-09-11	10000	9	2020	237
334	2020-09-11	10000	9	2020	70
335	2020-09-11	10000	9	2020	235
336	2020-09-11	10000	9	2020	56
337	2020-09-11	10000	9	2020	343
338	2020-09-11	10000	9	2020	241
339	2020-09-11	10000	9	2020	232
340	2020-09-11	10000	9	2020	121
341	2020-09-11	10000	9	2020	389
342	2020-09-11	10000	9	2020	46
343	2020-09-11	10000	9	2020	49
344	2020-09-11	10000	9	2020	48
345	2020-09-11	10000	9	2020	136
346	2020-09-11	10000	9	2020	328
347	2020-09-11	10000	9	2020	388
348	2020-09-11	10000	9	2020	351
349	2020-09-11	10000	9	2020	54
350	2020-09-11	10000	9	2020	394
351	2020-09-11	10000	9	2020	159
352	2020-09-11	10000	9	2020	233
353	2020-09-11	10000	9	2020	210
354	2020-09-11	10000	9	2020	347
355	2020-09-11	10000	9	2020	92
356	2020-09-11	10000	9	2020	82
357	2020-09-11	10000	9	2020	135
358	2020-09-11	10000	9	2020	194
359	2020-09-11	10000	9	2020	113
360	2020-09-11	10000	9	2020	119
361	2020-09-11	10000	9	2020	243
362	2020-09-11	10000	9	2020	172
363	2020-09-11	10000	9	2020	19
364	2020-09-11	10000	9	2020	242
365	2020-09-11	10000	9	2020	234
366	2020-09-11	10000	9	2020	72
367	2020-09-11	10000	9	2020	6
368	2020-09-11	10000	9	2020	69
369	2020-09-11	10000	9	2020	60
370	2020-09-11	10000	9	2020	254
371	2020-09-11	10000	9	2020	45
372	2020-09-11	10000	9	2020	91
373	2020-09-11	10000	9	2020	238
374	2020-09-11	10000	9	2020	57
375	2020-09-11	10000	9	2020	102
376	2020-09-11	10000	9	2020	214
377	2020-09-11	10000	9	2020	133
378	2020-09-11	10000	9	2020	130
379	2020-09-11	10000	9	2020	192
380	2020-09-11	10000	9	2020	139
381	2020-09-11	10000	9	2020	240
382	2020-09-11	10000	9	2020	239
383	2020-09-11	10000	9	2020	115
384	2020-09-11	10000	9	2020	212
385	2020-09-11	10000	9	2020	34
386	2020-09-11	10000	9	2020	5
387	2020-09-11	10000	9	2020	336
388	2020-09-11	10000	9	2020	165
389	2020-09-11	10000	9	2020	114
390	2020-09-11	10000	9	2020	100
391	2020-09-11	10000	9	2020	229
392	2020-09-11	10000	9	2020	213
393	2020-09-11	10000	9	2020	141
394	2020-09-11	10000	9	2020	117
395	2020-09-11	10000	9	2020	326
396	2020-09-11	10000	9	2020	345
397	2020-09-11	10000	9	2020	253
398	2020-09-11	10000	9	2020	152
399	2020-09-11	10000	9	2020	4
400	2020-09-11	10000	9	2020	245
401	2020-09-11	10000	9	2020	108
402	2020-09-11	10000	9	2020	86
403	2020-09-11	10000	9	2020	29
404	2020-09-11	10000	9	2020	10
405	2020-09-11	10000	9	2020	23
406	2020-09-11	10000	9	2020	166
407	2020-09-11	10000	9	2020	211
408	2020-09-11	10000	9	2020	199
409	2020-09-11	10000	9	2020	280
410	2020-09-11	10000	9	2020	160
411	2020-09-11	10000	9	2020	89
412	2020-09-11	10000	9	2020	256
413	2020-09-11	10000	9	2020	230
414	2020-09-11	10000	9	2020	9
415	2020-09-11	10000	9	2020	98
416	2020-09-11	10000	9	2020	398
417	2020-09-11	10000	9	2020	67
418	2020-09-11	10000	9	2020	353
419	2020-09-11	10000	9	2020	257
420	2020-09-11	10000	9	2020	390
421	2020-09-11	10000	9	2020	68
422	2020-09-11	10000	9	2020	42
423	2020-09-11	10000	9	2020	402
424	2020-09-11	10000	9	2020	274
425	2020-09-11	10000	9	2020	221
426	2020-09-11	10000	9	2020	426
427	2020-09-11	10000	9	2020	428
428	2020-09-11	10000	9	2020	75
429	2020-09-11	10000	9	2020	429
430	2020-09-11	10000	9	2020	421
431	2020-09-11	10000	9	2020	204
432	2020-09-11	10000	9	2020	289
433	2020-09-11	10000	9	2020	405
434	2020-09-11	10000	9	2020	358
435	2020-09-11	10000	9	2020	412
436	2020-09-11	10000	9	2020	340
437	2020-09-11	10000	9	2020	399
438	2020-09-11	10000	9	2020	406
439	2020-09-11	10000	9	2020	176
440	2020-09-11	10000	9	2020	37
441	2020-09-11	10000	9	2020	290
442	2020-09-11	10000	9	2020	376
443	2020-09-11	10000	9	2020	25
444	2020-09-11	10000	9	2020	146
445	2020-09-11	10000	9	2020	288
446	2020-09-11	10000	9	2020	120
447	2020-09-11	10000	9	2020	411
448	2020-09-11	10000	9	2020	414
449	2020-09-11	10000	9	2020	21
450	2020-09-11	10000	9	2020	427
451	2020-09-11	10000	9	2020	81
452	2020-09-11	10000	9	2020	109
453	2020-09-11	10000	9	2020	99
454	2020-09-11	10000	9	2020	396
455	2020-09-11	10000	9	2020	244
456	2020-09-11	10000	9	2020	333
457	2020-09-11	10000	9	2020	33
458	2020-09-11	10000	9	2020	93
459	2020-09-11	10000	9	2020	104
460	2020-09-11	10000	9	2020	202
461	2020-09-11	10000	9	2020	14
462	2020-09-11	10000	9	2020	15
463	2020-09-11	10000	9	2020	294
464	2020-09-11	10000	9	2020	174
838	2020-10-06	10000	10	2020	29
\.


--
-- TOC entry 2445 (class 0 OID 0)
-- Dependencies: 194
-- Name: ordenes_compras_pagos_seguro_id_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.ordenes_compras_pagos_seguro_id_seq', 1025, true);


--
-- TOC entry 2385 (class 0 OID 28807)
-- Dependencies: 195
-- Data for Name: relacion_laboral; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.relacion_laboral (relacion_laboral, descripcion) FROM stdin;
1	Contratado
2	Jornalero
3	Nombrado
\.


--
-- TOC entry 2446 (class 0 OID 0)
-- Dependencies: 197
-- Name: servicio_fijo_proceso_id_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.servicio_fijo_proceso_id_seq', 46, true);


--
-- TOC entry 2386 (class 0 OID 28813)
-- Dependencies: 196
-- Data for Name: servicios_fijos_descuentos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.servicios_fijos_descuentos (id, servicio, fecha, mes, agno, monto) FROM stdin;
33	2	2020-02-18	1	2020	20000
34	2	2020-03-19	2	2020	20000
35	2	2020-06-15	5	2020	20000
40	2	2020-07-16	6	2020	20000
41	2	2020-08-14	7	2020	20000
43	2	2020-09-11	8	2020	20000
\.


--
-- TOC entry 2388 (class 0 OID 28819)
-- Dependencies: 198
-- Data for Name: servicios_temporal_descuentos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.servicios_temporal_descuentos (id, servicio, fecha, mes, agno, cuotas, cuota, monto) FROM stdin;
\.


--
-- TOC entry 2447 (class 0 OID 0)
-- Dependencies: 199
-- Name: servicios_temporal_descuentos_id_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.servicios_temporal_descuentos_id_seq', 1, false);


--
-- TOC entry 2390 (class 0 OID 28825)
-- Dependencies: 200
-- Data for Name: socios; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.socios (socio, cedula, nombre_apellido, telefono, celular, direccion_particular, direccion_laboral, dependencia, relacion_laboral, ingreso_funcionario, estado) FROM stdin;
1	1485811	Sergio Daniel Mongelós	0982910211	0982910211	29 de setiembre 883	Mcal. López esq. Cap. Bueno	Dpto. Atención Ciudadana	1	2015-12-19	0
4	422775	BARBOZA BADAUI, MIGUEL ANGEL						2	\N	0
5	506239	GIMENEZ MALDONADO, VIRGILIO RAMON						2	\N	0
6	522583	ACOSTA, RAMON GENARO						2	\N	0
7	556184	BERNAL NUNEZ, RUBEN GUSTAVO						2	\N	0
8	579429	MENCIA ORTIZ, TOMAS AMADO						2	\N	0
10	739072	MARIN DE CARDOZO, ESTHER						2	\N	0
11	770401	SANCHEZ VERA, GUSTAVO						2	\N	0
12	784846	BAEZ GOMEZ, LAURO HERNAN						2	\N	0
13	790020	GOMEZ, BENITO MARIA						2	\N	0
14	794368	MENDIOLA GONZALEZ, DIONISIO						2	\N	0
15	823937	SOSA NOGUERA, VIRGILIO						2	\N	0
16	871328	BENITEZ GONZALEZ, SIXTO						2	\N	0
17	873162	VILLALBA GONZALEZ, MIRYAN						2	\N	0
18	884386	PALAU TELLEZ, LIDIA FELICIANA						2	\N	0
19	911995	CABALLERO, CALIXTO						2	\N	0
20	996818	TROCHE ROTELA, FLORENCIO						2	\N	0
21	1052802	CORVALAN NUNEZ, FERNANDO						2	\N	0
22	1056472	RAMIREZ LUGO, CARLOS						2	\N	0
23	1107193	SILGUERO AGUERO, EVARISTO						2	\N	0
24	1112956	DURE GALEANO,  IVON GRISELDA						2	\N	0
25	1125127	RECALDE CARMAGNOLA, ANTONIO MANUEL						2	\N	0
26	1125787	CABRERA PEREIRA,MARIA ESTELA						2	\N	0
27	1164282	CANTERO, SONIA						2	\N	0
28	1183692	BENITEZ, ALFONSO						2	\N	0
29	1247938	GONZALEZ M, FRANCISCO						2	\N	0
30	1296349	CABRERA GARCIA, MARTIN TEODORO						2	\N	0
31	1335075	ESPINOLA OSORIO,  PABLO DANIEL						2	\N	0
32	1354717	ROJAS LEIVA, LUIS GILBERTO						2	\N	0
33	1398691	BALBUENA MARTINEZ, ROSALINO AGUSTIN						2	\N	0
34	1434574	ALONSO AREVALO, LUIS GILBERTO						2	\N	0
35	1470458	IRALA PORTILLO, TANIA LORENA						2	\N	0
36	1491113	ESPINOLA DE ALCARAZ,  MIRTHA						2	\N	0
37	1544226	SANABRIA MORA, JULIO						2	\N	0
38	1552141	GONZALEZ TORRES, MARIA CECILIA						2	\N	0
39	1669226	MEDINA GOMEZ, ALFREDO LUIS						2	\N	0
40	1670976	VAZQUEZ, VICTOR MANUEL						2	\N	0
41	1677805	MARTINEZ SOTO, ENGELBERTA CARINA						2	\N	0
42	1679507	RIOS SAMUDIO, MIRIAM ELIZABETH						2	\N	0
43	1693280	MONTIEL DE PEREZ, JULIA						2	\N	0
44	1714443	ORTELLADO, FRANCISCO						2	\N	0
45	1751475	CARDOZO LAMBARE, MIRTA ELIZABETH						2	\N	0
46	1801497	ARCE RODRIGUEZ, EDEN EZEQUIEL						2	\N	0
47	1869574	CABRERA BENITEZ, ROMMEL ORLANDO						2	\N	0
48	1934656	GODOY, CECILIO						2	\N	0
49	1959464	RODAS, RICARDO SANTIAGO						2	\N	0
50	1995803	BAEZ ACOSTA, FREDY ANTONIO						2	\N	0
51	2009945	SALA, APARICIA						2	\N	0
52	2033394	TALAVERA DE CESPEDES, VIVIAN						2	\N	0
53	2038961	RESQUIN, NERY BONIFACIO						2	\N	0
54	2040981	CANDIDO SEGOVIA, MARIA JOSE RAMONA						2	\N	0
55	2059686	FIGUEREDO CASTELVI, CRISTINO MANUEL						2	\N	0
56	2087253	BENITEZ FIGUEREDO, SELVA MARIA						2	\N	0
57	2108064	ROMERO DE APONTE, MIRIAN ELIZABETH						2	\N	0
58	2149161	LUGO GONZALEZ, JOSE SALVADOR						2	\N	0
59	2152452	SANABRIA BAZAN, DANIEL						2	\N	0
60	2209200	GONZALEZ, LAURA						2	\N	0
61	2217438	DOMINGUEZ, LOURDES MARLENE						2	\N	0
62	2239903	SOSA DE ESPINOLA, CAROLINA						2	\N	0
63	2265617	BRITOS MARTINEZ, CLAUDIA CAROLINA						2	\N	0
64	2298734	MORAN BERNAL, CARLOS EDUARDO						2	\N	0
65	2304623	ESPINOLA LOPEZ, LETICIA CAROLINA						2	\N	0
66	2306895	RODAS, CESAR RAUL						2	\N	0
67	2314467	MARECOS PAREDES, ALFREDO ARIEL						2	\N	0
69	2411749	VAZQUEZ, CARLOS						2	\N	0
70	2457299	BOGADO NOGUERA, JUSTINO DARIO						2	\N	0
71	2468149	GONZALEZ, CARLOS ALBERTO						2	\N	0
72	2478450	CENTURION SALINAS, LILIAN MARLENE						2	\N	0
73	2486946	BRITEZ MENDIETA, ISIDRO						2	\N	0
74	2515300	ANTUNEZ AMARILLA, ANELIZE BEATRIZ						2	\N	0
75	2524517	VARELA COLMAN, CLAUDIO ARSENIO						2	\N	0
76	2525867	GONZALEZ SOLER, NELSON MANUEL						2	\N	0
77	2552779	BENITEZ, ROLANDO ALCIDES						2	\N	0
78	2563317	ARRUA MARTINEZ, ALBERTO						2	\N	0
79	2574649	ORTIGOZA DE BENITEZ, ANGELICA						2	\N	0
80	2653969	IRALA RIVAROLA, PEDRO						2	\N	0
81	2675271	GALEANO AGUERO, ASUNCION						2	\N	0
82	2830476	FRANCO ALMADA, RAFAEL						2	\N	0
83	2914600	IDALGO,  MARIA CRISTINA						2	\N	0
84	2969692	CARDOZO, FEDERICO 						2	\N	0
85	2992257	GARCIA JARA,  GERARDO						2	\N	0
86	3000753	MEDINA GOMEZ, ROBERTO						2	\N	0
87	3170325	SANCHEZ FLEITAS, RICHARD JAVIER						2	\N	0
88	3208603	CARDOZO, MA LILIANA						2	\N	0
89	3209336	ACOSTA ORTIGOZA, ALBERTO JOSE MARIA						2	\N	0
90	3212403	CAREAGA, LIZ CAROLINA						2	\N	0
91	3227259	GAONA AYALA, HUGO GERMAN						2	\N	0
92	3384133	OLIVEIRA,  DANIEL						2	\N	0
9	623195	CANTERO, MIGUEL ANGEL		(0972)146-379	KANONNIKOFF 579 		MERCADO ZONALES	2	\N	0
93	3388910	VERA INSFRAN,  ROSALBA BELEN						2	\N	0
94	3402320	FERNANDEZ GALEANO, NORMA BEATRIZ						2	\N	0
95	3425343	BENITEZ DUARTE,  HERMINIA RAQUE						2	\N	0
96	3433382	OCAMPOS CACERES, EVER RAFAEL						2	\N	0
97	3439264	GOMEZ, ZUNILDA						2	\N	0
98	3451739	COLMAN, HUGO CESAR						2	\N	0
99	3501365	MENDOZA PRIETO, HERNAN ADOLFO						2	\N	0
100	3508298	FLORES RUIZ DIAZ, CARLOS FRANCISCO						2	\N	0
101	3523057	DUARTE LOPEZ, GUILLERMO 						2	\N	0
102	3529496	VERGARA, DULCE VICTORIA						2	\N	0
103	3529531	CANTERO ESPINOZA, EVER GERMAN						2	\N	0
104	3579937	ACOSTA ACOSTA, RAUL ISAAC						2	\N	0
105	3599563	ARZAMENDIA OSORIO, DARIO FRANCISCO						2	\N	0
106	3623009	GIMENEZ VILLALBA, GUSTAVO ADOLFO						2	\N	0
107	3624402	AYALA DE GRANCE, LILIANA MABEL						2	\N	0
108	3630911	CESPEDES SCOLARI, ARMANDO						2	\N	0
109	3639270	CASTILLO BRITEZ, INES SOLEDAD						2	\N	0
110	3643205	RUIZ DIAZ ALMADA, RAMON ESTEBA						2	\N	0
111	3644207	BELLI SANCHEZ, SILVIO FERNANDO 						2	\N	0
112	3650440	DUARTE AVALOS, MELIZZA MARIEL						2	\N	0
113	3693364	MOREL SANABRIA, EDUARDO 						2	\N	0
114	3710924	DIAZ MARTINEZ, FRANCISCO CEFERINO						2	\N	0
115	3732752	ORTEGA DUARTE, MARIA DOLORES 						2	\N	0
116	3805823	NUNEZ MELGAREJO, CARLOS DANIEL						2	\N	0
117	3830840	BENITEZ SOLER, MIRTA ISABEL						2	\N	0
118	3838705	SANCHEZ FLEITAS, LIZ PATRCIA 						2	\N	0
119	3862803	DOS SANTOS VILLALBA, CARLOS MARIA						2	\N	0
120	3922288	UCEDO ALFONSO,  HENRY PORTILLO						2	\N	0
121	3975094	LOPEZ LUGO, ROSA LILIANA 						2	\N	0
122	4009429	BARBOZA MARTINEZ, RODRIGO BENJAMIN						2	\N	0
123	4034723	BARRIOS LOPEZ, CESAR JAVIER						2	\N	0
124	4089726	PARRA CARDOZO, FABIO ALEXIS						2	\N	0
125	4091259	GAMARRA, ALICIA  						2	\N	0
126	4122442	FERNANDEZ MARECOS, EMILIANO						2	\N	0
127	4146376	AUADA, DEHIDI JURIDIA FAVIOLA  						2	\N	0
128	4172860	MALDONADO VELAZQUEZ, ARNALDO						2	\N	0
129	4178087	PANIAGUA GONZALEZ, CARLOS YAMIL						2	\N	0
130	4184172	QUINTANA ROMERO, EDGAR  						2	\N	0
131	4189319	ROA SANCHEZ, LAURA LORENA						2	\N	0
132	4227126	RODRIGUEZ FIGUEREDO, LIXI ADRIANA						2	\N	0
133	4258605	AGUADA GOMEZ,  MIGUEL ANGEL 						2	\N	0
134	4321421	OTAZU MARIN, MILCIADES JAVIER						2	\N	0
135	4363139	MONTERO FLEITAS, HUMBERTO MARC						2	\N	0
136	4369142	ROJAS, MARIA ELIZABETH						2	\N	0
137	4390507	MELGAREJO DE DIAZ, LUCIDA FLOR						2	\N	0
138	4442329	MARTINEZ GAYOSO,  OTILIA MANUEL						2	\N	0
139	4500024	ESCOBAR BRIZUELA, PATRICIA AGRIPINA						2	\N	0
140	4501500	BOGADO,  ANIBAL JAVIER						2	\N	0
141	4513155	BAREIRO RODRIGUEZ, CHRISTIAN MATHIA						2	\N	0
142	4528869	RECALDE VELAZQUEZ, ERICA MARIA DE J						2	\N	0
143	4580851	BAEZ MORINIGO, ARSENIO RAMON						2	\N	0
144	4634859	FARINA RODRIGUEZ, LETICIA RAFAELA						2	\N	0
145	4639058	CASTILLO BENITEZ, RAQUEL ESTELA 						2	\N	0
146	4639898	RIVAROLA DURE, ELIANA GISSELE						2	\N	0
147	4670339	CRISTALDO BENITEZ, JORGE						2	\N	0
148	4693034	JACQUET CANIZA, SERGIO DE JESUS						2	\N	0
149	4693212	AQUINO MARTINEZ, MARIEL						2	\N	0
150	4705111	DITRANI GONZALEZ, CESAR 						2	\N	0
151	4759810	GONZALEZ ISASI, JESUS						2	\N	0
152	4814818	GONZALEZ, GABRIELA SOLEDAD						2	\N	0
153	4867818	ARCE RODRIGUEZ,  PABLO ABEL						2	\N	0
154	4890137	ACOSTA RUIZ, CRISTIAN						2	\N	0
155	4897208	CASTILLO BRITEZ, LUCIANO RAMON 						2	\N	0
156	4969865	ECHEVERRIA BARRETO, NANCY						2	\N	0
157	5032495	GONZALEZ TORALES, MARIO LUIS						2	\N	0
158	5091870	FARINA RODRIGUEZ, ADRIANA SOLEDAD						2	\N	0
159	5099624	ROMERO, NESTOR DANIEL						2	\N	0
160	5127673	PEREZ MONTIEL, FREDY MAGDALENO						2	\N	0
161	5174653	TORRES DIAZ, RUBEN DARIO						2	\N	0
162	5217678	VERA VARZAMENDIA, PATRICIA						2	\N	0
163	5420168	DOMINGUEZ MENDOZA, OSMAR DAVID 						2	\N	0
164	5444331	AREVALO RIVEROS,  RODRIGO ARNAL 						2	\N	0
165	5489631	GONZALEZ SANCHEZ, RODOLFO JAVIER 						2	\N	0
166	5651013	CUENCA MARTINEZ, JUNIOR GERARDO						2	\N	0
167	5705010	VILLALBA RODAS, RICARDO MANUEL						2	\N	0
168	6064919	BELOZO, CLAUDIO EMILIANO 						2	\N	0
169	6101397	ARRUA GOMEZ, ALBERTO  						2	\N	0
171	1001036	MERCADO ARANDA,   MARIA ANGELICA						3	\N	0
172	1020333	NUÑEZ, FRANCISCO EDELIO					DEPARTAMENTO DE LIMPIEZA	3	\N	0
174	1068151	ALVAREZ,  FAUSTO RAMON					OPERACIONES	3	\N	0
175	1104622	CORREA ADORNO VDA DE, CLAUDIA					COBRANZAS	3	\N	0
176	1121190	ALVAREZ GALEANO, MERCEDES ELIZABETH					JUZGADO DE FALTAS	3	\N	0
177	1194859	MEDA ARGUELLO, GUSTAVO ADOLFO					FISCALIZACION DE VENTA EN LA VIA PUBLICA	3	\N	0
179	1316675	RIOS RODRIGUEZ, CYNTHIA ADRIANA					BIODIVERSIDAD	3	\N	0
180	1357294	MENDIETA SALINAS, OMAR DAVID					PROY MEJORAMIENTO DEL MERCADO MUNI. N°4	3	\N	0
181	1408408	VERA GAYOSO,  MIGUEL ANGEL					MERCADO 1	3	\N	0
170	1000829	CARDOZO DOMINGUEZ,  OSVALDO					BASE 5	3	\N	0
173	1022900	GIMENEZ ACOSTA, FLORENCIO					ADMINISTRATIVO	3	\N	0
178	1204281	PENAYO OCAMPOS, RUBEN REINALDO					BIENESTAR DEL PERSONAL	3	\N	0
182	1514478	MARTINEZ DE MARIN, LILIAN VIRGINIA					DEPARTAMENTO DE TESORERIA	3	\N	0
183	1528240	DUARTE ACEVEDO, ALEJANDRA BEATRIZ					MANTENIMIENTO CATASTRAL	3	\N	0
184	1561403	RODRIGUEZ GONZALEZ,  RUFINA ELI					MERCADO 5	3	\N	0
185	1589954	VILLALBA SALINAS    MARCIAL ERNESTO					SECRETARIA ADMINISTRATIVA	3	\N	0
186	1750851	FRANCO GALEANO, JULIO CESAR					ESCENARIO MOVIL	3	\N	0
187	1884170	RIVEROS PORTILLO, ROSANA					OPERATIVA DESCENTRALIZADA	3	\N	0
189	1885635	SANCHEZ, SERGIO JAVIER					COMISIONES ASESORAS	3	\N	0
190	2105201	SANCHEZ KNIEFF,  MIGUEL FELICIANO					INFRACCIONES	3	\N	0
191	2162719	ARANDA GONZALEZ,  ARNALDO ANDRES					BIENESTAR DEL PERSONAL	3	\N	0
192	2182108	CAÑETE ORREGO, ROLANDO GABRIEL					OPERACIONES	3	\N	0
193	2191938	ARAUJO AMARILLA, SILVIA BEATRIZ					SECRETARIA ADMINISTRATIVA	3	\N	0
194	2198342	SALINAS, SANDRA ELIZABETH					CEMENTERIOS	3	\N	0
195	2201170	CAÑETE CABALLERO, VICTOR HUGO					INFRACCIONES	3	\N	0
196	2220447	SANTACRUZ, JOSE LUIS					DEPARTAMENTO DE DEPOSITO	3	\N	0
197	2285585	GAMARRA DENIS, JORGE FERNANDO					COMISIONES ASESORAS	3	\N	0
198	2689338	VILLALBA CHAVEZ,  MIRIAN ROCIO					FISCALIZACION E INSPECCION TECNICA VEHI.	3	\N	0
199	3175861	FERREIRA, MARIA VICTORIA 					CONTROLES EN ESTABLECIMIENTOS	3	\N	0
200	3176680	NUÑEZ ACOSTA, HUGO ALFREDO					COBRANZAS	3	\N	0
201	3177269	ANGELONI FLORENTIN  SHIRLEY YENISSE					SECRETARIA ADMINISTRATIVA	3	\N	0
202	3198080	VAZQUEZ RAMIREZ     ROBERTO JAVIER					BIENESTAR DEL PERSONAL	3	\N	0
203	3268250	CANDIA, CLAUDIA LETICIA					COMISIONES ASESORAS	3	\N	0
204	3380568	 BERNAL MARIN, JUAN CARLOS					BIENESTAR DEL PERSONAL	3	\N	0
205	3518206	GARAY RAMIREZ,  MARCELO MANUEL  					JUVENTUD	3	\N	0
206	3574126	ESCOBAR HERMOSILLA, FABIAN SILVESTRE					DEPARTAMENTO DE ARCHIVO	3	\N	0
207	3623016	RECALDE BENITEZ,  JOSE MARIA					COBRANZAS	3	\N	0
208	4003732	BOBADILLA BENITEZ   DAVID SANTIAGO					LICENCIAS DE CONDUCIR	3	\N	0
209	415976	GUANES GIMENEZ, RAUL OSVALDO 					DIRECCION DE RECURSOS HUMANOS	3	\N	0
210	4185728	LUCERO DESVAR, CARMEN LETICIA					SECRETARIA ADMINISTRATIVA	3	\N	0
211	4280790	DOMINGUEZ,  ANGELICA MARILIZA					SECRETARIA ADMINISTRATIVA	3	\N	0
212	429631	ARAUJO MONGES,   RUSLAN DIONISIO					DEPARTAMENTO DE MANTENIMIENTO	3	\N	0
213	4484351	SILVA ESPINOLA,  JUAN VICTOR					OPERATIVO Y DE TRAFICO	3	\N	0
214	4573574	TORRES, ORLANDO ANIBAL					SECRETARIA ADMINISTRATIVA	3	\N	0
215	4665039	ORTIZ VILLALBA, ANDREA CELESTE					DIR. DE CENTRO DE DESARROLLO INTEGRAL	3	\N	0
217	5012326	ROMERO, FRANCISCO JAVIER					ADMINISTRATIVO FINANCIERO	3	\N	0
218	5212672	SANCHEZ ARCE,CARMELA					CONTROL AMBIENTAL	3	\N	0
220	5477850	ARRUA, JULIAN 					OPERATIVO Y DE TRAFICO	3	\N	0
221	5572891	ARCE MARECOS, FREDY MIGUEL					BASE 4	3	\N	0
222	610477	TORRES FRANCO, BERNARDA 					DEPARTAMENTO DE SEGURIDAD	3	\N	0
223	657774	GRANCE, LUCILA 					MERCADOS	3	\N	0
224	6829031	TORALES, CAMILA DAHIANA  					DEPARTAMENTO DE CAFETERIA	3	\N	0
225	710055	POST MERELES,  MIRTHA ELIZABETH					SECRETARIA ADMINISTRATIVA	3	\N	0
226	731799	VERA MARTINEZ, GILBERTO ANDRES					INGENIERIA DE TRAFICO	3	\N	0
227	756535	GOMEZ CHIPITE, MIRTA GRACIELA					BIENESTAR DEL PERSONAL	3	\N	0
228	820037	RIVAS BAZAN, SONIA CAROLINA					LEGAJOS	3	\N	0
229	847827	URUNAGA, RAFAEL DARIO 					DEPARTAMENTO DE MANTENIMIENTO	3	\N	0
230	905443	AQUINO MENDOZA, JULIO CESAR					SERVICIOS GENERALES	3	\N	0
231	911517	ALVAREZ, RUBEN DARIO   					CONTROL OPERATIVO	3	\N	0
232	931092	CACERES ALLENDE,  CINTHIA 					MANTENIMIENTO	3	\N	0
3	376043	ZARZA AGUILERA, LEANDRO						2	\N	0
233	5193334	Balbuena Martinez Daisy Marie		0982932830		Junta Municipal de Asunción		2	\N	0
234	3548162	Rodriguez Insaurralde, Gustavo Javier		0991739090		Gral. Santos y Playa	Dirección de Vialidad	2	\N	0
235	1557171	Alcides Dario Bareiro Cantero 		0986896186				3	2008-09-11	0
236	2303118	Jorge Maria Amarilla samudio		0971176242	23 proyectadas e/ rojas silva y brasil 			2	2013-03-13	0
237	708596	Osvaldo  Concepcion Chavez Garrigoza		0982201136	nuestra señora del huerto 1460 barrio ricon ñemby			3	1991-11-01	0
238	5220127	Luis Miguel Aponte Romero		0972743439	33 proyectadas y pai perez			2	\N	0
239	1475855	Alejandro Aponte		0974615644	33 y pai perez.roberto ele peti		mercado 1	2	1997-05-15	0
240	2849448	Gustavo Lopez		0972566738	diputado nacional ,teodoro rivarola		seguridad 	2	2010-06-01	0
242	5192982	Maria Alicia Acosta Andino					cementerio del este 	2	\N	0
243	487760	Aurelio Bobadilla		0994257905	comandante gamarra1617 c/ de las llanas			3	2014-06-01	0
244	2011634	Roque Gustavo Recalde 		0995698359				3	2010-06-08	0
245	4278533	Eduardo,Benitez		0981332776				3	2006-06-07	0
254	2860074	HERNAN VERGARA		0994410369				2	2002-03-01	0
255	2963474	MERCEDES NOEMI ALCARAZ TORRES		0983456596	CORONEL GRACIA ESQUINA TESTANOVA 	JUSGADO		3	2005-06-03	0
253	2373427	ROSA FLEITAS		0991390171	36 PROYECTADAS CANTALUPI	TERMINAL		2	2008-02-04	0
256	4830738	Sanchez Fleitas,Lucero Noemi		0971556300	Mencia c/ bruno gugiari 456	Servicio General Bloque C		3	2019-10-15	0
257	2454339	Barrios Melgarejo,Miguel 		0994223691	Sagrado corazon de jesus c/ defensores del chaco	Abasto	seguridad 	2	2007-08-27	0
258	2224560	Paniagua,Maria						2	\N	0
259	446706	Godoy, Arsenio						2	\N	0
260	450947	Davalos, Silvana						2	\N	0
261	518227	Bobadilla, Miguel						2	\N	0
262	587243	Alderete, Julio						2	\N	0
263	599889	Amarilla, Marcial						2	\N	0
264	877175	Galeano, Julio						2	\N	0
265	1206038	Paniagua, Estanislao						2	\N	0
268	1383343	Benitez, Jorge						2	\N	0
266	1307774	Torres, Alejandro						3	\N	0
219	5324369	VILLALBA RODAS,  VICTOR ADRIAN					OPERACIONES	2	\N	0
269	1481326	Nuñez, Lorenza						2	\N	0
270	1551535	Gimenez, Ricardo						2	\N	0
272	1872959	Barboza, Juan						2	\N	0
274	2186459	Gomez, Santiago						2	\N	0
275	2440547	Coronel, Lucio						2	\N	0
276	2466462	Aquino, Jorge						2	\N	0
277	2525823	Montiel, Asuncion						2	\N	0
278	2938927	Fernandez, Oscar						2	\N	0
279	3194698	Vera, Alberto						2	\N	0
280	3315837	Lescano, Magno						2	\N	0
281	3404169	Escurra, Fernando						2	\N	0
282	3489061	Chavez, Francisco						2	\N	0
283	3532152	Gomez, Adriana						2	\N	0
284	3539821	Gaona, Fredy						2	\N	0
285	3522792	Cabañas, Fidencia						2	\N	0
286	3756074	Calixtro, Modenga						2	\N	0
287	3984389	Espinoza, Jose						2	\N	0
288	4029525	Pereira, Maria						2	\N	0
289	4039618	Galeano, Diego						2	\N	0
290	4799113	Aquino, Regina						2	\N	0
291	4820774	Santa Cruz, Cristian						2	\N	0
292	4921166	Resquin, Liz						2	\N	0
293	4928147	Alvarez, Josue						2	\N	0
294	4951391	Cabrera, Juan						2	\N	0
295	4959116	Rodriguez, Natalia						2	\N	0
296	5130752	Mariza, Gutierrez						2	\N	0
303	5875202	Medina, Alexandra						2	\N	0
304	5900709	Lescano, Emilce						2	\N	0
305	6658987	Riveros, Victor						2	\N	0
311	4516983	Cantero, Rosa						2	\N	0
315	869390	Matto, Juan						3	\N	0
316	923843	Urunaga, Bernardo						3	\N	0
317	985610	Villalba, Edgar						3	\N	0
318	1031213	Canatta, Enrique						3	\N	0
319	1059736	Martinez, Yeimy						3	\N	0
320	1769613	Britez, Rosa						3	\N	0
321	2543050	Ucedo, Hilbert						3	\N	0
322	2836156	Lescano, Rodrigo						3	\N	0
323	2924809	Lescano, Luz						3	\N	0
324	3220638	Urunaga, Nilsa						3	\N	0
325	3440664	Zayas, Cristhian						3	\N	0
326	3637758	Mendoza, Dianna						3	\N	0
327	3786304	Gonzalez, Walter						3	\N	0
328	4160230	Valdez, Gilberto						3	\N	0
329	5164559	Soto, Matias						3	\N	0
330	5900744	Lescano, Tania						3	\N	0
333	7543362	Dias, Emanuel						3	\N	0
335	2186447	Olmedo, Aaron						3	\N	0
336	4900668	Espinola, Alfirio						3	\N	0
337	822531	Jover, Juan						3	\N	0
338	2385700	Gonzalez, Maria						3	\N	0
339	4256259	Resquin, Mercedes						3	\N	0
340	3186807	Vera, Graciela						3	\N	0
341	4271737	Gimenez, Guillermo						3	\N	0
343	2189145	Torres,Jorge 	0985311062	0985311062	jejui 957 	Mercado 4	zonales	2	2000-04-14	0
344	927335	Ayala, Trifilo		0992749467	Mburucuya 25 casi Cacique Lambare	Mcal  Lopez	D.G.A.F	3	1990-10-10	0
346	1441901	Marcio, Moreno						1	\N	0
347	3582410	Cardozo, Luis Alberto						2	\N	0
348	1866419	Samudio, Alejo						2	\N	0
349	1703939	Carreño, Nora Ester						2	\N	0
350	4554227	Alfaro, Ever Hugo						2	\N	0
351	4513898	Rolon Cecco, Dario Ramon		0981831284	Manuel Gondra 393 c/ Mompox	Mcal  Lopez	Municipalidad de  Asuncion	3	2016-07-01	0
352	1517260	Fernandez Benitez, Irene		0992571837	Barrio 14 de mayo - Ypane	Mercado Abasto	seguridad 	2	2015-07-01	0
353	697735	Aguero Lopez, Vidal		0993320921	33 y Pa'i Perez 	21 proyectadas - Oñondivepa	seguridad 	2	1993-02-01	0
354	3970865	Coronel, Pedro		0981591323	Rio de la Plata y Juan de Garay	Mcal  Lopez	Juzgado de Faltas - Mesa de entrada	2	2008-11-01	0
356	810968	Fernandez Diaz, Victor Antonio		0993352086	36 proyectadas y Mexico	Mercados Zonales		3	1991-07-26	0
357	2036210	Robles, Hector		0981223516	Proceres de mayo y Zorrilla	Mercados Zonales		2	2003-03-21	0
358	1194276	Rodriguez, Jorge		0981198925	Guillermo Arias c/ De las Llanas 	Mercados Zonales		3	2000-07-02	0
359	4371655	Carlos Gomez		0994603399	Capitan Miranda 2665	Mercado zonales 		3	2008-10-20	0
360	4797153	Evangelina Nuñez						2	\N	0
361	645652	Jose Maria Niz		0981720679	Nimia Candia 1130 c/ Bernardino caballero		Terminal 	3	2004-07-01	0
362	4833133	Nelson Caballero						2	\N	0
363	2459240	Concepcion Rios						2	\N	0
364	401877	Santiago Godoy						2	\N	0
365	3315884	Victor Gamarra						2	\N	0
366	3029520	Gladys Zarate					Terminal 	3	\N	0
367	4908628	Heraldo Rojas		0986118597				3	\N	0
368	4630984	Mauro Osvaldo Quintana Torres		0992655940	coroshire c/ bruno gugiare			2	\N	0
369	3538334	Hugo Medina Paez						2	\N	0
370	1187015	Blas Duarte		0983912706	20 proyectadas c/ paraguari		direccion de seguridad	2	2011-07-17	0
374	4354572	Denis Salinas Gayoso						3	\N	0
375	979175	Alcides Maldonado						2	\N	0
376	3735519	Edelio Martinez						2	\N	0
379	4111715	Humberto Cubilla		0982851230	14 proyectadas y alberdi 366		seguridad 	3	2016-06-01	0
380	1716978	Nicolas Nuñez		0994883841	18 piroy - capiata		mercado de Abasto	2	2005-05-05	0
382	1412575	Divina Flor de Cardozo						3	\N	0
384	1699405	Reina Acosta Zelaya						2	\N	0
385	2026699	Walter Francisco Britez		0983522145	urcisinio velazco c/ rancho 8 nro 3000		seguridad 	2	2019-05-11	0
386	812241	Jorge Manuel Mereles Portillo		0994495818	Virgen de la encarnacion 3265 ( v. elisa)		seguridad 	2	2019-05-11	0
345	3533833	Curril Notario, Daniel						3	\N	0
306	4149382	Enciso, Federico						3	\N	0
273	2042904	Ledesma, Leonardo						2	\N	0
387	2924869	Luz Marina Lezcano Amarilla		0991508780			Direccion de riesgos	3	\N	0
388	3487296	Bruno Fabian peña lopez 		0976485737	8 de setiembre esquina spanovich		atopama	2	2020-01-01	0
389	2947155	Nicolas Da Silva		0983663794		Mercado 4	zonales	2	\N	0
390	1442599	Juan Ramon Acuña restaino		0983480009	ri4 curupayty  lamas carisima 1490	Mcal  Lopez	junta 	2	2008-03-01	0
241	2191629	Eulalia dionisia Leon Alarcon		0991628111		mercado	matenimiento	3	2019-07-03	0
392	4660830	carla chamira,cano roa	0972762698	0972762698	aca vera casi cerro cora	junta municipal	recursos	3	2019-01-01	0
393	4899298	liliana leticia,leguizamon gimenez 		0984528559	10 de febrero esquina toa	junta municipal	recursos humanos	2	2015-11-11	0
394	5377067	STREULI SOSA,URSULINA BEATRIZ		0985711604	42 PROYECTADAS Y CAPITAN FIGARI	ASEO URBANO	AREAS VERDES	2	2017-10-01	0
395	4183780	Natalia Isabel , Gonzalez Bareiro		0983979742	Dr. Montero 1351 e/ alejo garcia y diaz solis	jose berges esquina rio de janeiro	atopama	2	2020-02-05	0
396	3301246	Cesar Daniel,Bareiro		0981720665	Grito de asencio esq/ efrain cardozo	centro 9 zeballos cue	Area social	3	2010-01-01	0
216	5010873	LEIMBACHER GONZALEZ,  MOISES DANIEL		0971914951			DPTO. BIENESTAR DEL PERSONAL	3	2016-02-05	0
68	2321243	CABRERA, ARIEL MAXIMO		(0991)340-156	CALLE 8 VIÑAS CUE			2	\N	0
399	1311469	CARMEN ELVIRA RAMIREZ		(0984) 822-324	VIÑAS CUE			2	2011-10-01	0
400	1884746	GRACIELA RODRIGUEZ DE CESPEDES		(09671)578-803	CAPIATA RUTA 1 KM 20 BARRIO KENEDY		JARDIN BORANICO ZOOLOGICO	2	2011-10-01	0
401	687402	LUIS ALBERTO CACERES ALMEDA		(0983)457424	AVENIDA SANTISIMA TRINIDAD Y VDA DE LAS LLANAS  	BOTANICO	JARDIN BOTANICO Y ZOOLOGICO	2	2004-01-02	0
402	2669465	JUSTO RAMON PEDROZO ESCOBAR		(0982)138329	41 PROYECTADAS Y JAPON	BOTANICO	JARDIN BOTANICO Y ZOOLOGICO	2	1987-09-01	0
403	2219060	LORENZO SANTA CRUZ FRANCO		0984)791-811	URSUCINO VELAZCO BAñADO TACUMBU	MERCADO 4		2	1999-12-22	0
404	3622417	JOSE FELIPE OLMEDO TORALES		(0984)172-747	42 PROYECTADAS Y CAPITAN FIGARI	MERCADO 4	MERCADO ZONALES	2	2012-09-01	0
405	2218199	JOSE DAVID FERREIRA FISHER		(0994)357-146	GOIGURU 1706 BARRIO NAZARETH	MERCADO 4		2	2012-02-02	0
406	1121556	CARLOS RAMON OLIVERA		(0991)869-559	CALLE VISTA ALEGRE C/ TTE ALCORTA B° VISTA ALEGRE	MERCADO 4	ASEO URBANO	2	1997-05-20	0
408	426182	GRACIELA CACERES DE FACCIOLI	(0984)570-063		ZACARIAS ARZA 1531 ESQ. APARIRY	MUNICIPALIDAD	DIRECCION DE TRANSITO Y TRANSPORTE	3	2001-04-01	0
398	855595	JUAN TORRES	(0985)810-747	(0985)810-747	TERMINAL (TOA)			2	\N	0
409	3402061	ARTURO HIPOLITO CORONEL LEGUIZAMON		(0983)132-378		DIRECCION DE  SEGURIDAD	DIRECCION PROMOCION DE LA PARTICIPACION CIUDADANA	2	\N	0
410	1440954	LIDIA ANTONIA BARRIENTOS ITURBE						2	\N	0
411	1353910	CARLOS RAMÓN FLEITAS MIRANDA					DIRECCIÓN DEL JARDÍN BOTÁNICO	2	\N	0
412	4655954	MELANIO IBARRA						3	\N	0
414	2011215	RICARDO ALBERTO LATORRE ROJAS						2	\N	0
415	4684365	NEUSA ESTHER SOUZA 		(0992)734-124	DR GARCETE Y DE LAS LLANAS 405	MUNICIPALIDAD		1	\N	0
416	3297984	DOLLY PAOLA CUBILLA CHAMORRO						3	\N	0
417	5495361	ALFREDO TADEO  JIMENEZ RUIZ DIAZ 						3	\N	0
418	3495184	CARLOS DANIEL BERNAL BAEZ						3	\N	0
419	2926716	MARIO RUBEN BENITEZ						2	\N	0
420	1448756	FELIPE DE JESUS GALEANO BENITEZ						2	\N	0
421	4705647	JORGE DANIEL ZARATE AGUILERA						2	\N	0
422	4906469	CLAUDIA SOLEDAD PANIAGUA CACERES						3	\N	0
423	1697660	ROQUE RUBEN ZARATE NUÑEZ						3	\N	0
424	2364908	LAURA FERREIRA GALEANO						3	\N	0
425	4934265	CINTHIA ELIZABETH ORTIZ CORONEL						2	\N	0
426	1483193	LUSICH VAZQUEZ ZULLY DOROTEA						2	\N	0
427	3500423	IBAÑEZ MIGUEL ALEJANDRO						1	\N	0
428	5667138	LUIS ENRIQUE SANABRIA JULIO						3	\N	0
429	5753558	MIGUEL ANGEL ALMADA FRANCO						2	\N	0
432	5031842	FAUSTINO DANIEL TORRES BELLO		0976110628	EPOPEYA NACIONAL 190 C/ MARISCAL ESTIG.	COMUEDA 		2	2015-07-15	0
433	4498375	DIEGO CLEMENTE MELGAREJO OLMEDO		0992644920	29 PROYECTADAS  C/ JAPON	MERCADO 1	ASEO URBANO	3	2016-06-01	0
434	4941119	JESSICA NAZARETH FLEITAS						2	\N	0
435	751069	PEÑA JORGE ANTONIO						3	\N	0
2	1884854	CHRISTIAN LIOSNEL BAREIRO CANTERO	0981484395	0981484395	Testanova 2620  y Tte, Kanonnikoff	Mcal. Lopez y Cap. Bueno	Dirección General de Mercados	3	1999-07-15	0
407	3447730	ISABEL ORTEGA DE VARGAS						2	\N	0
267	1374802	Araujo, Carlos						3	\N	0
271	1702956	Zaracho, Aldo						3	\N	0
436	1318092	GLORIA TERESA GAMARRA MEZA			GESTION DE RIESGO			3	\N	0
437	1319607	MARIA VICTORIA ESPINOZA				CENTRO MUNICIPAL N*9		3	\N	0
438	2088103	FRANCISCO JAVIER GIMENEZ FIGUEREDO				UNILIMER		3	\N	0
439	2372126	EUSTACIA GONZALEZ DE ESPINOLA				BASE 2		3	\N	0
440	2492995	GONZALO CARRERA				SECTOR FINANCIERO		3	\N	0
441	3641792	GERONIMO SANTI FERNANDEZ				CENTRO MUNICIPAL N*9		3	\N	0
442	3762686	FELIX MARIA QUIÑONES PAIVA				ZOOLOGICO		3	\N	0
443	4210070	EVER IGNACIO VERA				BIENESTAR DELO PERSONAL		3	\N	0
444	4422221	JOEL FERNANDO FRANCO				UNILIR MERCADO		3	\N	0
445	511310	ANTONIO RAMON SOSA 				SECRETARIA ADMINISTRATIVA		3	\N	0
413	2046729	ALFONSO ALEGRE						2	\N	0
447	2047496	OLGA TECHEIRA				RECURSOS HUMANOS		3	\N	0
448	2086688	DIEGO ORTIZ						2	\N	0
449	4247855	ALEX LEIMBACHER						2	\N	0
450	857472	CARLOS GARCIA		(0981)974-147				2	\N	0
451	1906339	ALBINO CAMPS						2	\N	0
452	1505508	MARCELINA VELAZQUEZ						2	\N	0
\.


--
-- TOC entry 2391 (class 0 OID 28832)
-- Dependencies: 201
-- Data for Name: socios_descuentos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.socios_descuentos (id, socio, fecha, servicio, monto_descuento, mes, agno) FROM stdin;
41	7	2020-02-18	2	20000	2	2020
42	4	2020-02-18	2	20000	2	2020
43	3	2020-02-18	2	20000	2	2020
44	2	2020-02-18	2	20000	2	2020
45	8	2020-02-18	2	20000	2	2020
46	1	2020-03-19	2	20000	3	2020
47	2	2020-03-19	2	20000	3	2020
48	4	2020-03-19	2	20000	3	2020
49	5	2020-03-19	2	20000	3	2020
50	6	2020-03-19	2	20000	3	2020
51	7	2020-03-19	2	20000	3	2020
52	8	2020-03-19	2	20000	3	2020
53	9	2020-03-19	2	20000	3	2020
54	10	2020-03-19	2	20000	3	2020
55	11	2020-03-19	2	20000	3	2020
56	12	2020-03-19	2	20000	3	2020
57	13	2020-03-19	2	20000	3	2020
58	14	2020-03-19	2	20000	3	2020
59	15	2020-03-19	2	20000	3	2020
60	16	2020-03-19	2	20000	3	2020
61	17	2020-03-19	2	20000	3	2020
62	18	2020-03-19	2	20000	3	2020
63	19	2020-03-19	2	20000	3	2020
64	20	2020-03-19	2	20000	3	2020
65	21	2020-03-19	2	20000	3	2020
66	22	2020-03-19	2	20000	3	2020
67	23	2020-03-19	2	20000	3	2020
68	24	2020-03-19	2	20000	3	2020
69	25	2020-03-19	2	20000	3	2020
70	26	2020-03-19	2	20000	3	2020
71	27	2020-03-19	2	20000	3	2020
72	28	2020-03-19	2	20000	3	2020
73	29	2020-03-19	2	20000	3	2020
74	30	2020-03-19	2	20000	3	2020
75	31	2020-03-19	2	20000	3	2020
76	32	2020-03-19	2	20000	3	2020
77	33	2020-03-19	2	20000	3	2020
78	34	2020-03-19	2	20000	3	2020
79	35	2020-03-19	2	20000	3	2020
80	36	2020-03-19	2	20000	3	2020
81	37	2020-03-19	2	20000	3	2020
82	38	2020-03-19	2	20000	3	2020
83	39	2020-03-19	2	20000	3	2020
84	40	2020-03-19	2	20000	3	2020
85	41	2020-03-19	2	20000	3	2020
86	42	2020-03-19	2	20000	3	2020
87	43	2020-03-19	2	20000	3	2020
88	44	2020-03-19	2	20000	3	2020
89	45	2020-03-19	2	20000	3	2020
90	46	2020-03-19	2	20000	3	2020
91	47	2020-03-19	2	20000	3	2020
92	48	2020-03-19	2	20000	3	2020
93	49	2020-03-19	2	20000	3	2020
94	50	2020-03-19	2	20000	3	2020
95	51	2020-03-19	2	20000	3	2020
96	52	2020-03-19	2	20000	3	2020
97	53	2020-03-19	2	20000	3	2020
98	54	2020-03-19	2	20000	3	2020
99	55	2020-03-19	2	20000	3	2020
100	56	2020-03-19	2	20000	3	2020
101	57	2020-03-19	2	20000	3	2020
102	58	2020-03-19	2	20000	3	2020
103	59	2020-03-19	2	20000	3	2020
104	60	2020-03-19	2	20000	3	2020
105	61	2020-03-19	2	20000	3	2020
106	62	2020-03-19	2	20000	3	2020
107	63	2020-03-19	2	20000	3	2020
108	64	2020-03-19	2	20000	3	2020
109	65	2020-03-19	2	20000	3	2020
110	66	2020-03-19	2	20000	3	2020
111	67	2020-03-19	2	20000	3	2020
112	68	2020-03-19	2	20000	3	2020
113	69	2020-03-19	2	20000	3	2020
114	70	2020-03-19	2	20000	3	2020
115	71	2020-03-19	2	20000	3	2020
116	72	2020-03-19	2	20000	3	2020
117	73	2020-03-19	2	20000	3	2020
118	74	2020-03-19	2	20000	3	2020
119	75	2020-03-19	2	20000	3	2020
120	76	2020-03-19	2	20000	3	2020
121	77	2020-03-19	2	20000	3	2020
122	78	2020-03-19	2	20000	3	2020
123	79	2020-03-19	2	20000	3	2020
124	80	2020-03-19	2	20000	3	2020
125	81	2020-03-19	2	20000	3	2020
126	82	2020-03-19	2	20000	3	2020
127	83	2020-03-19	2	20000	3	2020
128	84	2020-03-19	2	20000	3	2020
129	85	2020-03-19	2	20000	3	2020
130	86	2020-03-19	2	20000	3	2020
131	87	2020-03-19	2	20000	3	2020
132	88	2020-03-19	2	20000	3	2020
133	89	2020-03-19	2	20000	3	2020
134	90	2020-03-19	2	20000	3	2020
135	91	2020-03-19	2	20000	3	2020
136	92	2020-03-19	2	20000	3	2020
137	93	2020-03-19	2	20000	3	2020
138	94	2020-03-19	2	20000	3	2020
139	95	2020-03-19	2	20000	3	2020
140	96	2020-03-19	2	20000	3	2020
141	97	2020-03-19	2	20000	3	2020
142	98	2020-03-19	2	20000	3	2020
143	99	2020-03-19	2	20000	3	2020
144	100	2020-03-19	2	20000	3	2020
145	101	2020-03-19	2	20000	3	2020
146	102	2020-03-19	2	20000	3	2020
147	103	2020-03-19	2	20000	3	2020
148	104	2020-03-19	2	20000	3	2020
149	105	2020-03-19	2	20000	3	2020
150	106	2020-03-19	2	20000	3	2020
151	107	2020-03-19	2	20000	3	2020
152	108	2020-03-19	2	20000	3	2020
153	109	2020-03-19	2	20000	3	2020
154	110	2020-03-19	2	20000	3	2020
155	111	2020-03-19	2	20000	3	2020
156	112	2020-03-19	2	20000	3	2020
157	113	2020-03-19	2	20000	3	2020
158	114	2020-03-19	2	20000	3	2020
159	115	2020-03-19	2	20000	3	2020
160	116	2020-03-19	2	20000	3	2020
161	117	2020-03-19	2	20000	3	2020
162	118	2020-03-19	2	20000	3	2020
163	119	2020-03-19	2	20000	3	2020
164	120	2020-03-19	2	20000	3	2020
165	121	2020-03-19	2	20000	3	2020
166	122	2020-03-19	2	20000	3	2020
167	123	2020-03-19	2	20000	3	2020
168	124	2020-03-19	2	20000	3	2020
169	125	2020-03-19	2	20000	3	2020
170	126	2020-03-19	2	20000	3	2020
171	127	2020-03-19	2	20000	3	2020
172	128	2020-03-19	2	20000	3	2020
173	129	2020-03-19	2	20000	3	2020
174	130	2020-03-19	2	20000	3	2020
175	131	2020-03-19	2	20000	3	2020
176	132	2020-03-19	2	20000	3	2020
177	133	2020-03-19	2	20000	3	2020
178	134	2020-03-19	2	20000	3	2020
179	135	2020-03-19	2	20000	3	2020
180	136	2020-03-19	2	20000	3	2020
181	137	2020-03-19	2	20000	3	2020
182	138	2020-03-19	2	20000	3	2020
183	139	2020-03-19	2	20000	3	2020
184	140	2020-03-19	2	20000	3	2020
185	141	2020-03-19	2	20000	3	2020
186	142	2020-03-19	2	20000	3	2020
187	143	2020-03-19	2	20000	3	2020
188	144	2020-03-19	2	20000	3	2020
189	145	2020-03-19	2	20000	3	2020
190	146	2020-03-19	2	20000	3	2020
191	147	2020-03-19	2	20000	3	2020
192	148	2020-03-19	2	20000	3	2020
193	149	2020-03-19	2	20000	3	2020
194	150	2020-03-19	2	20000	3	2020
195	151	2020-03-19	2	20000	3	2020
196	152	2020-03-19	2	20000	3	2020
197	153	2020-03-19	2	20000	3	2020
198	154	2020-03-19	2	20000	3	2020
199	155	2020-03-19	2	20000	3	2020
200	156	2020-03-19	2	20000	3	2020
201	157	2020-03-19	2	20000	3	2020
202	158	2020-03-19	2	20000	3	2020
203	159	2020-03-19	2	20000	3	2020
204	160	2020-03-19	2	20000	3	2020
205	161	2020-03-19	2	20000	3	2020
206	162	2020-03-19	2	20000	3	2020
207	163	2020-03-19	2	20000	3	2020
208	164	2020-03-19	2	20000	3	2020
209	165	2020-03-19	2	20000	3	2020
210	166	2020-03-19	2	20000	3	2020
211	167	2020-03-19	2	20000	3	2020
212	168	2020-03-19	2	20000	3	2020
213	169	2020-03-19	2	20000	3	2020
214	171	2020-03-19	2	20000	3	2020
215	172	2020-03-19	2	20000	3	2020
216	174	2020-03-19	2	20000	3	2020
217	175	2020-03-19	2	20000	3	2020
218	176	2020-03-19	2	20000	3	2020
219	177	2020-03-19	2	20000	3	2020
220	179	2020-03-19	2	20000	3	2020
221	180	2020-03-19	2	20000	3	2020
222	181	2020-03-19	2	20000	3	2020
223	170	2020-03-19	2	20000	3	2020
224	173	2020-03-19	2	20000	3	2020
225	178	2020-03-19	2	20000	3	2020
226	182	2020-03-19	2	20000	3	2020
227	183	2020-03-19	2	20000	3	2020
228	184	2020-03-19	2	20000	3	2020
229	185	2020-03-19	2	20000	3	2020
230	186	2020-03-19	2	20000	3	2020
231	187	2020-03-19	2	20000	3	2020
232	189	2020-03-19	2	20000	3	2020
233	190	2020-03-19	2	20000	3	2020
234	191	2020-03-19	2	20000	3	2020
235	192	2020-03-19	2	20000	3	2020
236	193	2020-03-19	2	20000	3	2020
237	194	2020-03-19	2	20000	3	2020
238	195	2020-03-19	2	20000	3	2020
239	196	2020-03-19	2	20000	3	2020
240	197	2020-03-19	2	20000	3	2020
241	198	2020-03-19	2	20000	3	2020
242	199	2020-03-19	2	20000	3	2020
243	200	2020-03-19	2	20000	3	2020
244	201	2020-03-19	2	20000	3	2020
245	202	2020-03-19	2	20000	3	2020
246	203	2020-03-19	2	20000	3	2020
247	204	2020-03-19	2	20000	3	2020
248	205	2020-03-19	2	20000	3	2020
249	206	2020-03-19	2	20000	3	2020
250	207	2020-03-19	2	20000	3	2020
251	208	2020-03-19	2	20000	3	2020
252	209	2020-03-19	2	20000	3	2020
253	210	2020-03-19	2	20000	3	2020
254	211	2020-03-19	2	20000	3	2020
255	212	2020-03-19	2	20000	3	2020
256	213	2020-03-19	2	20000	3	2020
257	214	2020-03-19	2	20000	3	2020
258	215	2020-03-19	2	20000	3	2020
259	216	2020-03-19	2	20000	3	2020
260	217	2020-03-19	2	20000	3	2020
261	218	2020-03-19	2	20000	3	2020
262	219	2020-03-19	2	20000	3	2020
263	220	2020-03-19	2	20000	3	2020
264	221	2020-03-19	2	20000	3	2020
265	222	2020-03-19	2	20000	3	2020
266	223	2020-03-19	2	20000	3	2020
267	224	2020-03-19	2	20000	3	2020
268	225	2020-03-19	2	20000	3	2020
269	226	2020-03-19	2	20000	3	2020
270	227	2020-03-19	2	20000	3	2020
271	228	2020-03-19	2	20000	3	2020
272	229	2020-03-19	2	20000	3	2020
273	230	2020-03-19	2	20000	3	2020
274	231	2020-03-19	2	20000	3	2020
275	232	2020-03-19	2	20000	3	2020
276	3	2020-03-19	2	20000	3	2020
277	233	2020-03-19	2	20000	3	2020
278	234	2020-03-19	2	20000	3	2020
279	1	2020-06-15	2	20000	6	2020
280	2	2020-06-15	2	20000	6	2020
281	4	2020-06-15	2	20000	6	2020
282	5	2020-06-15	2	20000	6	2020
283	6	2020-06-15	2	20000	6	2020
284	7	2020-06-15	2	20000	6	2020
285	8	2020-06-15	2	20000	6	2020
286	9	2020-06-15	2	20000	6	2020
287	10	2020-06-15	2	20000	6	2020
288	11	2020-06-15	2	20000	6	2020
289	12	2020-06-15	2	20000	6	2020
290	13	2020-06-15	2	20000	6	2020
291	14	2020-06-15	2	20000	6	2020
292	15	2020-06-15	2	20000	6	2020
293	16	2020-06-15	2	20000	6	2020
294	17	2020-06-15	2	20000	6	2020
295	18	2020-06-15	2	20000	6	2020
296	19	2020-06-15	2	20000	6	2020
297	20	2020-06-15	2	20000	6	2020
298	21	2020-06-15	2	20000	6	2020
299	22	2020-06-15	2	20000	6	2020
300	23	2020-06-15	2	20000	6	2020
301	24	2020-06-15	2	20000	6	2020
302	25	2020-06-15	2	20000	6	2020
303	26	2020-06-15	2	20000	6	2020
304	27	2020-06-15	2	20000	6	2020
305	28	2020-06-15	2	20000	6	2020
306	29	2020-06-15	2	20000	6	2020
307	30	2020-06-15	2	20000	6	2020
308	31	2020-06-15	2	20000	6	2020
309	32	2020-06-15	2	20000	6	2020
310	33	2020-06-15	2	20000	6	2020
311	34	2020-06-15	2	20000	6	2020
312	35	2020-06-15	2	20000	6	2020
313	36	2020-06-15	2	20000	6	2020
314	37	2020-06-15	2	20000	6	2020
315	38	2020-06-15	2	20000	6	2020
316	39	2020-06-15	2	20000	6	2020
317	40	2020-06-15	2	20000	6	2020
318	41	2020-06-15	2	20000	6	2020
319	42	2020-06-15	2	20000	6	2020
320	43	2020-06-15	2	20000	6	2020
321	44	2020-06-15	2	20000	6	2020
322	45	2020-06-15	2	20000	6	2020
323	46	2020-06-15	2	20000	6	2020
324	47	2020-06-15	2	20000	6	2020
325	48	2020-06-15	2	20000	6	2020
326	49	2020-06-15	2	20000	6	2020
327	50	2020-06-15	2	20000	6	2020
328	51	2020-06-15	2	20000	6	2020
329	52	2020-06-15	2	20000	6	2020
330	53	2020-06-15	2	20000	6	2020
331	54	2020-06-15	2	20000	6	2020
332	55	2020-06-15	2	20000	6	2020
333	56	2020-06-15	2	20000	6	2020
334	57	2020-06-15	2	20000	6	2020
335	58	2020-06-15	2	20000	6	2020
336	59	2020-06-15	2	20000	6	2020
337	60	2020-06-15	2	20000	6	2020
338	61	2020-06-15	2	20000	6	2020
339	62	2020-06-15	2	20000	6	2020
340	63	2020-06-15	2	20000	6	2020
341	64	2020-06-15	2	20000	6	2020
342	65	2020-06-15	2	20000	6	2020
343	66	2020-06-15	2	20000	6	2020
344	67	2020-06-15	2	20000	6	2020
345	68	2020-06-15	2	20000	6	2020
346	69	2020-06-15	2	20000	6	2020
347	70	2020-06-15	2	20000	6	2020
348	71	2020-06-15	2	20000	6	2020
349	72	2020-06-15	2	20000	6	2020
350	73	2020-06-15	2	20000	6	2020
351	74	2020-06-15	2	20000	6	2020
352	75	2020-06-15	2	20000	6	2020
353	76	2020-06-15	2	20000	6	2020
354	77	2020-06-15	2	20000	6	2020
355	78	2020-06-15	2	20000	6	2020
356	79	2020-06-15	2	20000	6	2020
357	80	2020-06-15	2	20000	6	2020
358	81	2020-06-15	2	20000	6	2020
359	82	2020-06-15	2	20000	6	2020
360	83	2020-06-15	2	20000	6	2020
361	84	2020-06-15	2	20000	6	2020
362	85	2020-06-15	2	20000	6	2020
363	86	2020-06-15	2	20000	6	2020
364	87	2020-06-15	2	20000	6	2020
365	88	2020-06-15	2	20000	6	2020
366	89	2020-06-15	2	20000	6	2020
367	90	2020-06-15	2	20000	6	2020
368	91	2020-06-15	2	20000	6	2020
369	92	2020-06-15	2	20000	6	2020
370	93	2020-06-15	2	20000	6	2020
371	94	2020-06-15	2	20000	6	2020
372	95	2020-06-15	2	20000	6	2020
373	96	2020-06-15	2	20000	6	2020
374	97	2020-06-15	2	20000	6	2020
375	98	2020-06-15	2	20000	6	2020
376	99	2020-06-15	2	20000	6	2020
377	100	2020-06-15	2	20000	6	2020
378	101	2020-06-15	2	20000	6	2020
379	102	2020-06-15	2	20000	6	2020
380	103	2020-06-15	2	20000	6	2020
381	104	2020-06-15	2	20000	6	2020
382	105	2020-06-15	2	20000	6	2020
383	106	2020-06-15	2	20000	6	2020
384	107	2020-06-15	2	20000	6	2020
385	108	2020-06-15	2	20000	6	2020
386	109	2020-06-15	2	20000	6	2020
387	110	2020-06-15	2	20000	6	2020
388	111	2020-06-15	2	20000	6	2020
389	112	2020-06-15	2	20000	6	2020
390	113	2020-06-15	2	20000	6	2020
391	114	2020-06-15	2	20000	6	2020
392	115	2020-06-15	2	20000	6	2020
393	116	2020-06-15	2	20000	6	2020
394	117	2020-06-15	2	20000	6	2020
395	118	2020-06-15	2	20000	6	2020
396	119	2020-06-15	2	20000	6	2020
397	120	2020-06-15	2	20000	6	2020
398	121	2020-06-15	2	20000	6	2020
399	122	2020-06-15	2	20000	6	2020
400	123	2020-06-15	2	20000	6	2020
401	124	2020-06-15	2	20000	6	2020
402	125	2020-06-15	2	20000	6	2020
403	126	2020-06-15	2	20000	6	2020
404	127	2020-06-15	2	20000	6	2020
405	128	2020-06-15	2	20000	6	2020
406	129	2020-06-15	2	20000	6	2020
407	130	2020-06-15	2	20000	6	2020
408	131	2020-06-15	2	20000	6	2020
409	132	2020-06-15	2	20000	6	2020
410	133	2020-06-15	2	20000	6	2020
411	134	2020-06-15	2	20000	6	2020
412	135	2020-06-15	2	20000	6	2020
413	136	2020-06-15	2	20000	6	2020
414	137	2020-06-15	2	20000	6	2020
415	138	2020-06-15	2	20000	6	2020
416	139	2020-06-15	2	20000	6	2020
417	140	2020-06-15	2	20000	6	2020
418	141	2020-06-15	2	20000	6	2020
419	142	2020-06-15	2	20000	6	2020
420	143	2020-06-15	2	20000	6	2020
421	144	2020-06-15	2	20000	6	2020
422	145	2020-06-15	2	20000	6	2020
423	146	2020-06-15	2	20000	6	2020
424	147	2020-06-15	2	20000	6	2020
425	148	2020-06-15	2	20000	6	2020
426	149	2020-06-15	2	20000	6	2020
427	150	2020-06-15	2	20000	6	2020
428	151	2020-06-15	2	20000	6	2020
429	152	2020-06-15	2	20000	6	2020
430	153	2020-06-15	2	20000	6	2020
431	154	2020-06-15	2	20000	6	2020
432	155	2020-06-15	2	20000	6	2020
433	156	2020-06-15	2	20000	6	2020
434	157	2020-06-15	2	20000	6	2020
435	158	2020-06-15	2	20000	6	2020
436	159	2020-06-15	2	20000	6	2020
437	160	2020-06-15	2	20000	6	2020
438	161	2020-06-15	2	20000	6	2020
439	162	2020-06-15	2	20000	6	2020
440	163	2020-06-15	2	20000	6	2020
441	164	2020-06-15	2	20000	6	2020
442	165	2020-06-15	2	20000	6	2020
443	166	2020-06-15	2	20000	6	2020
444	167	2020-06-15	2	20000	6	2020
445	168	2020-06-15	2	20000	6	2020
446	169	2020-06-15	2	20000	6	2020
447	171	2020-06-15	2	20000	6	2020
448	172	2020-06-15	2	20000	6	2020
449	174	2020-06-15	2	20000	6	2020
450	175	2020-06-15	2	20000	6	2020
451	176	2020-06-15	2	20000	6	2020
452	177	2020-06-15	2	20000	6	2020
453	179	2020-06-15	2	20000	6	2020
454	180	2020-06-15	2	20000	6	2020
455	181	2020-06-15	2	20000	6	2020
456	170	2020-06-15	2	20000	6	2020
457	173	2020-06-15	2	20000	6	2020
458	178	2020-06-15	2	20000	6	2020
459	182	2020-06-15	2	20000	6	2020
460	183	2020-06-15	2	20000	6	2020
461	184	2020-06-15	2	20000	6	2020
462	185	2020-06-15	2	20000	6	2020
463	186	2020-06-15	2	20000	6	2020
464	187	2020-06-15	2	20000	6	2020
465	189	2020-06-15	2	20000	6	2020
466	190	2020-06-15	2	20000	6	2020
467	191	2020-06-15	2	20000	6	2020
468	192	2020-06-15	2	20000	6	2020
469	193	2020-06-15	2	20000	6	2020
470	194	2020-06-15	2	20000	6	2020
471	195	2020-06-15	2	20000	6	2020
472	196	2020-06-15	2	20000	6	2020
473	197	2020-06-15	2	20000	6	2020
474	198	2020-06-15	2	20000	6	2020
475	199	2020-06-15	2	20000	6	2020
476	200	2020-06-15	2	20000	6	2020
477	201	2020-06-15	2	20000	6	2020
478	202	2020-06-15	2	20000	6	2020
479	203	2020-06-15	2	20000	6	2020
480	204	2020-06-15	2	20000	6	2020
481	205	2020-06-15	2	20000	6	2020
482	206	2020-06-15	2	20000	6	2020
483	207	2020-06-15	2	20000	6	2020
484	208	2020-06-15	2	20000	6	2020
485	209	2020-06-15	2	20000	6	2020
486	210	2020-06-15	2	20000	6	2020
487	211	2020-06-15	2	20000	6	2020
488	212	2020-06-15	2	20000	6	2020
489	213	2020-06-15	2	20000	6	2020
490	214	2020-06-15	2	20000	6	2020
491	215	2020-06-15	2	20000	6	2020
492	216	2020-06-15	2	20000	6	2020
493	217	2020-06-15	2	20000	6	2020
494	218	2020-06-15	2	20000	6	2020
495	219	2020-06-15	2	20000	6	2020
496	220	2020-06-15	2	20000	6	2020
497	221	2020-06-15	2	20000	6	2020
498	222	2020-06-15	2	20000	6	2020
499	223	2020-06-15	2	20000	6	2020
500	224	2020-06-15	2	20000	6	2020
501	225	2020-06-15	2	20000	6	2020
502	226	2020-06-15	2	20000	6	2020
503	227	2020-06-15	2	20000	6	2020
504	228	2020-06-15	2	20000	6	2020
505	229	2020-06-15	2	20000	6	2020
506	230	2020-06-15	2	20000	6	2020
507	231	2020-06-15	2	20000	6	2020
508	232	2020-06-15	2	20000	6	2020
509	3	2020-06-15	2	20000	6	2020
510	233	2020-06-15	2	20000	6	2020
511	234	2020-06-15	2	20000	6	2020
512	235	2020-06-15	2	20000	6	2020
513	236	2020-06-15	2	20000	6	2020
514	237	2020-06-15	2	20000	6	2020
515	238	2020-06-15	2	20000	6	2020
516	239	2020-06-15	2	20000	6	2020
517	240	2020-06-15	2	20000	6	2020
518	241	2020-06-15	2	20000	6	2020
519	242	2020-06-15	2	20000	6	2020
520	243	2020-06-15	2	20000	6	2020
521	244	2020-06-15	2	20000	6	2020
522	245	2020-06-15	2	20000	6	2020
523	254	2020-06-15	2	20000	6	2020
524	255	2020-06-15	2	20000	6	2020
525	253	2020-06-15	2	20000	6	2020
526	256	2020-06-15	2	20000	6	2020
527	257	2020-06-15	2	20000	6	2020
528	258	2020-06-15	2	20000	6	2020
529	259	2020-06-15	2	20000	6	2020
530	260	2020-06-15	2	20000	6	2020
531	261	2020-06-15	2	20000	6	2020
532	262	2020-06-15	2	20000	6	2020
533	263	2020-06-15	2	20000	6	2020
534	264	2020-06-15	2	20000	6	2020
535	265	2020-06-15	2	20000	6	2020
536	266	2020-06-15	2	20000	6	2020
537	267	2020-06-15	2	20000	6	2020
538	268	2020-06-15	2	20000	6	2020
539	269	2020-06-15	2	20000	6	2020
540	270	2020-06-15	2	20000	6	2020
541	271	2020-06-15	2	20000	6	2020
542	272	2020-06-15	2	20000	6	2020
543	273	2020-06-15	2	20000	6	2020
544	274	2020-06-15	2	20000	6	2020
545	275	2020-06-15	2	20000	6	2020
546	276	2020-06-15	2	20000	6	2020
547	277	2020-06-15	2	20000	6	2020
548	278	2020-06-15	2	20000	6	2020
549	279	2020-06-15	2	20000	6	2020
550	280	2020-06-15	2	20000	6	2020
551	281	2020-06-15	2	20000	6	2020
552	282	2020-06-15	2	20000	6	2020
553	283	2020-06-15	2	20000	6	2020
554	284	2020-06-15	2	20000	6	2020
555	285	2020-06-15	2	20000	6	2020
556	286	2020-06-15	2	20000	6	2020
557	287	2020-06-15	2	20000	6	2020
558	288	2020-06-15	2	20000	6	2020
559	289	2020-06-15	2	20000	6	2020
560	290	2020-06-15	2	20000	6	2020
561	291	2020-06-15	2	20000	6	2020
562	292	2020-06-15	2	20000	6	2020
563	293	2020-06-15	2	20000	6	2020
564	294	2020-06-15	2	20000	6	2020
565	295	2020-06-15	2	20000	6	2020
566	296	2020-06-15	2	20000	6	2020
567	303	2020-06-15	2	20000	6	2020
568	304	2020-06-15	2	20000	6	2020
569	305	2020-06-15	2	20000	6	2020
570	306	2020-06-15	2	20000	6	2020
571	311	2020-06-15	2	20000	6	2020
572	315	2020-06-15	2	20000	6	2020
573	316	2020-06-15	2	20000	6	2020
574	317	2020-06-15	2	20000	6	2020
575	318	2020-06-15	2	20000	6	2020
576	319	2020-06-15	2	20000	6	2020
577	320	2020-06-15	2	20000	6	2020
578	321	2020-06-15	2	20000	6	2020
579	322	2020-06-15	2	20000	6	2020
580	323	2020-06-15	2	20000	6	2020
581	324	2020-06-15	2	20000	6	2020
582	325	2020-06-15	2	20000	6	2020
583	326	2020-06-15	2	20000	6	2020
584	327	2020-06-15	2	20000	6	2020
585	328	2020-06-15	2	20000	6	2020
586	329	2020-06-15	2	20000	6	2020
587	330	2020-06-15	2	20000	6	2020
588	333	2020-06-15	2	20000	6	2020
589	335	2020-06-15	2	20000	6	2020
590	336	2020-06-15	2	20000	6	2020
591	337	2020-06-15	2	20000	6	2020
592	338	2020-06-15	2	20000	6	2020
593	339	2020-06-15	2	20000	6	2020
594	340	2020-06-15	2	20000	6	2020
595	341	2020-06-15	2	20000	6	2020
596	343	2020-06-15	2	20000	6	2020
597	344	2020-06-15	2	20000	6	2020
598	345	2020-06-15	2	20000	6	2020
599	346	2020-06-15	2	20000	6	2020
600	347	2020-06-15	2	20000	6	2020
601	348	2020-06-15	2	20000	6	2020
602	349	2020-06-15	2	20000	6	2020
603	350	2020-06-15	2	20000	6	2020
604	351	2020-06-15	2	20000	6	2020
605	352	2020-06-15	2	20000	6	2020
606	353	2020-06-15	2	20000	6	2020
607	354	2020-06-15	2	20000	6	2020
608	356	2020-06-15	2	20000	6	2020
609	357	2020-06-15	2	20000	6	2020
610	358	2020-06-15	2	20000	6	2020
611	359	2020-06-15	2	20000	6	2020
612	360	2020-06-15	2	20000	6	2020
613	361	2020-06-15	2	20000	6	2020
614	362	2020-06-15	2	20000	6	2020
615	363	2020-06-15	2	20000	6	2020
616	364	2020-06-15	2	20000	6	2020
617	365	2020-06-15	2	20000	6	2020
618	366	2020-06-15	2	20000	6	2020
619	367	2020-06-15	2	20000	6	2020
620	368	2020-06-15	2	20000	6	2020
621	369	2020-06-15	2	20000	6	2020
622	370	2020-06-15	2	20000	6	2020
623	374	2020-06-15	2	20000	6	2020
624	375	2020-06-15	2	20000	6	2020
625	376	2020-06-15	2	20000	6	2020
626	379	2020-06-15	2	20000	6	2020
627	380	2020-06-15	2	20000	6	2020
628	382	2020-06-15	2	20000	6	2020
629	384	2020-06-15	2	20000	6	2020
630	385	2020-06-15	2	20000	6	2020
631	386	2020-06-15	2	20000	6	2020
993	1	2020-07-16	2	20000	7	2020
994	2	2020-07-16	2	20000	7	2020
995	4	2020-07-16	2	20000	7	2020
996	5	2020-07-16	2	20000	7	2020
997	6	2020-07-16	2	20000	7	2020
998	7	2020-07-16	2	20000	7	2020
999	8	2020-07-16	2	20000	7	2020
1000	9	2020-07-16	2	20000	7	2020
1001	10	2020-07-16	2	20000	7	2020
1002	11	2020-07-16	2	20000	7	2020
1003	12	2020-07-16	2	20000	7	2020
1004	13	2020-07-16	2	20000	7	2020
1005	14	2020-07-16	2	20000	7	2020
1006	15	2020-07-16	2	20000	7	2020
1007	16	2020-07-16	2	20000	7	2020
1008	17	2020-07-16	2	20000	7	2020
1009	18	2020-07-16	2	20000	7	2020
1010	19	2020-07-16	2	20000	7	2020
1011	20	2020-07-16	2	20000	7	2020
1012	21	2020-07-16	2	20000	7	2020
1013	22	2020-07-16	2	20000	7	2020
1014	23	2020-07-16	2	20000	7	2020
1015	24	2020-07-16	2	20000	7	2020
1016	25	2020-07-16	2	20000	7	2020
1017	26	2020-07-16	2	20000	7	2020
1018	27	2020-07-16	2	20000	7	2020
1019	28	2020-07-16	2	20000	7	2020
1020	29	2020-07-16	2	20000	7	2020
1021	30	2020-07-16	2	20000	7	2020
1022	31	2020-07-16	2	20000	7	2020
1023	32	2020-07-16	2	20000	7	2020
1024	33	2020-07-16	2	20000	7	2020
1025	34	2020-07-16	2	20000	7	2020
1026	35	2020-07-16	2	20000	7	2020
1027	36	2020-07-16	2	20000	7	2020
1028	37	2020-07-16	2	20000	7	2020
1029	38	2020-07-16	2	20000	7	2020
1030	39	2020-07-16	2	20000	7	2020
1031	40	2020-07-16	2	20000	7	2020
1032	41	2020-07-16	2	20000	7	2020
1033	42	2020-07-16	2	20000	7	2020
1034	43	2020-07-16	2	20000	7	2020
1035	44	2020-07-16	2	20000	7	2020
1036	45	2020-07-16	2	20000	7	2020
1037	46	2020-07-16	2	20000	7	2020
1038	47	2020-07-16	2	20000	7	2020
1039	48	2020-07-16	2	20000	7	2020
1040	49	2020-07-16	2	20000	7	2020
1041	50	2020-07-16	2	20000	7	2020
1042	51	2020-07-16	2	20000	7	2020
1043	52	2020-07-16	2	20000	7	2020
1044	53	2020-07-16	2	20000	7	2020
1045	54	2020-07-16	2	20000	7	2020
1046	55	2020-07-16	2	20000	7	2020
1047	56	2020-07-16	2	20000	7	2020
1048	57	2020-07-16	2	20000	7	2020
1049	58	2020-07-16	2	20000	7	2020
1050	59	2020-07-16	2	20000	7	2020
1051	60	2020-07-16	2	20000	7	2020
1052	61	2020-07-16	2	20000	7	2020
1053	62	2020-07-16	2	20000	7	2020
1054	63	2020-07-16	2	20000	7	2020
1055	64	2020-07-16	2	20000	7	2020
1056	65	2020-07-16	2	20000	7	2020
1057	66	2020-07-16	2	20000	7	2020
1058	67	2020-07-16	2	20000	7	2020
1059	68	2020-07-16	2	20000	7	2020
1060	69	2020-07-16	2	20000	7	2020
1061	70	2020-07-16	2	20000	7	2020
1062	71	2020-07-16	2	20000	7	2020
1063	72	2020-07-16	2	20000	7	2020
1064	73	2020-07-16	2	20000	7	2020
1065	74	2020-07-16	2	20000	7	2020
1066	75	2020-07-16	2	20000	7	2020
1067	76	2020-07-16	2	20000	7	2020
1068	77	2020-07-16	2	20000	7	2020
1069	78	2020-07-16	2	20000	7	2020
1070	79	2020-07-16	2	20000	7	2020
1071	80	2020-07-16	2	20000	7	2020
1072	81	2020-07-16	2	20000	7	2020
1073	82	2020-07-16	2	20000	7	2020
1074	83	2020-07-16	2	20000	7	2020
1075	84	2020-07-16	2	20000	7	2020
1076	85	2020-07-16	2	20000	7	2020
1077	86	2020-07-16	2	20000	7	2020
1078	87	2020-07-16	2	20000	7	2020
1079	88	2020-07-16	2	20000	7	2020
1080	89	2020-07-16	2	20000	7	2020
1081	90	2020-07-16	2	20000	7	2020
1082	91	2020-07-16	2	20000	7	2020
1083	92	2020-07-16	2	20000	7	2020
1084	93	2020-07-16	2	20000	7	2020
1085	94	2020-07-16	2	20000	7	2020
1086	95	2020-07-16	2	20000	7	2020
1087	96	2020-07-16	2	20000	7	2020
1088	97	2020-07-16	2	20000	7	2020
1089	98	2020-07-16	2	20000	7	2020
1090	99	2020-07-16	2	20000	7	2020
1091	100	2020-07-16	2	20000	7	2020
1092	101	2020-07-16	2	20000	7	2020
1093	102	2020-07-16	2	20000	7	2020
1094	103	2020-07-16	2	20000	7	2020
1095	104	2020-07-16	2	20000	7	2020
1096	105	2020-07-16	2	20000	7	2020
1097	106	2020-07-16	2	20000	7	2020
1098	107	2020-07-16	2	20000	7	2020
1099	108	2020-07-16	2	20000	7	2020
1100	109	2020-07-16	2	20000	7	2020
1101	110	2020-07-16	2	20000	7	2020
1102	111	2020-07-16	2	20000	7	2020
1103	112	2020-07-16	2	20000	7	2020
1104	113	2020-07-16	2	20000	7	2020
1105	114	2020-07-16	2	20000	7	2020
1106	115	2020-07-16	2	20000	7	2020
1107	116	2020-07-16	2	20000	7	2020
1108	117	2020-07-16	2	20000	7	2020
1109	118	2020-07-16	2	20000	7	2020
1110	119	2020-07-16	2	20000	7	2020
1111	120	2020-07-16	2	20000	7	2020
1112	121	2020-07-16	2	20000	7	2020
1113	122	2020-07-16	2	20000	7	2020
1114	123	2020-07-16	2	20000	7	2020
1115	124	2020-07-16	2	20000	7	2020
1116	125	2020-07-16	2	20000	7	2020
1117	126	2020-07-16	2	20000	7	2020
1118	127	2020-07-16	2	20000	7	2020
1119	128	2020-07-16	2	20000	7	2020
1120	129	2020-07-16	2	20000	7	2020
1121	130	2020-07-16	2	20000	7	2020
1122	131	2020-07-16	2	20000	7	2020
1123	132	2020-07-16	2	20000	7	2020
1124	133	2020-07-16	2	20000	7	2020
1125	134	2020-07-16	2	20000	7	2020
1126	135	2020-07-16	2	20000	7	2020
1127	136	2020-07-16	2	20000	7	2020
1128	137	2020-07-16	2	20000	7	2020
1129	138	2020-07-16	2	20000	7	2020
1130	139	2020-07-16	2	20000	7	2020
1131	140	2020-07-16	2	20000	7	2020
1132	141	2020-07-16	2	20000	7	2020
1133	142	2020-07-16	2	20000	7	2020
1134	143	2020-07-16	2	20000	7	2020
1135	144	2020-07-16	2	20000	7	2020
1136	145	2020-07-16	2	20000	7	2020
1137	146	2020-07-16	2	20000	7	2020
1138	147	2020-07-16	2	20000	7	2020
1139	148	2020-07-16	2	20000	7	2020
1140	149	2020-07-16	2	20000	7	2020
1141	150	2020-07-16	2	20000	7	2020
1142	151	2020-07-16	2	20000	7	2020
1143	152	2020-07-16	2	20000	7	2020
1144	153	2020-07-16	2	20000	7	2020
1145	154	2020-07-16	2	20000	7	2020
1146	155	2020-07-16	2	20000	7	2020
1147	156	2020-07-16	2	20000	7	2020
1148	157	2020-07-16	2	20000	7	2020
1149	158	2020-07-16	2	20000	7	2020
1150	159	2020-07-16	2	20000	7	2020
1151	160	2020-07-16	2	20000	7	2020
1152	161	2020-07-16	2	20000	7	2020
1153	162	2020-07-16	2	20000	7	2020
1154	163	2020-07-16	2	20000	7	2020
1155	164	2020-07-16	2	20000	7	2020
1156	165	2020-07-16	2	20000	7	2020
1157	166	2020-07-16	2	20000	7	2020
1158	167	2020-07-16	2	20000	7	2020
1159	168	2020-07-16	2	20000	7	2020
1160	169	2020-07-16	2	20000	7	2020
1161	171	2020-07-16	2	20000	7	2020
1162	172	2020-07-16	2	20000	7	2020
1163	174	2020-07-16	2	20000	7	2020
1164	175	2020-07-16	2	20000	7	2020
1165	176	2020-07-16	2	20000	7	2020
1166	177	2020-07-16	2	20000	7	2020
1167	179	2020-07-16	2	20000	7	2020
1168	180	2020-07-16	2	20000	7	2020
1169	181	2020-07-16	2	20000	7	2020
1170	170	2020-07-16	2	20000	7	2020
1171	173	2020-07-16	2	20000	7	2020
1172	178	2020-07-16	2	20000	7	2020
1173	182	2020-07-16	2	20000	7	2020
1174	183	2020-07-16	2	20000	7	2020
1175	184	2020-07-16	2	20000	7	2020
1176	185	2020-07-16	2	20000	7	2020
1177	186	2020-07-16	2	20000	7	2020
1178	187	2020-07-16	2	20000	7	2020
1179	189	2020-07-16	2	20000	7	2020
1180	190	2020-07-16	2	20000	7	2020
1181	191	2020-07-16	2	20000	7	2020
1182	192	2020-07-16	2	20000	7	2020
1183	193	2020-07-16	2	20000	7	2020
1184	194	2020-07-16	2	20000	7	2020
1185	195	2020-07-16	2	20000	7	2020
1186	196	2020-07-16	2	20000	7	2020
1187	197	2020-07-16	2	20000	7	2020
1188	198	2020-07-16	2	20000	7	2020
1189	199	2020-07-16	2	20000	7	2020
1190	200	2020-07-16	2	20000	7	2020
1191	201	2020-07-16	2	20000	7	2020
1192	202	2020-07-16	2	20000	7	2020
1193	203	2020-07-16	2	20000	7	2020
1194	204	2020-07-16	2	20000	7	2020
1195	205	2020-07-16	2	20000	7	2020
1196	206	2020-07-16	2	20000	7	2020
1197	207	2020-07-16	2	20000	7	2020
1198	208	2020-07-16	2	20000	7	2020
1199	209	2020-07-16	2	20000	7	2020
1200	210	2020-07-16	2	20000	7	2020
1201	211	2020-07-16	2	20000	7	2020
1202	212	2020-07-16	2	20000	7	2020
1203	213	2020-07-16	2	20000	7	2020
1204	214	2020-07-16	2	20000	7	2020
1205	215	2020-07-16	2	20000	7	2020
1206	217	2020-07-16	2	20000	7	2020
1207	218	2020-07-16	2	20000	7	2020
1208	219	2020-07-16	2	20000	7	2020
1209	220	2020-07-16	2	20000	7	2020
1210	221	2020-07-16	2	20000	7	2020
1211	222	2020-07-16	2	20000	7	2020
1212	223	2020-07-16	2	20000	7	2020
1213	224	2020-07-16	2	20000	7	2020
1214	225	2020-07-16	2	20000	7	2020
1215	226	2020-07-16	2	20000	7	2020
1216	227	2020-07-16	2	20000	7	2020
1217	228	2020-07-16	2	20000	7	2020
1218	229	2020-07-16	2	20000	7	2020
1219	230	2020-07-16	2	20000	7	2020
1220	231	2020-07-16	2	20000	7	2020
1221	232	2020-07-16	2	20000	7	2020
1222	3	2020-07-16	2	20000	7	2020
1223	233	2020-07-16	2	20000	7	2020
1224	234	2020-07-16	2	20000	7	2020
1225	235	2020-07-16	2	20000	7	2020
1226	236	2020-07-16	2	20000	7	2020
1227	237	2020-07-16	2	20000	7	2020
1228	238	2020-07-16	2	20000	7	2020
1229	239	2020-07-16	2	20000	7	2020
1230	240	2020-07-16	2	20000	7	2020
1231	242	2020-07-16	2	20000	7	2020
1232	243	2020-07-16	2	20000	7	2020
1233	244	2020-07-16	2	20000	7	2020
1234	245	2020-07-16	2	20000	7	2020
1235	254	2020-07-16	2	20000	7	2020
1236	255	2020-07-16	2	20000	7	2020
1237	253	2020-07-16	2	20000	7	2020
1238	256	2020-07-16	2	20000	7	2020
1239	257	2020-07-16	2	20000	7	2020
1240	258	2020-07-16	2	20000	7	2020
1241	259	2020-07-16	2	20000	7	2020
1242	260	2020-07-16	2	20000	7	2020
1243	261	2020-07-16	2	20000	7	2020
1244	262	2020-07-16	2	20000	7	2020
1245	263	2020-07-16	2	20000	7	2020
1246	264	2020-07-16	2	20000	7	2020
1247	265	2020-07-16	2	20000	7	2020
1248	266	2020-07-16	2	20000	7	2020
1249	267	2020-07-16	2	20000	7	2020
1250	268	2020-07-16	2	20000	7	2020
1251	269	2020-07-16	2	20000	7	2020
1252	270	2020-07-16	2	20000	7	2020
1253	271	2020-07-16	2	20000	7	2020
1254	272	2020-07-16	2	20000	7	2020
1255	273	2020-07-16	2	20000	7	2020
1256	274	2020-07-16	2	20000	7	2020
1257	275	2020-07-16	2	20000	7	2020
1258	276	2020-07-16	2	20000	7	2020
1259	277	2020-07-16	2	20000	7	2020
1260	278	2020-07-16	2	20000	7	2020
1261	279	2020-07-16	2	20000	7	2020
1262	280	2020-07-16	2	20000	7	2020
1263	281	2020-07-16	2	20000	7	2020
1264	282	2020-07-16	2	20000	7	2020
1265	283	2020-07-16	2	20000	7	2020
1266	284	2020-07-16	2	20000	7	2020
1267	285	2020-07-16	2	20000	7	2020
1268	286	2020-07-16	2	20000	7	2020
1269	287	2020-07-16	2	20000	7	2020
1270	288	2020-07-16	2	20000	7	2020
1271	289	2020-07-16	2	20000	7	2020
1272	290	2020-07-16	2	20000	7	2020
1273	291	2020-07-16	2	20000	7	2020
1274	292	2020-07-16	2	20000	7	2020
1275	293	2020-07-16	2	20000	7	2020
1276	294	2020-07-16	2	20000	7	2020
1277	295	2020-07-16	2	20000	7	2020
1278	296	2020-07-16	2	20000	7	2020
1279	303	2020-07-16	2	20000	7	2020
1280	304	2020-07-16	2	20000	7	2020
1281	305	2020-07-16	2	20000	7	2020
1282	306	2020-07-16	2	20000	7	2020
1283	311	2020-07-16	2	20000	7	2020
1284	315	2020-07-16	2	20000	7	2020
1285	316	2020-07-16	2	20000	7	2020
1286	317	2020-07-16	2	20000	7	2020
1287	318	2020-07-16	2	20000	7	2020
1288	319	2020-07-16	2	20000	7	2020
1289	320	2020-07-16	2	20000	7	2020
1290	321	2020-07-16	2	20000	7	2020
1291	322	2020-07-16	2	20000	7	2020
1292	323	2020-07-16	2	20000	7	2020
1293	324	2020-07-16	2	20000	7	2020
1294	325	2020-07-16	2	20000	7	2020
1295	326	2020-07-16	2	20000	7	2020
1296	327	2020-07-16	2	20000	7	2020
1297	328	2020-07-16	2	20000	7	2020
1298	329	2020-07-16	2	20000	7	2020
1299	330	2020-07-16	2	20000	7	2020
1300	333	2020-07-16	2	20000	7	2020
1301	335	2020-07-16	2	20000	7	2020
1302	336	2020-07-16	2	20000	7	2020
1303	337	2020-07-16	2	20000	7	2020
1304	338	2020-07-16	2	20000	7	2020
1305	339	2020-07-16	2	20000	7	2020
1306	340	2020-07-16	2	20000	7	2020
1307	341	2020-07-16	2	20000	7	2020
1308	343	2020-07-16	2	20000	7	2020
1309	344	2020-07-16	2	20000	7	2020
1310	345	2020-07-16	2	20000	7	2020
1311	346	2020-07-16	2	20000	7	2020
1312	347	2020-07-16	2	20000	7	2020
1313	348	2020-07-16	2	20000	7	2020
1314	349	2020-07-16	2	20000	7	2020
1315	350	2020-07-16	2	20000	7	2020
1316	351	2020-07-16	2	20000	7	2020
1317	352	2020-07-16	2	20000	7	2020
1318	353	2020-07-16	2	20000	7	2020
1319	354	2020-07-16	2	20000	7	2020
1320	356	2020-07-16	2	20000	7	2020
1321	357	2020-07-16	2	20000	7	2020
1322	358	2020-07-16	2	20000	7	2020
1323	359	2020-07-16	2	20000	7	2020
1324	360	2020-07-16	2	20000	7	2020
1325	361	2020-07-16	2	20000	7	2020
1326	362	2020-07-16	2	20000	7	2020
1327	363	2020-07-16	2	20000	7	2020
1328	364	2020-07-16	2	20000	7	2020
1329	365	2020-07-16	2	20000	7	2020
1330	366	2020-07-16	2	20000	7	2020
1331	367	2020-07-16	2	20000	7	2020
1332	368	2020-07-16	2	20000	7	2020
1333	369	2020-07-16	2	20000	7	2020
1334	370	2020-07-16	2	20000	7	2020
1335	374	2020-07-16	2	20000	7	2020
1336	375	2020-07-16	2	20000	7	2020
1337	376	2020-07-16	2	20000	7	2020
1338	379	2020-07-16	2	20000	7	2020
1339	380	2020-07-16	2	20000	7	2020
1340	382	2020-07-16	2	20000	7	2020
1341	384	2020-07-16	2	20000	7	2020
1342	385	2020-07-16	2	20000	7	2020
1343	386	2020-07-16	2	20000	7	2020
1344	387	2020-07-16	2	20000	7	2020
1345	388	2020-07-16	2	20000	7	2020
1346	389	2020-07-16	2	20000	7	2020
1347	390	2020-07-16	2	20000	7	2020
1348	241	2020-07-16	2	20000	7	2020
1349	392	2020-07-16	2	20000	7	2020
1350	393	2020-07-16	2	20000	7	2020
1351	394	2020-07-16	2	20000	7	2020
1352	395	2020-07-16	2	20000	7	2020
1353	396	2020-07-16	2	20000	7	2020
1354	216	2020-07-16	2	20000	7	2020
1355	1	2020-08-14	2	20000	8	2020
1356	2	2020-08-14	2	20000	8	2020
1357	4	2020-08-14	2	20000	8	2020
1358	5	2020-08-14	2	20000	8	2020
1359	6	2020-08-14	2	20000	8	2020
1360	7	2020-08-14	2	20000	8	2020
1361	8	2020-08-14	2	20000	8	2020
1362	10	2020-08-14	2	20000	8	2020
1363	11	2020-08-14	2	20000	8	2020
1364	12	2020-08-14	2	20000	8	2020
1365	13	2020-08-14	2	20000	8	2020
1366	14	2020-08-14	2	20000	8	2020
1367	15	2020-08-14	2	20000	8	2020
1368	16	2020-08-14	2	20000	8	2020
1369	17	2020-08-14	2	20000	8	2020
1370	18	2020-08-14	2	20000	8	2020
1371	19	2020-08-14	2	20000	8	2020
1372	20	2020-08-14	2	20000	8	2020
1373	21	2020-08-14	2	20000	8	2020
1374	22	2020-08-14	2	20000	8	2020
1375	23	2020-08-14	2	20000	8	2020
1376	24	2020-08-14	2	20000	8	2020
1377	25	2020-08-14	2	20000	8	2020
1378	26	2020-08-14	2	20000	8	2020
1379	27	2020-08-14	2	20000	8	2020
1380	28	2020-08-14	2	20000	8	2020
1381	29	2020-08-14	2	20000	8	2020
1382	30	2020-08-14	2	20000	8	2020
1383	31	2020-08-14	2	20000	8	2020
1384	32	2020-08-14	2	20000	8	2020
1385	33	2020-08-14	2	20000	8	2020
1386	34	2020-08-14	2	20000	8	2020
1387	35	2020-08-14	2	20000	8	2020
1388	36	2020-08-14	2	20000	8	2020
1389	37	2020-08-14	2	20000	8	2020
1390	38	2020-08-14	2	20000	8	2020
1391	39	2020-08-14	2	20000	8	2020
1392	40	2020-08-14	2	20000	8	2020
1393	41	2020-08-14	2	20000	8	2020
1394	42	2020-08-14	2	20000	8	2020
1395	43	2020-08-14	2	20000	8	2020
1396	44	2020-08-14	2	20000	8	2020
1397	45	2020-08-14	2	20000	8	2020
1398	46	2020-08-14	2	20000	8	2020
1399	47	2020-08-14	2	20000	8	2020
1400	48	2020-08-14	2	20000	8	2020
1401	49	2020-08-14	2	20000	8	2020
1402	50	2020-08-14	2	20000	8	2020
1403	51	2020-08-14	2	20000	8	2020
1404	52	2020-08-14	2	20000	8	2020
1405	53	2020-08-14	2	20000	8	2020
1406	54	2020-08-14	2	20000	8	2020
1407	55	2020-08-14	2	20000	8	2020
1408	56	2020-08-14	2	20000	8	2020
1409	57	2020-08-14	2	20000	8	2020
1410	58	2020-08-14	2	20000	8	2020
1411	59	2020-08-14	2	20000	8	2020
1412	60	2020-08-14	2	20000	8	2020
1413	61	2020-08-14	2	20000	8	2020
1414	62	2020-08-14	2	20000	8	2020
1415	63	2020-08-14	2	20000	8	2020
1416	64	2020-08-14	2	20000	8	2020
1417	65	2020-08-14	2	20000	8	2020
1418	66	2020-08-14	2	20000	8	2020
1419	67	2020-08-14	2	20000	8	2020
1420	69	2020-08-14	2	20000	8	2020
1421	70	2020-08-14	2	20000	8	2020
1422	71	2020-08-14	2	20000	8	2020
1423	72	2020-08-14	2	20000	8	2020
1424	73	2020-08-14	2	20000	8	2020
1425	74	2020-08-14	2	20000	8	2020
1426	75	2020-08-14	2	20000	8	2020
1427	76	2020-08-14	2	20000	8	2020
1428	77	2020-08-14	2	20000	8	2020
1429	78	2020-08-14	2	20000	8	2020
1430	79	2020-08-14	2	20000	8	2020
1431	80	2020-08-14	2	20000	8	2020
1432	81	2020-08-14	2	20000	8	2020
1433	82	2020-08-14	2	20000	8	2020
1434	83	2020-08-14	2	20000	8	2020
1435	84	2020-08-14	2	20000	8	2020
1436	85	2020-08-14	2	20000	8	2020
1437	86	2020-08-14	2	20000	8	2020
1438	87	2020-08-14	2	20000	8	2020
1439	88	2020-08-14	2	20000	8	2020
1440	89	2020-08-14	2	20000	8	2020
1441	90	2020-08-14	2	20000	8	2020
1442	91	2020-08-14	2	20000	8	2020
1443	92	2020-08-14	2	20000	8	2020
1444	9	2020-08-14	2	20000	8	2020
1445	93	2020-08-14	2	20000	8	2020
1446	94	2020-08-14	2	20000	8	2020
1447	95	2020-08-14	2	20000	8	2020
1448	96	2020-08-14	2	20000	8	2020
1449	97	2020-08-14	2	20000	8	2020
1450	98	2020-08-14	2	20000	8	2020
1451	99	2020-08-14	2	20000	8	2020
1452	100	2020-08-14	2	20000	8	2020
1453	101	2020-08-14	2	20000	8	2020
1454	102	2020-08-14	2	20000	8	2020
1455	103	2020-08-14	2	20000	8	2020
1456	104	2020-08-14	2	20000	8	2020
1457	105	2020-08-14	2	20000	8	2020
1458	106	2020-08-14	2	20000	8	2020
1459	107	2020-08-14	2	20000	8	2020
1460	108	2020-08-14	2	20000	8	2020
1461	109	2020-08-14	2	20000	8	2020
1462	110	2020-08-14	2	20000	8	2020
1463	111	2020-08-14	2	20000	8	2020
1464	112	2020-08-14	2	20000	8	2020
1465	113	2020-08-14	2	20000	8	2020
1466	114	2020-08-14	2	20000	8	2020
1467	115	2020-08-14	2	20000	8	2020
1468	116	2020-08-14	2	20000	8	2020
1469	117	2020-08-14	2	20000	8	2020
1470	118	2020-08-14	2	20000	8	2020
1471	119	2020-08-14	2	20000	8	2020
1472	120	2020-08-14	2	20000	8	2020
1473	121	2020-08-14	2	20000	8	2020
1474	122	2020-08-14	2	20000	8	2020
1475	123	2020-08-14	2	20000	8	2020
1476	124	2020-08-14	2	20000	8	2020
1477	125	2020-08-14	2	20000	8	2020
1478	126	2020-08-14	2	20000	8	2020
1479	127	2020-08-14	2	20000	8	2020
1480	128	2020-08-14	2	20000	8	2020
1481	129	2020-08-14	2	20000	8	2020
1482	130	2020-08-14	2	20000	8	2020
1483	131	2020-08-14	2	20000	8	2020
1484	132	2020-08-14	2	20000	8	2020
1485	133	2020-08-14	2	20000	8	2020
1486	134	2020-08-14	2	20000	8	2020
1487	135	2020-08-14	2	20000	8	2020
1488	136	2020-08-14	2	20000	8	2020
1489	137	2020-08-14	2	20000	8	2020
1490	138	2020-08-14	2	20000	8	2020
1491	139	2020-08-14	2	20000	8	2020
1492	140	2020-08-14	2	20000	8	2020
1493	141	2020-08-14	2	20000	8	2020
1494	142	2020-08-14	2	20000	8	2020
1495	143	2020-08-14	2	20000	8	2020
1496	144	2020-08-14	2	20000	8	2020
1497	145	2020-08-14	2	20000	8	2020
1498	146	2020-08-14	2	20000	8	2020
1499	147	2020-08-14	2	20000	8	2020
1500	148	2020-08-14	2	20000	8	2020
1501	149	2020-08-14	2	20000	8	2020
1502	150	2020-08-14	2	20000	8	2020
1503	151	2020-08-14	2	20000	8	2020
1504	152	2020-08-14	2	20000	8	2020
1505	153	2020-08-14	2	20000	8	2020
1506	154	2020-08-14	2	20000	8	2020
1507	155	2020-08-14	2	20000	8	2020
1508	156	2020-08-14	2	20000	8	2020
1509	157	2020-08-14	2	20000	8	2020
1510	158	2020-08-14	2	20000	8	2020
1511	159	2020-08-14	2	20000	8	2020
1512	160	2020-08-14	2	20000	8	2020
1513	161	2020-08-14	2	20000	8	2020
1514	162	2020-08-14	2	20000	8	2020
1515	163	2020-08-14	2	20000	8	2020
1516	164	2020-08-14	2	20000	8	2020
1517	165	2020-08-14	2	20000	8	2020
1518	166	2020-08-14	2	20000	8	2020
1519	167	2020-08-14	2	20000	8	2020
1520	168	2020-08-14	2	20000	8	2020
1521	169	2020-08-14	2	20000	8	2020
1522	171	2020-08-14	2	20000	8	2020
1523	172	2020-08-14	2	20000	8	2020
1524	174	2020-08-14	2	20000	8	2020
1525	175	2020-08-14	2	20000	8	2020
1526	176	2020-08-14	2	20000	8	2020
1527	177	2020-08-14	2	20000	8	2020
1528	179	2020-08-14	2	20000	8	2020
1529	180	2020-08-14	2	20000	8	2020
1530	181	2020-08-14	2	20000	8	2020
1531	170	2020-08-14	2	20000	8	2020
1532	173	2020-08-14	2	20000	8	2020
1533	178	2020-08-14	2	20000	8	2020
1534	182	2020-08-14	2	20000	8	2020
1535	183	2020-08-14	2	20000	8	2020
1536	184	2020-08-14	2	20000	8	2020
1537	185	2020-08-14	2	20000	8	2020
1538	186	2020-08-14	2	20000	8	2020
1539	187	2020-08-14	2	20000	8	2020
1540	189	2020-08-14	2	20000	8	2020
1541	190	2020-08-14	2	20000	8	2020
1542	191	2020-08-14	2	20000	8	2020
1543	192	2020-08-14	2	20000	8	2020
1544	193	2020-08-14	2	20000	8	2020
1545	194	2020-08-14	2	20000	8	2020
1546	195	2020-08-14	2	20000	8	2020
1547	196	2020-08-14	2	20000	8	2020
1548	197	2020-08-14	2	20000	8	2020
1549	198	2020-08-14	2	20000	8	2020
1550	199	2020-08-14	2	20000	8	2020
1551	200	2020-08-14	2	20000	8	2020
1552	201	2020-08-14	2	20000	8	2020
1553	202	2020-08-14	2	20000	8	2020
1554	203	2020-08-14	2	20000	8	2020
1555	204	2020-08-14	2	20000	8	2020
1556	205	2020-08-14	2	20000	8	2020
1557	206	2020-08-14	2	20000	8	2020
1558	207	2020-08-14	2	20000	8	2020
1559	208	2020-08-14	2	20000	8	2020
1560	209	2020-08-14	2	20000	8	2020
1561	210	2020-08-14	2	20000	8	2020
1562	211	2020-08-14	2	20000	8	2020
1563	212	2020-08-14	2	20000	8	2020
1564	213	2020-08-14	2	20000	8	2020
1565	214	2020-08-14	2	20000	8	2020
1566	215	2020-08-14	2	20000	8	2020
1567	217	2020-08-14	2	20000	8	2020
1568	218	2020-08-14	2	20000	8	2020
1569	219	2020-08-14	2	20000	8	2020
1570	220	2020-08-14	2	20000	8	2020
1571	221	2020-08-14	2	20000	8	2020
1572	222	2020-08-14	2	20000	8	2020
1573	223	2020-08-14	2	20000	8	2020
1574	224	2020-08-14	2	20000	8	2020
1575	225	2020-08-14	2	20000	8	2020
1576	226	2020-08-14	2	20000	8	2020
1577	227	2020-08-14	2	20000	8	2020
1578	228	2020-08-14	2	20000	8	2020
1579	229	2020-08-14	2	20000	8	2020
1580	230	2020-08-14	2	20000	8	2020
1581	231	2020-08-14	2	20000	8	2020
1582	232	2020-08-14	2	20000	8	2020
1583	3	2020-08-14	2	20000	8	2020
1584	233	2020-08-14	2	20000	8	2020
1585	234	2020-08-14	2	20000	8	2020
1586	235	2020-08-14	2	20000	8	2020
1587	236	2020-08-14	2	20000	8	2020
1588	237	2020-08-14	2	20000	8	2020
1589	238	2020-08-14	2	20000	8	2020
1590	239	2020-08-14	2	20000	8	2020
1591	240	2020-08-14	2	20000	8	2020
1592	242	2020-08-14	2	20000	8	2020
1593	243	2020-08-14	2	20000	8	2020
1594	244	2020-08-14	2	20000	8	2020
1595	245	2020-08-14	2	20000	8	2020
1596	254	2020-08-14	2	20000	8	2020
1597	255	2020-08-14	2	20000	8	2020
1598	253	2020-08-14	2	20000	8	2020
1599	256	2020-08-14	2	20000	8	2020
1600	257	2020-08-14	2	20000	8	2020
1601	258	2020-08-14	2	20000	8	2020
1602	259	2020-08-14	2	20000	8	2020
1603	260	2020-08-14	2	20000	8	2020
1604	261	2020-08-14	2	20000	8	2020
1605	262	2020-08-14	2	20000	8	2020
1606	263	2020-08-14	2	20000	8	2020
1607	264	2020-08-14	2	20000	8	2020
1608	265	2020-08-14	2	20000	8	2020
1609	266	2020-08-14	2	20000	8	2020
1610	267	2020-08-14	2	20000	8	2020
1611	268	2020-08-14	2	20000	8	2020
1612	269	2020-08-14	2	20000	8	2020
1613	270	2020-08-14	2	20000	8	2020
1614	271	2020-08-14	2	20000	8	2020
1615	272	2020-08-14	2	20000	8	2020
1616	273	2020-08-14	2	20000	8	2020
1617	274	2020-08-14	2	20000	8	2020
1618	275	2020-08-14	2	20000	8	2020
1619	276	2020-08-14	2	20000	8	2020
1620	277	2020-08-14	2	20000	8	2020
1621	278	2020-08-14	2	20000	8	2020
1622	279	2020-08-14	2	20000	8	2020
1623	280	2020-08-14	2	20000	8	2020
1624	281	2020-08-14	2	20000	8	2020
1625	282	2020-08-14	2	20000	8	2020
1626	283	2020-08-14	2	20000	8	2020
1627	284	2020-08-14	2	20000	8	2020
1628	285	2020-08-14	2	20000	8	2020
1629	286	2020-08-14	2	20000	8	2020
1630	287	2020-08-14	2	20000	8	2020
1631	288	2020-08-14	2	20000	8	2020
1632	289	2020-08-14	2	20000	8	2020
1633	290	2020-08-14	2	20000	8	2020
1634	291	2020-08-14	2	20000	8	2020
1635	292	2020-08-14	2	20000	8	2020
1636	293	2020-08-14	2	20000	8	2020
1637	294	2020-08-14	2	20000	8	2020
1638	295	2020-08-14	2	20000	8	2020
1639	296	2020-08-14	2	20000	8	2020
1640	303	2020-08-14	2	20000	8	2020
1641	304	2020-08-14	2	20000	8	2020
1642	305	2020-08-14	2	20000	8	2020
1643	306	2020-08-14	2	20000	8	2020
1644	311	2020-08-14	2	20000	8	2020
1645	315	2020-08-14	2	20000	8	2020
1646	316	2020-08-14	2	20000	8	2020
1647	317	2020-08-14	2	20000	8	2020
1648	318	2020-08-14	2	20000	8	2020
1649	319	2020-08-14	2	20000	8	2020
1650	320	2020-08-14	2	20000	8	2020
1651	321	2020-08-14	2	20000	8	2020
1652	322	2020-08-14	2	20000	8	2020
1653	323	2020-08-14	2	20000	8	2020
1654	324	2020-08-14	2	20000	8	2020
1655	325	2020-08-14	2	20000	8	2020
1656	326	2020-08-14	2	20000	8	2020
1657	327	2020-08-14	2	20000	8	2020
1658	328	2020-08-14	2	20000	8	2020
1659	329	2020-08-14	2	20000	8	2020
1660	330	2020-08-14	2	20000	8	2020
1661	333	2020-08-14	2	20000	8	2020
1662	335	2020-08-14	2	20000	8	2020
1663	336	2020-08-14	2	20000	8	2020
1664	337	2020-08-14	2	20000	8	2020
1665	338	2020-08-14	2	20000	8	2020
1666	339	2020-08-14	2	20000	8	2020
1667	340	2020-08-14	2	20000	8	2020
1668	341	2020-08-14	2	20000	8	2020
1669	343	2020-08-14	2	20000	8	2020
1670	344	2020-08-14	2	20000	8	2020
1671	345	2020-08-14	2	20000	8	2020
1672	346	2020-08-14	2	20000	8	2020
1673	347	2020-08-14	2	20000	8	2020
1674	348	2020-08-14	2	20000	8	2020
1675	349	2020-08-14	2	20000	8	2020
1676	350	2020-08-14	2	20000	8	2020
1677	351	2020-08-14	2	20000	8	2020
1678	352	2020-08-14	2	20000	8	2020
1679	353	2020-08-14	2	20000	8	2020
1680	354	2020-08-14	2	20000	8	2020
1681	356	2020-08-14	2	20000	8	2020
1682	357	2020-08-14	2	20000	8	2020
1683	358	2020-08-14	2	20000	8	2020
1684	359	2020-08-14	2	20000	8	2020
1685	360	2020-08-14	2	20000	8	2020
1686	361	2020-08-14	2	20000	8	2020
1687	362	2020-08-14	2	20000	8	2020
1688	363	2020-08-14	2	20000	8	2020
1689	364	2020-08-14	2	20000	8	2020
1690	365	2020-08-14	2	20000	8	2020
1691	366	2020-08-14	2	20000	8	2020
1692	367	2020-08-14	2	20000	8	2020
1693	368	2020-08-14	2	20000	8	2020
1694	369	2020-08-14	2	20000	8	2020
1695	370	2020-08-14	2	20000	8	2020
1696	374	2020-08-14	2	20000	8	2020
1697	375	2020-08-14	2	20000	8	2020
1698	376	2020-08-14	2	20000	8	2020
1699	379	2020-08-14	2	20000	8	2020
1700	380	2020-08-14	2	20000	8	2020
1701	382	2020-08-14	2	20000	8	2020
1702	384	2020-08-14	2	20000	8	2020
1703	385	2020-08-14	2	20000	8	2020
1704	386	2020-08-14	2	20000	8	2020
1705	387	2020-08-14	2	20000	8	2020
1706	388	2020-08-14	2	20000	8	2020
1707	389	2020-08-14	2	20000	8	2020
1708	390	2020-08-14	2	20000	8	2020
1709	241	2020-08-14	2	20000	8	2020
1710	392	2020-08-14	2	20000	8	2020
1711	393	2020-08-14	2	20000	8	2020
1712	394	2020-08-14	2	20000	8	2020
1713	395	2020-08-14	2	20000	8	2020
1714	396	2020-08-14	2	20000	8	2020
1715	216	2020-08-14	2	20000	8	2020
1716	398	2020-08-14	2	20000	8	2020
1717	68	2020-08-14	2	20000	8	2020
2112	1	2020-09-11	2	20000	9	2020
2113	2	2020-09-11	2	20000	9	2020
2114	4	2020-09-11	2	20000	9	2020
2115	5	2020-09-11	2	20000	9	2020
2116	6	2020-09-11	2	20000	9	2020
2117	7	2020-09-11	2	20000	9	2020
2118	8	2020-09-11	2	20000	9	2020
2119	10	2020-09-11	2	20000	9	2020
2120	11	2020-09-11	2	20000	9	2020
2121	12	2020-09-11	2	20000	9	2020
2122	13	2020-09-11	2	20000	9	2020
2123	14	2020-09-11	2	20000	9	2020
2124	15	2020-09-11	2	20000	9	2020
2125	16	2020-09-11	2	20000	9	2020
2126	17	2020-09-11	2	20000	9	2020
2127	18	2020-09-11	2	20000	9	2020
2128	19	2020-09-11	2	20000	9	2020
2129	20	2020-09-11	2	20000	9	2020
2130	21	2020-09-11	2	20000	9	2020
2131	22	2020-09-11	2	20000	9	2020
2132	23	2020-09-11	2	20000	9	2020
2133	24	2020-09-11	2	20000	9	2020
2134	25	2020-09-11	2	20000	9	2020
2135	26	2020-09-11	2	20000	9	2020
2136	27	2020-09-11	2	20000	9	2020
2137	28	2020-09-11	2	20000	9	2020
2138	29	2020-09-11	2	20000	9	2020
2139	30	2020-09-11	2	20000	9	2020
2140	31	2020-09-11	2	20000	9	2020
2141	32	2020-09-11	2	20000	9	2020
2142	33	2020-09-11	2	20000	9	2020
2143	34	2020-09-11	2	20000	9	2020
2144	35	2020-09-11	2	20000	9	2020
2145	36	2020-09-11	2	20000	9	2020
2146	37	2020-09-11	2	20000	9	2020
2147	38	2020-09-11	2	20000	9	2020
2148	39	2020-09-11	2	20000	9	2020
2149	40	2020-09-11	2	20000	9	2020
2150	41	2020-09-11	2	20000	9	2020
2151	42	2020-09-11	2	20000	9	2020
2152	43	2020-09-11	2	20000	9	2020
2153	44	2020-09-11	2	20000	9	2020
2154	45	2020-09-11	2	20000	9	2020
2155	46	2020-09-11	2	20000	9	2020
2156	47	2020-09-11	2	20000	9	2020
2157	48	2020-09-11	2	20000	9	2020
2158	49	2020-09-11	2	20000	9	2020
2159	50	2020-09-11	2	20000	9	2020
2160	51	2020-09-11	2	20000	9	2020
2161	52	2020-09-11	2	20000	9	2020
2162	53	2020-09-11	2	20000	9	2020
2163	54	2020-09-11	2	20000	9	2020
2164	55	2020-09-11	2	20000	9	2020
2165	56	2020-09-11	2	20000	9	2020
2166	57	2020-09-11	2	20000	9	2020
2167	58	2020-09-11	2	20000	9	2020
2168	59	2020-09-11	2	20000	9	2020
2169	60	2020-09-11	2	20000	9	2020
2170	61	2020-09-11	2	20000	9	2020
2171	62	2020-09-11	2	20000	9	2020
2172	63	2020-09-11	2	20000	9	2020
2173	64	2020-09-11	2	20000	9	2020
2174	65	2020-09-11	2	20000	9	2020
2175	66	2020-09-11	2	20000	9	2020
2176	67	2020-09-11	2	20000	9	2020
2177	69	2020-09-11	2	20000	9	2020
2178	70	2020-09-11	2	20000	9	2020
2179	71	2020-09-11	2	20000	9	2020
2180	72	2020-09-11	2	20000	9	2020
2181	73	2020-09-11	2	20000	9	2020
2182	74	2020-09-11	2	20000	9	2020
2183	75	2020-09-11	2	20000	9	2020
2184	76	2020-09-11	2	20000	9	2020
2185	77	2020-09-11	2	20000	9	2020
2186	78	2020-09-11	2	20000	9	2020
2187	79	2020-09-11	2	20000	9	2020
2188	80	2020-09-11	2	20000	9	2020
2189	81	2020-09-11	2	20000	9	2020
2190	82	2020-09-11	2	20000	9	2020
2191	83	2020-09-11	2	20000	9	2020
2192	84	2020-09-11	2	20000	9	2020
2193	85	2020-09-11	2	20000	9	2020
2194	86	2020-09-11	2	20000	9	2020
2195	87	2020-09-11	2	20000	9	2020
2196	88	2020-09-11	2	20000	9	2020
2197	89	2020-09-11	2	20000	9	2020
2198	90	2020-09-11	2	20000	9	2020
2199	91	2020-09-11	2	20000	9	2020
2200	92	2020-09-11	2	20000	9	2020
2201	9	2020-09-11	2	20000	9	2020
2202	93	2020-09-11	2	20000	9	2020
2203	94	2020-09-11	2	20000	9	2020
2204	95	2020-09-11	2	20000	9	2020
2205	96	2020-09-11	2	20000	9	2020
2206	97	2020-09-11	2	20000	9	2020
2207	98	2020-09-11	2	20000	9	2020
2208	99	2020-09-11	2	20000	9	2020
2209	100	2020-09-11	2	20000	9	2020
2210	101	2020-09-11	2	20000	9	2020
2211	102	2020-09-11	2	20000	9	2020
2212	103	2020-09-11	2	20000	9	2020
2213	104	2020-09-11	2	20000	9	2020
2214	105	2020-09-11	2	20000	9	2020
2215	106	2020-09-11	2	20000	9	2020
2216	107	2020-09-11	2	20000	9	2020
2217	108	2020-09-11	2	20000	9	2020
2218	109	2020-09-11	2	20000	9	2020
2219	110	2020-09-11	2	20000	9	2020
2220	111	2020-09-11	2	20000	9	2020
2221	112	2020-09-11	2	20000	9	2020
2222	113	2020-09-11	2	20000	9	2020
2223	114	2020-09-11	2	20000	9	2020
2224	115	2020-09-11	2	20000	9	2020
2225	116	2020-09-11	2	20000	9	2020
2226	117	2020-09-11	2	20000	9	2020
2227	118	2020-09-11	2	20000	9	2020
2228	119	2020-09-11	2	20000	9	2020
2229	120	2020-09-11	2	20000	9	2020
2230	121	2020-09-11	2	20000	9	2020
2231	122	2020-09-11	2	20000	9	2020
2232	123	2020-09-11	2	20000	9	2020
2233	124	2020-09-11	2	20000	9	2020
2234	125	2020-09-11	2	20000	9	2020
2235	126	2020-09-11	2	20000	9	2020
2236	127	2020-09-11	2	20000	9	2020
2237	128	2020-09-11	2	20000	9	2020
2238	129	2020-09-11	2	20000	9	2020
2239	130	2020-09-11	2	20000	9	2020
2240	131	2020-09-11	2	20000	9	2020
2241	132	2020-09-11	2	20000	9	2020
2242	133	2020-09-11	2	20000	9	2020
2243	134	2020-09-11	2	20000	9	2020
2244	135	2020-09-11	2	20000	9	2020
2245	136	2020-09-11	2	20000	9	2020
2246	137	2020-09-11	2	20000	9	2020
2247	138	2020-09-11	2	20000	9	2020
2248	139	2020-09-11	2	20000	9	2020
2249	140	2020-09-11	2	20000	9	2020
2250	141	2020-09-11	2	20000	9	2020
2251	142	2020-09-11	2	20000	9	2020
2252	143	2020-09-11	2	20000	9	2020
2253	144	2020-09-11	2	20000	9	2020
2254	145	2020-09-11	2	20000	9	2020
2255	146	2020-09-11	2	20000	9	2020
2256	147	2020-09-11	2	20000	9	2020
2257	148	2020-09-11	2	20000	9	2020
2258	149	2020-09-11	2	20000	9	2020
2259	150	2020-09-11	2	20000	9	2020
2260	151	2020-09-11	2	20000	9	2020
2261	152	2020-09-11	2	20000	9	2020
2262	153	2020-09-11	2	20000	9	2020
2263	154	2020-09-11	2	20000	9	2020
2264	155	2020-09-11	2	20000	9	2020
2265	156	2020-09-11	2	20000	9	2020
2266	157	2020-09-11	2	20000	9	2020
2267	158	2020-09-11	2	20000	9	2020
2268	159	2020-09-11	2	20000	9	2020
2269	160	2020-09-11	2	20000	9	2020
2270	161	2020-09-11	2	20000	9	2020
2271	162	2020-09-11	2	20000	9	2020
2272	163	2020-09-11	2	20000	9	2020
2273	164	2020-09-11	2	20000	9	2020
2274	165	2020-09-11	2	20000	9	2020
2275	166	2020-09-11	2	20000	9	2020
2276	167	2020-09-11	2	20000	9	2020
2277	168	2020-09-11	2	20000	9	2020
2278	169	2020-09-11	2	20000	9	2020
2279	171	2020-09-11	2	20000	9	2020
2280	172	2020-09-11	2	20000	9	2020
2281	174	2020-09-11	2	20000	9	2020
2282	175	2020-09-11	2	20000	9	2020
2283	176	2020-09-11	2	20000	9	2020
2284	177	2020-09-11	2	20000	9	2020
2285	179	2020-09-11	2	20000	9	2020
2286	180	2020-09-11	2	20000	9	2020
2287	181	2020-09-11	2	20000	9	2020
2288	170	2020-09-11	2	20000	9	2020
2289	173	2020-09-11	2	20000	9	2020
2290	178	2020-09-11	2	20000	9	2020
2291	182	2020-09-11	2	20000	9	2020
2292	183	2020-09-11	2	20000	9	2020
2293	184	2020-09-11	2	20000	9	2020
2294	185	2020-09-11	2	20000	9	2020
2295	186	2020-09-11	2	20000	9	2020
2296	187	2020-09-11	2	20000	9	2020
2297	189	2020-09-11	2	20000	9	2020
2298	190	2020-09-11	2	20000	9	2020
2299	191	2020-09-11	2	20000	9	2020
2300	192	2020-09-11	2	20000	9	2020
2301	193	2020-09-11	2	20000	9	2020
2302	194	2020-09-11	2	20000	9	2020
2303	195	2020-09-11	2	20000	9	2020
2304	196	2020-09-11	2	20000	9	2020
2305	197	2020-09-11	2	20000	9	2020
2306	198	2020-09-11	2	20000	9	2020
2307	199	2020-09-11	2	20000	9	2020
2308	200	2020-09-11	2	20000	9	2020
2309	201	2020-09-11	2	20000	9	2020
2310	202	2020-09-11	2	20000	9	2020
2311	203	2020-09-11	2	20000	9	2020
2312	204	2020-09-11	2	20000	9	2020
2313	205	2020-09-11	2	20000	9	2020
2314	206	2020-09-11	2	20000	9	2020
2315	207	2020-09-11	2	20000	9	2020
2316	208	2020-09-11	2	20000	9	2020
2317	209	2020-09-11	2	20000	9	2020
2318	210	2020-09-11	2	20000	9	2020
2319	211	2020-09-11	2	20000	9	2020
2320	212	2020-09-11	2	20000	9	2020
2321	213	2020-09-11	2	20000	9	2020
2322	214	2020-09-11	2	20000	9	2020
2323	215	2020-09-11	2	20000	9	2020
2324	217	2020-09-11	2	20000	9	2020
2325	218	2020-09-11	2	20000	9	2020
2326	219	2020-09-11	2	20000	9	2020
2327	220	2020-09-11	2	20000	9	2020
2328	221	2020-09-11	2	20000	9	2020
2329	222	2020-09-11	2	20000	9	2020
2330	223	2020-09-11	2	20000	9	2020
2331	224	2020-09-11	2	20000	9	2020
2332	225	2020-09-11	2	20000	9	2020
2333	226	2020-09-11	2	20000	9	2020
2334	227	2020-09-11	2	20000	9	2020
2335	228	2020-09-11	2	20000	9	2020
2336	229	2020-09-11	2	20000	9	2020
2337	230	2020-09-11	2	20000	9	2020
2338	231	2020-09-11	2	20000	9	2020
2339	232	2020-09-11	2	20000	9	2020
2340	3	2020-09-11	2	20000	9	2020
2341	233	2020-09-11	2	20000	9	2020
2342	234	2020-09-11	2	20000	9	2020
2343	235	2020-09-11	2	20000	9	2020
2344	236	2020-09-11	2	20000	9	2020
2345	237	2020-09-11	2	20000	9	2020
2346	238	2020-09-11	2	20000	9	2020
2347	239	2020-09-11	2	20000	9	2020
2348	240	2020-09-11	2	20000	9	2020
2349	242	2020-09-11	2	20000	9	2020
2350	243	2020-09-11	2	20000	9	2020
2351	244	2020-09-11	2	20000	9	2020
2352	245	2020-09-11	2	20000	9	2020
2353	254	2020-09-11	2	20000	9	2020
2354	255	2020-09-11	2	20000	9	2020
2355	253	2020-09-11	2	20000	9	2020
2356	256	2020-09-11	2	20000	9	2020
2357	257	2020-09-11	2	20000	9	2020
2358	258	2020-09-11	2	20000	9	2020
2359	259	2020-09-11	2	20000	9	2020
2360	260	2020-09-11	2	20000	9	2020
2361	261	2020-09-11	2	20000	9	2020
2362	262	2020-09-11	2	20000	9	2020
2363	263	2020-09-11	2	20000	9	2020
2364	264	2020-09-11	2	20000	9	2020
2365	265	2020-09-11	2	20000	9	2020
2366	266	2020-09-11	2	20000	9	2020
2367	267	2020-09-11	2	20000	9	2020
2368	268	2020-09-11	2	20000	9	2020
2369	269	2020-09-11	2	20000	9	2020
2370	270	2020-09-11	2	20000	9	2020
2371	271	2020-09-11	2	20000	9	2020
2372	272	2020-09-11	2	20000	9	2020
2373	273	2020-09-11	2	20000	9	2020
2374	274	2020-09-11	2	20000	9	2020
2375	275	2020-09-11	2	20000	9	2020
2376	276	2020-09-11	2	20000	9	2020
2377	277	2020-09-11	2	20000	9	2020
2378	278	2020-09-11	2	20000	9	2020
2379	279	2020-09-11	2	20000	9	2020
2380	280	2020-09-11	2	20000	9	2020
2381	281	2020-09-11	2	20000	9	2020
2382	282	2020-09-11	2	20000	9	2020
2383	283	2020-09-11	2	20000	9	2020
2384	284	2020-09-11	2	20000	9	2020
2385	285	2020-09-11	2	20000	9	2020
2386	286	2020-09-11	2	20000	9	2020
2387	287	2020-09-11	2	20000	9	2020
2388	288	2020-09-11	2	20000	9	2020
2389	289	2020-09-11	2	20000	9	2020
2390	290	2020-09-11	2	20000	9	2020
2391	291	2020-09-11	2	20000	9	2020
2392	292	2020-09-11	2	20000	9	2020
2393	293	2020-09-11	2	20000	9	2020
2394	294	2020-09-11	2	20000	9	2020
2395	295	2020-09-11	2	20000	9	2020
2396	296	2020-09-11	2	20000	9	2020
2397	303	2020-09-11	2	20000	9	2020
2398	304	2020-09-11	2	20000	9	2020
2399	305	2020-09-11	2	20000	9	2020
2400	306	2020-09-11	2	20000	9	2020
2401	311	2020-09-11	2	20000	9	2020
2402	315	2020-09-11	2	20000	9	2020
2403	316	2020-09-11	2	20000	9	2020
2404	317	2020-09-11	2	20000	9	2020
2405	318	2020-09-11	2	20000	9	2020
2406	319	2020-09-11	2	20000	9	2020
2407	320	2020-09-11	2	20000	9	2020
2408	321	2020-09-11	2	20000	9	2020
2409	322	2020-09-11	2	20000	9	2020
2410	323	2020-09-11	2	20000	9	2020
2411	324	2020-09-11	2	20000	9	2020
2412	325	2020-09-11	2	20000	9	2020
2413	326	2020-09-11	2	20000	9	2020
2414	327	2020-09-11	2	20000	9	2020
2415	328	2020-09-11	2	20000	9	2020
2416	329	2020-09-11	2	20000	9	2020
2417	330	2020-09-11	2	20000	9	2020
2418	333	2020-09-11	2	20000	9	2020
2419	335	2020-09-11	2	20000	9	2020
2420	336	2020-09-11	2	20000	9	2020
2421	337	2020-09-11	2	20000	9	2020
2422	338	2020-09-11	2	20000	9	2020
2423	339	2020-09-11	2	20000	9	2020
2424	340	2020-09-11	2	20000	9	2020
2425	341	2020-09-11	2	20000	9	2020
2426	343	2020-09-11	2	20000	9	2020
2427	344	2020-09-11	2	20000	9	2020
2428	345	2020-09-11	2	20000	9	2020
2429	346	2020-09-11	2	20000	9	2020
2430	347	2020-09-11	2	20000	9	2020
2431	348	2020-09-11	2	20000	9	2020
2432	349	2020-09-11	2	20000	9	2020
2433	350	2020-09-11	2	20000	9	2020
2434	351	2020-09-11	2	20000	9	2020
2435	352	2020-09-11	2	20000	9	2020
2436	353	2020-09-11	2	20000	9	2020
2437	354	2020-09-11	2	20000	9	2020
2438	356	2020-09-11	2	20000	9	2020
2439	357	2020-09-11	2	20000	9	2020
2440	358	2020-09-11	2	20000	9	2020
2441	359	2020-09-11	2	20000	9	2020
2442	360	2020-09-11	2	20000	9	2020
2443	361	2020-09-11	2	20000	9	2020
2444	362	2020-09-11	2	20000	9	2020
2445	363	2020-09-11	2	20000	9	2020
2446	364	2020-09-11	2	20000	9	2020
2447	365	2020-09-11	2	20000	9	2020
2448	366	2020-09-11	2	20000	9	2020
2449	367	2020-09-11	2	20000	9	2020
2450	368	2020-09-11	2	20000	9	2020
2451	369	2020-09-11	2	20000	9	2020
2452	370	2020-09-11	2	20000	9	2020
2453	374	2020-09-11	2	20000	9	2020
2454	375	2020-09-11	2	20000	9	2020
2455	376	2020-09-11	2	20000	9	2020
2456	379	2020-09-11	2	20000	9	2020
2457	380	2020-09-11	2	20000	9	2020
2458	382	2020-09-11	2	20000	9	2020
2459	384	2020-09-11	2	20000	9	2020
2460	385	2020-09-11	2	20000	9	2020
2461	386	2020-09-11	2	20000	9	2020
2462	387	2020-09-11	2	20000	9	2020
2463	388	2020-09-11	2	20000	9	2020
2464	389	2020-09-11	2	20000	9	2020
2465	390	2020-09-11	2	20000	9	2020
2466	241	2020-09-11	2	20000	9	2020
2467	392	2020-09-11	2	20000	9	2020
2468	393	2020-09-11	2	20000	9	2020
2469	394	2020-09-11	2	20000	9	2020
2470	395	2020-09-11	2	20000	9	2020
2471	396	2020-09-11	2	20000	9	2020
2472	216	2020-09-11	2	20000	9	2020
2473	68	2020-09-11	2	20000	9	2020
2474	399	2020-09-11	2	20000	9	2020
2475	400	2020-09-11	2	20000	9	2020
2476	401	2020-09-11	2	20000	9	2020
2477	402	2020-09-11	2	20000	9	2020
2478	403	2020-09-11	2	20000	9	2020
2479	404	2020-09-11	2	20000	9	2020
2480	405	2020-09-11	2	20000	9	2020
2481	406	2020-09-11	2	20000	9	2020
2482	407	2020-09-11	2	20000	9	2020
2483	408	2020-09-11	2	20000	9	2020
2484	398	2020-09-11	2	20000	9	2020
2485	409	2020-09-11	2	20000	9	2020
2486	410	2020-09-11	2	20000	9	2020
2487	411	2020-09-11	2	20000	9	2020
2488	412	2020-09-11	2	20000	9	2020
2489	413	2020-09-11	2	20000	9	2020
2490	414	2020-09-11	2	20000	9	2020
2491	415	2020-09-11	2	20000	9	2020
2492	416	2020-09-11	2	20000	9	2020
2493	417	2020-09-11	2	20000	9	2020
2494	418	2020-09-11	2	20000	9	2020
2495	419	2020-09-11	2	20000	9	2020
2496	420	2020-09-11	2	20000	9	2020
2497	421	2020-09-11	2	20000	9	2020
2498	422	2020-09-11	2	20000	9	2020
2499	423	2020-09-11	2	20000	9	2020
2500	424	2020-09-11	2	20000	9	2020
2501	425	2020-09-11	2	20000	9	2020
2502	426	2020-09-11	2	20000	9	2020
2503	427	2020-09-11	2	20000	9	2020
2504	428	2020-09-11	2	20000	9	2020
2505	429	2020-09-11	2	20000	9	2020
\.


--
-- TOC entry 2448 (class 0 OID 0)
-- Dependencies: 202
-- Name: socios_descuentos_id_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.socios_descuentos_id_seq', 3738, true);


--
-- TOC entry 2393 (class 0 OID 28837)
-- Dependencies: 203
-- Data for Name: socios_descuentos_servicios; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.socios_descuentos_servicios (id, socio, fecha, servicio, monto_descuento, mes, agno, id_cab) FROM stdin;
\.


--
-- TOC entry 2449 (class 0 OID 0)
-- Dependencies: 204
-- Name: socios_descuentos_servicios_id_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.socios_descuentos_servicios_id_seq', 1, false);


--
-- TOC entry 2450 (class 0 OID 0)
-- Dependencies: 205
-- Name: socios_socio_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.socios_socio_seq', 452, true);


--
-- TOC entry 2396 (class 0 OID 28844)
-- Dependencies: 206
-- Data for Name: tipo_descuentos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.tipo_descuentos (tipo_descuento, descripcion) FROM stdin;
1	cuota fija
2	Pago seguro orden de compra
3	Monto fijo temporal
\.


--
-- TOC entry 2451 (class 0 OID 0)
-- Dependencies: 207
-- Name: tipo_descuentos_tipo_descuento_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.tipo_descuentos_tipo_descuento_seq', 3, true);


--
-- TOC entry 2398 (class 0 OID 28852)
-- Dependencies: 208
-- Data for Name: roles; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.roles (rol, nombre_rol) FROM stdin;
9	SysAdmin
11	operador
12	Rol de cierre
\.


--
-- TOC entry 2452 (class 0 OID 0)
-- Dependencies: 209
-- Name: roles_rol_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.roles_rol_seq', 12, true);


--
-- TOC entry 2400 (class 0 OID 28857)
-- Dependencies: 210
-- Data for Name: roles_x_selectores; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.roles_x_selectores (id, rol, selector) FROM stdin;
3	9	36
4	9	2
5	9	3
2	9	11
35	9	57
70	9	75
71	9	76
72	9	78
74	9	80
75	11	36
76	11	53
77	11	57
79	9	81
80	9	82
81	11	82
82	9	83
83	11	83
84	9	86
85	9	88
86	9	89
87	11	86
88	11	88
90	9	90
91	11	90
92	11	89
95	12	36
96	12	57
97	12	78
98	12	86
99	12	88
100	12	90
101	12	80
103	12	53
104	12	82
105	12	83
106	12	89
107	9	93
108	11	93
109	12	93
110	9	94
111	11	94
112	12	94
\.


--
-- TOC entry 2453 (class 0 OID 0)
-- Dependencies: 211
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.roles_x_selectores_id_seq', 112, true);


--
-- TOC entry 2402 (class 0 OID 28862)
-- Dependencies: 212
-- Data for Name: selectores; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.selectores (id, superior, descripcion, ord, link) FROM stdin;
3	1	Roles	2	/sistema/rol/
11	1	Acceso menu	3	/sistema/selector/
36	1	Salir	7	/
1	0	Sistema	10	
52	0	Administracion	11	
57	1	Cambio de password	4	/aplicacion/cambiopass/
74	0	Configuracion	12	
75	74	Tipo descuento	10	/configuracion/tipodescuento/
76	74	Conceptos servicios	20	/configuracion/conceptoservicio/
77	74	Procesar descuento	30	
81	52	Socio	11	/admin/socio/
53	52	Socio	10	/aplicacion/socio/
83	52	Ordenes de Compra	40	/aplicacion/ordencompra/
82	52	Casas Comerciales	20	/admin/casacomercial/
84	0	Consultas	40	
85	84	Casas Comerciales	10	
86	85	Ver ordenes de compra	10	/consulta/casacomercial/ordencompra/
87	84	Clientes	20	
88	87	Ver ordenes de compras	10	/consulta/socio/ordencompra/
89	52	Ordenes de Compra Pagos	41	/aplicacion/ordencomprapago/
91	0	Descuentos	50	
92	91	Orden de Compra (archivo.xls)	20	/descuento/ordencompraxls/
80	91	Archivo xls	10	/descuento/archivoxls/
78	77	Cierrre Mensual	10	/configuracion/cierremensual/
93	85	Cuotas por mes y año detallado 	20	/consulta/casacomercial/pagomesdet/
90	84	Extracto de cuenta	30	/consulta/socio/extractoactual/
94	87	Extractos anteriores	20	/consulta/socio/extracto/
2	1	Usuarios	1	/sistema/usuario/
\.


--
-- TOC entry 2454 (class 0 OID 0)
-- Dependencies: 213
-- Name: selectores_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.selectores_id_seq', 94, true);


--
-- TOC entry 2404 (class 0 OID 28870)
-- Dependencies: 214
-- Data for Name: selectores_x_webservice; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.selectores_x_webservice (id, selector, wservice) FROM stdin;
\.


--
-- TOC entry 2455 (class 0 OID 0)
-- Dependencies: 215
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.selectores_x_webservice_id_seq', 1, false);


--
-- TOC entry 2406 (class 0 OID 28875)
-- Dependencies: 216
-- Data for Name: usuarios; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.usuarios (usuario, cuenta, clave, token_iat) FROM stdin;
5	prueba	827ccb0eea8a706c4c34a16891f84e7b	1594920638062
2	bruno	5ebe2294ecd0e0f08eab7690d2a6ee69	1601316269937
4	natalia	c1ed60949799e3adcd72928bb3314fe0	1602265796667
3	edit	de95b43bceeb4b998aed4aed5cef1ae7	1602260826509
1	root	8fed0c52c93622fb5163bbc60b64800c	1602293919296
\.


--
-- TOC entry 2456 (class 0 OID 0)
-- Dependencies: 217
-- Name: usuarios_usuario_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.usuarios_usuario_seq', 5, true);


--
-- TOC entry 2408 (class 0 OID 28883)
-- Dependencies: 218
-- Data for Name: usuarios_x_roles; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.usuarios_x_roles (id, usuario, rol) FROM stdin;
197	1	9
202	2	11
203	3	11
205	5	12
206	4	12
\.


--
-- TOC entry 2457 (class 0 OID 0)
-- Dependencies: 219
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.usuarios_x_roles_id_seq', 206, true);


--
-- TOC entry 2410 (class 0 OID 28888)
-- Dependencies: 220
-- Data for Name: webservice; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.webservice (wservice, path, nombre) FROM stdin;
\.


--
-- TOC entry 2458 (class 0 OID 0)
-- Dependencies: 221
-- Name: webservice_wservice_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.webservice_wservice_seq', 1, false);


--
-- TOC entry 2209 (class 2606 OID 28916)
-- Name: casas_comerciales_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.casas_comerciales
    ADD CONSTRAINT casas_comerciales_pkey PRIMARY KEY (casacomercial);


--
-- TOC entry 2211 (class 2606 OID 29001)
-- Name: cierre_mensual_mes_agno_key; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.cierre_mensual
    ADD CONSTRAINT cierre_mensual_mes_agno_key UNIQUE (mes, agno);


--
-- TOC entry 2213 (class 2606 OID 28920)
-- Name: cierre_mes_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.cierre_mensual
    ADD CONSTRAINT cierre_mes_pkey PRIMARY KEY (id);


--
-- TOC entry 2215 (class 2606 OID 28922)
-- Name: concepto_servicios_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.concepto_servicios
    ADD CONSTRAINT concepto_servicios_pkey PRIMARY KEY (servicio);


--
-- TOC entry 2219 (class 2606 OID 28924)
-- Name: ordenes_compras_pagos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.ordenes_compras_pagos
    ADD CONSTRAINT ordenes_compras_pagos_pkey PRIMARY KEY (id);


--
-- TOC entry 2217 (class 2606 OID 28926)
-- Name: ordenes_compras_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.ordenes_compras
    ADD CONSTRAINT ordenes_compras_pkey PRIMARY KEY (ordencompra);


--
-- TOC entry 2221 (class 2606 OID 28928)
-- Name: relacion_laboral_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.relacion_laboral
    ADD CONSTRAINT relacion_laboral_pkey PRIMARY KEY (relacion_laboral);


--
-- TOC entry 2223 (class 2606 OID 28930)
-- Name: servicio_fijo_proceso_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.servicios_fijos_descuentos
    ADD CONSTRAINT servicio_fijo_proceso_pkey PRIMARY KEY (id);


--
-- TOC entry 2225 (class 2606 OID 28932)
-- Name: servicio_fijo_proceso_servcio_mes_agno_key; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.servicios_fijos_descuentos
    ADD CONSTRAINT servicio_fijo_proceso_servcio_mes_agno_key UNIQUE (servicio, mes, agno);


--
-- TOC entry 2227 (class 2606 OID 28934)
-- Name: servicios_temporal_descuentos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.servicios_temporal_descuentos
    ADD CONSTRAINT servicios_temporal_descuentos_pkey PRIMARY KEY (id);


--
-- TOC entry 2229 (class 2606 OID 28936)
-- Name: socio_pkey1; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.socios
    ADD CONSTRAINT socio_pkey1 PRIMARY KEY (socio);


--
-- TOC entry 2231 (class 2606 OID 28938)
-- Name: socios_cedula_key; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.socios
    ADD CONSTRAINT socios_cedula_key UNIQUE (cedula);


--
-- TOC entry 2233 (class 2606 OID 28940)
-- Name: socios_descuentos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.socios_descuentos
    ADD CONSTRAINT socios_descuentos_pkey PRIMARY KEY (id);


--
-- TOC entry 2235 (class 2606 OID 28942)
-- Name: tipo_descuentos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.tipo_descuentos
    ADD CONSTRAINT tipo_descuentos_pkey PRIMARY KEY (tipo_descuento);


--
-- TOC entry 2237 (class 2606 OID 28944)
-- Name: roles_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (rol);


--
-- TOC entry 2239 (class 2606 OID 28946)
-- Name: roles_x_selectores_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_pkey PRIMARY KEY (id);


--
-- TOC entry 2241 (class 2606 OID 28948)
-- Name: selectores_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores
    ADD CONSTRAINT selectores_pkey PRIMARY KEY (id);


--
-- TOC entry 2243 (class 2606 OID 28950)
-- Name: selectores_x_webservice_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores_x_webservice
    ADD CONSTRAINT selectores_x_webservice_pkey PRIMARY KEY (id);


--
-- TOC entry 2245 (class 2606 OID 28952)
-- Name: usuarios_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (usuario);


--
-- TOC entry 2247 (class 2606 OID 28954)
-- Name: usuarios_x_roles_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2249 (class 2606 OID 28956)
-- Name: webservice_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.webservice
    ADD CONSTRAINT webservice_pkey PRIMARY KEY (wservice);


--
-- TOC entry 2258 (class 2620 OID 28957)
-- Name: t_servicio_fijos_descuentos_after_insert; Type: TRIGGER; Schema: aplicacion; Owner: postgres
--

CREATE TRIGGER t_servicio_fijos_descuentos_after_insert AFTER INSERT ON aplicacion.servicios_fijos_descuentos FOR EACH ROW EXECUTE PROCEDURE aplicacion.insertar_socio_descuento_fijo();


--
-- TOC entry 2250 (class 2606 OID 28958)
-- Name: concepto_servicios_tipo_descuento_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.concepto_servicios
    ADD CONSTRAINT concepto_servicios_tipo_descuento_fkey FOREIGN KEY (tipo_descuento) REFERENCES aplicacion.tipo_descuentos(tipo_descuento);


--
-- TOC entry 2251 (class 2606 OID 28963)
-- Name: ordenes_compras_casacomercial_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.ordenes_compras
    ADD CONSTRAINT ordenes_compras_casacomercial_fkey FOREIGN KEY (casacomercial) REFERENCES aplicacion.casas_comerciales(casacomercial);


--
-- TOC entry 2253 (class 2606 OID 28968)
-- Name: ordenes_compras_pagos_ordencompra_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.ordenes_compras_pagos
    ADD CONSTRAINT ordenes_compras_pagos_ordencompra_fkey FOREIGN KEY (ordencompra) REFERENCES aplicacion.ordenes_compras(ordencompra);


--
-- TOC entry 2252 (class 2606 OID 28973)
-- Name: ordenes_compras_socio_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.ordenes_compras
    ADD CONSTRAINT ordenes_compras_socio_fkey FOREIGN KEY (socio) REFERENCES aplicacion.socios(socio);


--
-- TOC entry 2254 (class 2606 OID 28978)
-- Name: roles_x_selectores_rol_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_rol_fkey FOREIGN KEY (rol) REFERENCES sistema.roles(rol);


--
-- TOC entry 2255 (class 2606 OID 28983)
-- Name: roles_x_selectores_selector_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_selector_fkey FOREIGN KEY (selector) REFERENCES sistema.selectores(id);


--
-- TOC entry 2256 (class 2606 OID 28988)
-- Name: usuarios_x_roles_rol_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_rol_fkey FOREIGN KEY (rol) REFERENCES sistema.roles(rol);


--
-- TOC entry 2257 (class 2606 OID 28993)
-- Name: usuarios_x_roles_usuario_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_usuario_fkey FOREIGN KEY (usuario) REFERENCES sistema.usuarios(usuario);


--
-- TOC entry 2419 (class 0 OID 0)
-- Dependencies: 9
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2020-10-09 22:42:35 -03

--
-- PostgreSQL database dump complete
--

