 
SELECT ordenes_compras.ordencompra, casacomercial, socio, credito, cantidad_cuotas, monto_cuota,  
cuotas_pagadas, credito_saldo, fecha   
FROM aplicacion.ordenes_compras  
left join  
( 
SELECT id, ordencompra, mes, agno 
FROM aplicacion.ordenes_compras_pagos 
where ( mes = v0 and agno = v1 ) 
) as tpagos on (ordenes_compras.ordencompra = tpagos.ordencompra ) 
where cantidad_cuotas <> cuotas_pagadas 
and credito_saldo >= monto_cuota    
and mes is null 

