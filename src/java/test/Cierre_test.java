/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import jakarta.servlet.ServletContext;
import nebuleuse.ORM.Persistencia;
import nebuleuse.util.Datetime;
import nebuleuse.util.FileXls;
import py.com.base.aplicacion.cierremensual.CierreMensual;
import py.com.base.aplicacion.cierremensual.CierreMensualDAO;
import py.com.base.aplicacion.nodescontado.NoDescontadoDAO;
import py.com.base.aplicacion.nodescontado.NoDescontadoJSON;
import py.com.base.aplicacion.ordencompra.OrdenCompra;
import py.com.base.aplicacion.ordencompra.OrdenCompraDAO;
import py.com.base.aplicacion.ordencompra.OrdenCompraRS;
import py.com.base.aplicacion.relacionlaboral.RelacionLaboral;
import py.com.base.aplicacion.relacionlaboral.RelacionLaboralDAO;
import py.com.base.aplicacion.serviciofijodescuento.ServicioFijoDescuento;
import py.com.base.aplicacion.serviciofijodescuento.ServicioFijoDescuentoDAO;
import py.com.base.aplicacion.serviciotemporaldescuento.ServicioTemporalDescuentoDAO;
import py.com.base.aplicacion.socio.SocioJSON;
import py.com.base.aplicacion.sociodescuento.SocioDescuentoRS;
import py.com.base.aplicacion.sociodescuentoordencompra.SocioDescuentoOrdenCompraRS;
import py.com.base.configuracion.conceptoservicio.ConceptoServicio;
import py.com.base.configuracion.conceptoservicio.ConceptoServicioDAO;
import py.com.base.sistema.usuario.UsuarioDAO;


/**
 *
 * @author hugo
 */
public class Cierre_test {


    
    public static void main(String args[]) throws Exception {
        
        
        CierreMensual com = new CierreMensual();   
        Persistencia persistencia = new Persistencia();   
        Gson gson = new Gson();          
        
String json = "{\"agno\":2022, \"sel_mes\" :{  \"sel_mes\": \"3\"} }";       
                
                // tabla  cierre mensual

                Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
                CierreMensual req = gsonf.fromJson(json, CierreMensual.class);                   

        
                JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);
                req.setMes(  jsonObject.get("sel_mes").getAsJsonObject().get ("sel_mes").getAsInt()   );                
                
                req.setFecha(  new Date() );
                                
                com = (CierreMensual) persistencia.insert( req );    
                
                
                // tabla servicios fijos
                // filtrar servicio  --  cuota socual   -- cod 2
                
                Integer cuotasocial = 2;
                
                ServicioFijoDescuento serviciofijodescuento = new ServicioFijoDescuento();
                
                ConceptoServicio serv = new ConceptoServicio();
                serv = (ConceptoServicio) persistencia.filtrarId( serv, cuotasocial );                  
                
                
                serviciofijodescuento.setMonto(  serv.getMonto_fijo() );                               
                serviciofijodescuento.setServicio(serv);
                serviciofijodescuento.setAgno(com.getAgno());
                serviciofijodescuento.setMes(com.getMes());
                serviciofijodescuento.setFecha(  new Date() );
                
                persistencia.insert(serviciofijodescuento);    
                
                
                
                
                // aca guardar los conceptos temporales
            
                ServicioTemporalDescuentoDAO temporal = new ServicioTemporalDescuentoDAO();
            
                temporal.procedimiento_cierre_servicio(com);
            
            
                
                
                
                // guardar  cuotas de ordenes de compras
                
                OrdenCompraDAO ocDAO = new OrdenCompraDAO();              
                ocDAO.cierreMensual(com.getMes(),  com.getAgno(), com.getId() );
                
                json = gson.toJson(com);    
        
        
                System.out.println(json);
        
        
    }

        
        

}


/*
{"gasto":0,
"fecha":"2019-11-21",
"descripcion":"GASDGDASGA",
"factura_numero":"4545",
"importe":5000, 
"cat_gasto" :{  "cat_gasto": "1"} }



{"ingreso":0,
"fecha":"2019-11-20",
"descripcion":"fsafasfas",
"importe":10000, 
"cat_ingreso" :{  "cat_ingreso": "2"} }

*/







