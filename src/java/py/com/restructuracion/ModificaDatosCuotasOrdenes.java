/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.restructuracion;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.sql.BasicSQL;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;
import py.com.base.aplicacion.ordencompra.OrdenCompraDAO;
import py.com.base.aplicacion.ordencomprapago.OrdenCompraPago;
import py.com.base.aplicacion.ordencomprapago.OrdenCompraPagoDAO;
import py.com.base.aplicacion.socio.Socio;

/**
 *
 * @author hugo
 */
public class ModificaDatosCuotasOrdenes {
    
    
    
    public ResultSet  seleccionOrdenes (  ) throws Exception {
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;      
        
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();

            String sql = "";                                 
            ReaderT readerSQL = new ReaderT("ModificaDatosCuotasOrdenes");
            readerSQL.fileExt = "seleccionOrdenes.sql";
    
            sql = readerSQL.get(   );   

        resultset = statement.executeQuery(sql);     
        conexion.desconectar();                
        return resultset;                 

    }
    
        
    
    
    public void borrarcuotas (Integer ordencompra) throws SQLException, IOException{


        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;      
        
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();

            String sql = "";                                 
            ReaderT readerSQL = new ReaderT("ModificaDatosCuotasOrdenes");
            readerSQL.fileExt = "borrarcuotas.sql";
            sql = readerSQL.get(  ordencompra );   

System.out.println( sql );    
            
        statement.execute(sql);
        
        conexion.desconectar();                
        
    }
            
            
    
    public void updateCero (Integer ordencompra) throws SQLException, IOException{


        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;      
        
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();

            String sql = "";                                 
            ReaderT readerSQL = new ReaderT("ModificaDatosCuotasOrdenes");
            readerSQL.fileExt = "updateCero.sql";
            sql = readerSQL.get(  ordencompra );   

    System.out.println( sql );
            
        statement.execute(sql);     
        conexion.desconectar();                
        
    }
            

    
    

public OrdenCompraPago cargarCuota  (
        Integer ordencompra, Long montocuota,
        Date fecha, Integer agno, Integer mes)         
        throws IOException, Exception {

        Persistencia persistencia = new Persistencia();    
        OrdenCompraPago pago = new OrdenCompraPago();                   
                
        
        pago.setOrdencompra(ordencompra);
                
        pago.setNumero_cuota(
                new OrdenCompraPagoDAO().maxNumeroCuota(
                        ordencompra ));                

        
        Long monto_saldo = new OrdenCompraDAO().restarSaldo(
                ordencompra,montocuota );  
        

        pago.setCredito_saldo( monto_saldo  );                    
        pago.setFecha(fecha);
        pago.setMonto_cuota(montocuota);
        pago.setMes(mes);
        pago.setAgno(agno);
        
        pago = (OrdenCompraPago) persistencia.insert(pago);                

        new OrdenCompraDAO().actualizarSaldo(
                ordencompra, monto_saldo, pago.getNumero_cuota()
        );                    

        
        
    return pago;
    
}  


    
    public void cargarTodasLasCuotas  (
        Integer ordencompra, Long montocuota
        ) throws SQLException, IOException, Exception{


        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;      
        
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();

            String sql = "";                                 
            ReaderT readerSQL = new ReaderT("ModificaDatosCuotasOrdenes");
            readerSQL.fileExt = "cierresMeses.sql";
            sql = readerSQL.get(  ordencompra );   

    System.out.println( sql );
            
        resultset = statement.executeQuery(sql);     
        
        while (resultset.next()){            
        
            System.out.println( resultset.getString("fecha") );
            
            
            this.cargarCuota(ordencompra, montocuota, 
                    resultset.getDate("fecha") , 
                    Integer.parseInt( resultset.getString("agno") )  , 
                    Integer.parseInt( resultset.getString("mes") ) 
            );
            
        }
                    
        
        
        
        conexion.desconectar();                
        
    }
            




    
}
