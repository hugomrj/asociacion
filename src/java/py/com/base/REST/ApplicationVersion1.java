/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.REST;

import java.util.Set;
import jakarta.ws.rs.core.Application;

/**
 *
 * @author hugo
 */


@jakarta.ws.rs.ApplicationPath("api")
public class ApplicationVersion1 extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {

        resources.add(py.com.base.aplicacion.casacomercial.CasaComercialWS.class);
        resources.add(py.com.base.aplicacion.casacomercial.CasaComercialWS_QRY.class);
        resources.add(py.com.base.aplicacion.cierremensual.CierreMensualWS.class);
        resources.add(py.com.base.aplicacion.direccionsueldo.DireccionSueldoWS.class);
        resources.add(py.com.base.aplicacion.nodescontado.NoDescontadoWS.class);
        resources.add(py.com.base.aplicacion.ordencompra.OrdenCompraWS.class);
        resources.add(py.com.base.aplicacion.ordencompra.OrdenCompraWS_consulta.class);
        resources.add(py.com.base.aplicacion.ordencomprapago.OrdenCompraPagoWS.class);
        resources.add(py.com.base.aplicacion.relacionlaboral.RelacionLaboralWS.class);
        resources.add(py.com.base.aplicacion.serviciofijodescuento.ServicioFijoDescuentoWS.class);
        resources.add(py.com.base.aplicacion.socio.SocioWS.class);
        resources.add(py.com.base.configuracion.conceptoservicio.ConceptoServicioWS.class);
        resources.add(py.com.base.configuracion.tipodescuento.TipoDescuentoWS.class);
        resources.add(py.com.base.sistema.rol.RolWS.class);
        resources.add(py.com.base.sistema.rol_selector.RolSelectorWS.class);
        resources.add(py.com.base.sistema.selector.SelectorWS.class);
        resources.add(py.com.base.sistema.usuario.UsuarioWS.class);
        resources.add(py.com.base.sistema.usuario_rol.UsuarioRolWS.class);
               
            

    }
    
}
