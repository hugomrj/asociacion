/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.direccionsueldo;



/**
 *
 * @author hugom_000
 */
public class DireccionSueldo {

    
    private Integer direccion_sueldo;
    private String descripcion;    

    public Integer getDireccion_sueldo() {
        return direccion_sueldo;
    }

    public void setDireccion_sueldo(Integer direccion_sueldo) {
        this.direccion_sueldo = direccion_sueldo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }



    
    
}
