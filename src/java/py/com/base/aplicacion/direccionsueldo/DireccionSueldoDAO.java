/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.direccionsueldo;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class DireccionSueldoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public DireccionSueldoDAO ( ) throws IOException  {
    }
      

    public List<DireccionSueldo>  all () {
                
        List<DireccionSueldo>  lista = null;        
        try {                        
                        
            DireccionSueldoRS rs = new DireccionSueldoRS();            
            lista = new Coleccion<DireccionSueldo>().resultsetToList(
                    new DireccionSueldo(),
                    rs.all()
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
        
    
    
}
