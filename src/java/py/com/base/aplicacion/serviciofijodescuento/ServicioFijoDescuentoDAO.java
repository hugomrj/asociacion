/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.serviciofijodescuento;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class ServicioFijoDescuentoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public ServicioFijoDescuentoDAO ( ) throws IOException  {
    }
      
    
    public List<ServicioFijoDescuento>  list (Integer page) {
                
        List<ServicioFijoDescuento>  lista = null;        
        try {                        
                        
            ServicioFijoDescuentoRS rs = new ServicioFijoDescuentoRS();            
            
            
            lista = new Coleccion<ServicioFijoDescuento>().resultsetToList(
                    new ServicioFijoDescuento(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<ServicioFijoDescuento>  search (Integer page, String busqueda) {
                
        List<ServicioFijoDescuento>  lista = null;
        
        try {                       
                        
            ServicioFijoDescuentoRS rs = new ServicioFijoDescuentoRS();
            lista = new Coleccion<ServicioFijoDescuento>().resultsetToList(
                    new ServicioFijoDescuento(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    

    
}
