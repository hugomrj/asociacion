/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.sociodescuentoordencompra;

import java.util.Date;
import py.com.base.aplicacion.socio.Socio;

/**
 *
 * @author hugo
 */
public class SocioDescuentoOrdenCompra {
    
    private Integer id;
    private Socio socio;
    private Date fecha;
    private Long monto_descuento;
    private Integer mes;
    private Integer agno;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getMonto_descuento() {
        return monto_descuento;
    }

    public void setMonto_descuento(Long monto_descuento) {
        this.monto_descuento = monto_descuento;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Integer getAgno() {
        return agno;
    }

    public void setAgno(Integer agno) {
        this.agno = agno;
    }
    
    
    
}

