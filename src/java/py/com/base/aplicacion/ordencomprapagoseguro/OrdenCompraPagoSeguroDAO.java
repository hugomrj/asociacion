/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordencomprapagoseguro;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;
import py.com.base.aplicacion.ordencompra.OrdenCompraDAO;
import py.com.base.aplicacion.ordencomprapago.OrdenCompraPago;
import py.com.base.configuracion.conceptoservicio.ConceptoServicio;

/**
 * @author hugom_000
 */
public class OrdenCompraPagoSeguroDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    private Long monto_seguro = 0L;
    
    
    public OrdenCompraPagoSeguroDAO ( ) throws IOException  {
    }
      
   

    

    public OrdenCompraPagoSeguro agregarSeguro  (OrdenCompraPago pago, Integer socio)         
            throws IOException, Exception {

        OrdenCompraPagoSeguro objeto = new OrdenCompraPagoSeguro();                   

        //this.setMonto_seguro();
        
        
        objeto.setId(0);
        
        objeto.setSocio(socio);
        
        objeto.setFecha(pago.getFecha());
        objeto.setMonto_seguro( this.getMonto_seguro()  );
        objeto.setMes( pago.getMes());
        objeto.setAgno(pago.getAgno());

    
        // crear un control de si existe el seguro para ese socio
        // en ese mes y ese año, si exite no cargar
        
        if (!(this.isSeguro(socio, pago.getAgno(), pago.getMes()))){
            objeto = (OrdenCompraPagoSeguro) persistencia.insert(objeto);
        }
        

        return objeto;

    }  




    public Long getMonto_seguro() {
        return monto_seguro;
    }

    
    public void setMonto_seguro() throws Exception {

        ConceptoServicio concepto = new  ConceptoServicio();
        concepto = (ConceptoServicio) persistencia.filtrarId(concepto, 3);
        
        this.monto_seguro = concepto.getMonto_fijo();

    }


    public Boolean isSeguro (Integer socio, Integer agno, Integer mes)
            throws Exception {      

          OrdenCompraPagoSeguro objeto = new OrdenCompraPagoSeguro();  

          String sql = 
                " SELECT id, fecha, monto_seguro, mes, agno, socio\n" +
                " FROM aplicacion.ordenes_compras_pagos_seguro\n" +
                " WHERE socio = " + socio + 
                " and mes = " + mes +
                " and agno  = " + agno  ;  
                
          objeto = (OrdenCompraPagoSeguro) persistencia.sqlToObject(sql, objeto);

          if (objeto != null){
              return true;
          }
          else{
              return false;
          }
          

      }    
    
    


      
}
