/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordencomprapagoseguro;

import java.util.Date;

/**
 * @author hugom_000
 */

public class OrdenCompraPagoSeguro {

    public Integer getSocio() {
        return socio;
    }

    public void setSocio(Integer socio) {
        this.socio = socio;
    }
    
    private Integer id;
    private Integer socio;
    private Date fecha;
    private Long monto_seguro;
    private Integer mes;
    private Integer agno;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Integer getAgno() {
        return agno;
    }

    public void setAgno(Integer agno) {
        this.agno = agno;
    }

    public Long getMonto_seguro() {
        return monto_seguro;
    }

    public void setMonto_seguro(Long monto_seguro) {
        this.monto_seguro = monto_seguro;
    }
    
    
    
}


