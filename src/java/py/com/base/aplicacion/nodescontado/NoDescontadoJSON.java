/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.nodescontado;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Map;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.RegistroMap;


public class NoDescontadoJSON  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public NoDescontadoJSON ( ) throws IOException  {
    
    }
      
    
    
    
    

    public JsonArray  consultaweb (Integer mes, Integer agno, Integer socio ) {
        
        Map<String, String> map = null;        
        JsonArray jsonarray = new JsonArray();      
        RegistroMap registoMap = new RegistroMap();     
        Gson gson = new Gson();               
        
        
        try 
        {   
            
            NoDescontadoRS rs = new NoDescontadoRS();            
            ResultSet resulset = rs.consultaweb(  mes, agno, socio );                
            
            while(resulset.next()) 
            {  
                map = registoMap.convertirHashMap(resulset);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonarray.add( element );
            }                    
            this.total_registros = rs.total_registros  ;   
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonarray ;         
        }
    }      
          
    

    public JsonElement  totales (Integer mes, Integer agno ) {
        
        Map<String, String> map = null;        
        //JsonArray jsonarray = new JsonArray();      
        JsonElement jsonElement = null  ;
        RegistroMap registoMap = new RegistroMap();     
        Gson gson = new Gson();               
        
        
        try 
        {   
            
            NoDescontadoRS rs = new NoDescontadoRS();            
            ResultSet resulset = rs.totales(  mes, agno );                
            
            if(resulset.next()) 
            {  
                map = registoMap.convertirHashMap(resulset);     
                jsonElement = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
          //      jsonarray.add( element );
            }                    
            //this.total_registros = rs.total_registros  ;   
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonElement ;         
        }
    }      
          
        
    
    
    
    
    
        
}
