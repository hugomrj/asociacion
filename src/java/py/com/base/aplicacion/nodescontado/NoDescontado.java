/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.nodescontado;

import java.util.Date;
import py.com.base.aplicacion.socio.Socio;

/**
 *
 * @author hugo
 */
public class NoDescontado {
    
        private Integer id;
        private Socio socio;
        private Long monto;
        private Long interes;
        private Integer agno;
        private Integer mes;
        
        private Integer aplica_mes;
        private Integer aplica_agno;
        
        private Long suma;
        private Long pagado;
        private Long saldo;
        
        

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    public Long getMonto() {
        return monto;
    }

    public void setMonto(Long monto) {
        this.monto = monto;
    }

    public Long getInteres() {
        return interes;
    }

    public void setInteres(Long interes) {
        this.interes = interes;
    }

    public Integer getAgno() {
        return agno;
    }

    public void setAgno(Integer agno) {
        this.agno = agno;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Integer getAplica_mes() {
        return aplica_mes;
    }

    public void setAplica_mes(Integer aplica_mes) {
        this.aplica_mes = aplica_mes;
    }

    public Integer getAplica_agno() {
        return aplica_agno;
    }

    public void setAplica_agno(Integer aplica_agno) {
        this.aplica_agno = aplica_agno;
    }

    public Long getSuma() {
        return suma;
    }

    public void setSuma(Long suma) {
        this.suma = suma;
    }

    public Long getPagado() {
        return pagado;
    }

    public void setPagado(Long pagado) {
        this.pagado = pagado;
    }

    public Long getSaldo() {
        return saldo;
    }

    public void setSaldo(Long saldo) {
        this.saldo = saldo;
    }

}
