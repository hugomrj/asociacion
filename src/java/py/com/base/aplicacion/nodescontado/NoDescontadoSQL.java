/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.nodescontado;

import java.io.IOException;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */

public class NoDescontadoSQL {
    
        
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new NoDescontado(), busqueda);        
        
        return sql ;             
    }        
    
    
        
    
    
    public String  list_mes_agno (Integer mes, Integer agno ) 
            throws IOException {

        String sql = "";
        
        ReaderT readerSQL = new ReaderT("NoDescontado");
        readerSQL.fileExt = "list_mes_agno.sql";

        sql = readerSQL.get(  mes, agno );                      
    
        return sql ;                     
    }

    
    
    
    public String  list_mes_agno_xls (Integer mes, Integer agno ) 
            throws IOException {

        String sql = "";
        
        ReaderT readerSQL = new ReaderT("NoDescontado");
        readerSQL.fileExt = "list_mes_agno_xls.sql";

        sql = readerSQL.get(  mes, agno );         

    
        return sql ;                     
    }
    
    
        
    public String  list_mes_agno_search (Integer mes, Integer agno, String buscar ) 
            throws IOException {

        String sql = "";
        
        ReaderT readerSQL = new ReaderT("NoDescontado");
        readerSQL.fileExt = "list_mes_agno_search.sql";

        sql = readerSQL.get(  mes, agno, buscar );    
        
        return sql ;                     
    }
    
    
    
    public String  consultaweb (Integer mes, Integer agno, Integer socio ) 
            throws IOException {

        String sql = "";
        
        ReaderT readerSQL = new ReaderT("NoDescontado");
        readerSQL.fileExt = "consultaweb.sql";

        sql = readerSQL.get(  mes, agno, socio );                      
    
        return sql ;                     
    }

    
    
    public String  totales (Integer mes, Integer agno) 
            throws IOException {

        String sql = "";
        
        ReaderT readerSQL = new ReaderT("NoDescontado");
        readerSQL.fileExt = "totales.sql";

        sql = readerSQL.get(  mes, agno );                      
    
        return sql ;                     
    }

    
    
    
    
    
}
