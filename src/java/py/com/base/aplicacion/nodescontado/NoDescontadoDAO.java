/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.nodescontado;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class NoDescontadoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public NoDescontadoDAO ( ) throws IOException  {
    }
      
    
    public List<NoDescontado>  list (Integer page) {
                
        List<NoDescontado>  lista = null;        
        try {                        
                        
            NoDescontadoRS rs = new NoDescontadoRS();            
            
            
            lista = new Coleccion<NoDescontado>().resultsetToList(
                    new NoDescontado(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      
    
    
    
    public List<NoDescontado>  list_mes_agno ( Integer mes, Integer agno,  Integer page ) {
                
        List<NoDescontado>  lista = null;        
        try {                        
                        
            NoDescontadoRS rs = new NoDescontadoRS();            
            
            
            lista = new Coleccion<NoDescontado>().resultsetToList(
                    new NoDescontado(),
                    rs.list_mes_agno(mes, agno, page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      
    
    public List<NoDescontado>  list_mes_agno_search ( Integer mes, Integer agno,  Integer page,
            String buscar ) {
                
        List<NoDescontado>  lista = null;        
        try {                        
                        
            NoDescontadoRS rs = new NoDescontadoRS();            
            
            
            lista = new Coleccion<NoDescontado>().resultsetToList(
                    new NoDescontado(),
                    rs.list_mes_agno_search(mes, agno, page, buscar)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<NoDescontado>  search (Integer page, String busqueda) {
                
        List<NoDescontado>  lista = null;
        
        try {                       
                        
            NoDescontadoRS rs = new NoDescontadoRS();
            lista = new Coleccion<NoDescontado>().resultsetToList(
                    new NoDescontado(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
    

    public NoDescontado  descuentoAgnoMes (NoDescontado objeto) {
                
        Integer mes = objeto.getMes();
        Integer agno = objeto.getAgno();


        if (mes == 12 ) {
            
            objeto.setAplica_mes( 1 );
            objeto.setAplica_agno( objeto.getAgno() + 1 );
        
        }
        else{
        
            objeto.setAplica_mes(objeto.getMes() + 1  );
            objeto.setAplica_agno( objeto.getAgno());
        
        }

        


        
        return objeto ;          
    }          
    
    
    

    public List<NoDescontado>  consultaweb ( Integer mes, Integer agno,  Integer socio ) {
                
        List<NoDescontado>  lista = null;        
        try {                        
                        
            NoDescontadoRS rs = new NoDescontadoRS();            
            
            
            lista = new Coleccion<NoDescontado>().resultsetToList(
                    new NoDescontado(),
                    rs.consultaweb(mes, agno, socio)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
        
    
    
    
     
}
