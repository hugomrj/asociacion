/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.nodescontado;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.sql.BasicSQL;
import nebuleuse.ORM.sql.SentenciaSQL;
import nebuleuse.ORM.xml.Global;

/**
 *
 * @author hugom_000
 */
public class NoDescontadoRS {
    
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;          
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
        public Integer total_registros = 0;
        
    
    public NoDescontadoRS ( ) throws IOException, SQLException  {
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();              
    }
   
    
    
    public ResultSet  list ( Integer page ) throws Exception {

            statement = conexion.getConexion().createStatement();      
            
            String sql = SentenciaSQL.select(new NoDescontado());       
            sql = sql + "  order by ordencompra desc " ; 
            
            

            this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            sql = sql + BasicSQL.limite_offset(page, lineas);

            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
    
    
    public ResultSet  list_mes_agno (  Integer mes, Integer agno,  Integer page ) 
            throws Exception {

            statement = conexion.getConexion().createStatement();      
            String sql = new NoDescontadoSQL().list_mes_agno(mes, agno);
    
            this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            sql = sql + BasicSQL.limite_offset(page, lineas);

            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
    
    
    
    
    public ResultSet  list_mes_agno_xls (  Integer mes, Integer agno  ) 
            throws Exception {

            statement = conexion.getConexion().createStatement();      
            
            String sql = new NoDescontadoSQL().list_mes_agno_xls(mes, agno);
            
            this.total_registros =  BasicSQL.cont_registros(conexion, sql);
            
            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
    




    
    
    public ResultSet  list_mes_agno_search (  Integer mes, Integer agno,  Integer page,
            String buscar) 
            throws Exception {

            statement = conexion.getConexion().createStatement();      
                        
            String sql = new NoDescontadoSQL().list_mes_agno_search (mes, agno, buscar);
            
            this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            sql = sql + BasicSQL.limite_offset(page, lineas);

            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
    
    
   
    public ResultSet  search ( Integer page, String busqueda ) throws Exception {

            statement = conexion.getConexion().createStatement();                  
            String sql = new NoDescontadoSQL().search(busqueda);
            
            this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            sql = sql + BasicSQL.limite_offset(page, lineas);
            resultset = statement.executeQuery(sql);     
            
            conexion.desconectar();                
            return resultset;                 
            
    }
   
   
    public ResultSet  consultaweb (  Integer mes, Integer agno,  Integer socio ) 
            throws Exception {

            statement = conexion.getConexion().createStatement();      
            String sql = new NoDescontadoSQL().consultaweb(mes, agno, socio);
    
            
            this.total_registros =  BasicSQL.cont_registros(conexion, sql);        
            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
    
    

    public ResultSet  totales (  Integer mes, Integer agno ) 
            throws Exception {

            statement = conexion.getConexion().createStatement();      
            String sql = new NoDescontadoSQL().totales(mes, agno);
    
            
            this.total_registros =  BasicSQL.cont_registros(conexion, sql);        
            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
    
    
    
        
    
}
