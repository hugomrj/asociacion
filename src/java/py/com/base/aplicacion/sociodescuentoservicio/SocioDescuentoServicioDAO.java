/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.sociodescuentoservicio;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class SocioDescuentoServicioDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public SocioDescuentoServicioDAO ( ) throws IOException  {
    }
      

    public void insertarLote (Integer id_cab) throws Exception{
    
        String sql =  new SocioDescuentoServicioSQL().insertarLote(id_cab);
                
        persistencia.ejecutarSQL(sql);        
        
    } 
    
}
