/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.aplicacion.casacomercial;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;


/**
 * REST Web Service
 * @author hugo
 */





@Path("/consulta/casacomercial")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class CasaComercialWS_QRY {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new Gson();          
    private Response.Status status  = Response.Status.OK;
    
    CasaComercial com = new CasaComercial();       
                         
    public CasaComercialWS_QRY() {
    }

    
    

    
    
    @GET        
    @Path("/pagomes/{casacomercial}/{agno}/{mes}"  )    
    public Response pagomes (            
            @PathParam ("casacomercial") Integer casacomercial,
            @PathParam ("agno") Integer agno,
            @PathParam ("mes") Integer mes,
            @QueryParam("page") Integer page,
            @HeaderParam("token") String strToken
            ) {
        
        
        if (page == null) {                
            page = 1;
        }        
        
        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                String json = "[]";
                
                CasaComercialJSON casacomercialjson = new CasaComercialJSON();                
                
                JsonArray jsonarray = casacomercialjson.pagomes(casacomercial, agno, mes, page);
                        
                json = jsonarray.toString();        
                
                return Response
                        .status(Response.Status.OK)
                        .entity( json )
                        .header("token", autorizacion.encriptar())     
                        .header("total_registros", casacomercialjson.total_registros )
                        .build();                       
            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();     
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    

    
    
    
    
    

    
    
    @GET        
    @Path("/expedidasfechas/{casacomercial}/{fechadesde}/{fechahasta}"  )    
    public Response expedidasfechas (            
            @PathParam ("casacomercial") Integer casacomercial,
            @PathParam ("fechadesde") String fechadesde,
            @PathParam ("fechahasta") String fechahasta,
            @QueryParam("page") Integer page,
            @HeaderParam("token") String strToken
            ) {
        
        
        if (page == null) {                
            page = 1;
        }        
        
        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                String json = "[]";
                
                CasaComercialJSON casacomercialjson = new CasaComercialJSON();                
                
                JsonArray jsonarray = casacomercialjson.expedidasfechas(casacomercial, fechadesde, 
                        fechahasta, page);
                        
                json = jsonarray.toString();        
                
                return Response
                        .status(Response.Status.OK)
                        .entity( json )
                        .header("token", autorizacion.encriptar())     
                        .header("total_registros", casacomercialjson.total_registros )
                        .build();                       
            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();     
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    

    
    
    @GET        
    @Path("/expedidasfechas/suma/{casacomercial}/{fechadesde}/{fechahasta}"  )    
    public Response expedidasfechas_suma (            
            @PathParam ("casacomercial") Integer casacomercial,
            @PathParam ("fechadesde") String fechadesde,
            @PathParam ("fechahasta") String fechahasta,
            @QueryParam("page") Integer page,
            @HeaderParam("token") String strToken
            ) {
        
        
        if (page == null) {                
            page = 1;
        }        
        
        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                String json = "[]";
                
                CasaComercialJSON casacomercialjson = new CasaComercialJSON();                
                
                JsonArray jsonarray = casacomercialjson.expedidasfechas_suma(casacomercial, 
                        fechadesde, fechahasta);
                        
                json = jsonarray.toString();        
                
                return Response
                        .status(Response.Status.OK)
                        .entity( json )
                        .header("token", autorizacion.encriptar())     
                        .header("total_registros", casacomercialjson.total_registros )
                        .build();                       
            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();     
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    

    
    
    
    
    
    
    
    @GET        
    @Path("/pagoporfechas/{casacomercial}/{fechadesde}/{fechahasta}"  )    
    public Response pagoporfechas (            
            @PathParam ("casacomercial") Integer casacomercial,
            @PathParam ("fechadesde") String fechadesde,
            @PathParam ("fechahasta") String fechahasta,
            @QueryParam("page") Integer page,
            @HeaderParam("token") String strToken
            ) {
        
        
        if (page == null) {                
            page = 1;
        }        
        
        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                String json = "[]";
                
                CasaComercialJSON casacomercialjson = new CasaComercialJSON();                
                
                
                JsonArray jsonarray = casacomercialjson.pagoporfechas(
                        casacomercial, fechadesde, fechahasta, page);
                        
                
                json = jsonarray.toString();        
                
                return Response
                        .status(Response.Status.OK)
                        .entity( json )
                        .header("token", autorizacion.encriptar())     
                        .header("total_registros", casacomercialjson.total_registros )
                        .build();                       
            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();     
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    

    
    
    
    @GET        
    @Path("/pagoporfechas/suma/{casacomercial}/{fechadesde}/{fechahasta}"  )    
    public Response pagoporfechas_suma (            
            @PathParam ("casacomercial") Integer casacomercial,
            @PathParam ("fechadesde") String fechadesde,
            @PathParam ("fechahasta") String fechahasta,
            @QueryParam("page") Integer page,
            @HeaderParam("token") String strToken
            ) {
        
        
        if (page == null) {                
            page = 1;
        }        
        
        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                String json = "[]";
                
                CasaComercialJSON casacomercialjson = new CasaComercialJSON();                
                
                JsonArray jsonarray = casacomercialjson.pagoporfechas_suma(casacomercial, 
                        fechadesde, fechahasta);
                        
                json = jsonarray.toString();        
                
                return Response
                        .status(Response.Status.OK)
                        .entity( json )
                        .header("token", autorizacion.encriptar())     
                        .header("total_registros", casacomercialjson.total_registros )
                        .build();                       
            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();     
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    

        
        
    
    
}