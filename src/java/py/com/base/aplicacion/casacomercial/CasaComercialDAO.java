/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.casacomercial;


import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class CasaComercialDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public CasaComercialDAO ( ) throws IOException  {
    }
      
    
    public List<CasaComercial>  list (Integer page) {
                
        List<CasaComercial>  lista = null;        
        try {                        
                        
            CasaComercialRS rs = new CasaComercialRS();            
            
            
            lista = new Coleccion<CasaComercial>().resultsetToList(
                    new CasaComercial(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      
    
    

    
    
    public List<CasaComercial>  search (Integer page, String busqueda) {
                
        List<CasaComercial>  lista = null;
        
        try {                       
                        
            CasaComercialRS rs = new CasaComercialRS();
            lista = new Coleccion<CasaComercial>().resultsetToList(
                    new CasaComercial(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    

    
}
