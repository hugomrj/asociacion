/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.casacomercial;


import java.io.IOException;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */

public class CasaComercialSQL {
    
        
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new CasaComercial(), busqueda);        
        
        return sql ;             
    }        
       

    
    
    public String pagomes  (Integer casacomercial, Integer agno, Integer mes ) 
            throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("CasaComercial");
        
        if (casacomercial == 0 ){
        
            readerSQL.fileExt = "pagomes_all.sql";        
            sql = readerSQL.get(  agno, mes );                      
        
        }
        else{
        
            readerSQL.fileExt = "pagomes.sql";        
            sql = readerSQL.get(  agno, mes, casacomercial );
        
        }

        return sql ;                     
    }


    
    
    
    public String expedidasfechas  (Integer casacomercial, 
            String fechadesde, String fechahasta ) 
            throws IOException {
        
        
            if (fechadesde.trim().equals("0")){                       
                fechadesde = "19010101";
            }
        
            if (fechahasta.trim().equals("0")){
                fechahasta = "19000101";
            }            
                    
        
        
            String sql = "";                                 
            ReaderT readerSQL = new ReaderT("CasaComercial");
        
        
            readerSQL.fileExt = "expedidasfechas.sql";        
            sql = readerSQL.get(  fechadesde, fechahasta, casacomercial );
        
    
        return sql ;                     
    }

    
    public String expedidasfechas_suma  (Integer casacomercial, 
            String fechadesde, String fechahasta ) 
            throws IOException {
        
        
            if (fechadesde.trim().equals("0")){                       
                fechadesde = "19010101";
            }
        
            if (fechahasta.trim().equals("0")){
                fechahasta = "19000101";
            }            
                        
        
        
            String sql = "";                                 
            ReaderT readerSQL = new ReaderT("CasaComercial");
        
        
            readerSQL.fileExt = "expedidasfechas_suma.sql";        
            sql = readerSQL.get(  fechadesde, fechahasta, casacomercial );
        
            

    
        return sql ;                     
    }

    
    
    
    
    
    public String pagoporfechas  (Integer casacomercial, String fechadesde, 
            String fechahasta ) 
            throws IOException {
        
        
            if (fechadesde.trim().equals("0")){                       
                fechadesde = "19010101";
            }
        
            if (fechahasta.trim().equals("0")){
                fechahasta = "19000101";
            }            
            
        
            String sql = "";                                 
            ReaderT readerSQL = new ReaderT("CasaComercial");
        
        
            readerSQL.fileExt = "pagoporfechas.sql";        
            sql = readerSQL.get(  fechadesde, fechahasta, casacomercial );
            
            
        
        return sql ;                     
    }
    
    
    
    
    
    public String pagoporfechas_suma  (Integer casacomercial,
             String fechadesde, String fechahasta) 
            throws IOException {
        
            if (fechadesde.trim().equals("0")){                       
                fechadesde = "19010101";
            }
        
            if (fechahasta.trim().equals("0")){
                fechahasta = "19000101";
            }             
        
        
            String sql = "";                                 
            ReaderT readerSQL = new ReaderT("CasaComercial");
        
        
            readerSQL.fileExt = "pagoporfechas_suma.sql";        
            sql = readerSQL.get(  fechadesde, fechahasta, casacomercial );
        
    
            
        return sql ;                     
    }
    
    
    
}
