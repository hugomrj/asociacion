/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.casacomercial;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.sql.BasicSQL;
import nebuleuse.ORM.sql.SentenciaSQL;
import nebuleuse.ORM.xml.Global;
import py.com.base.aplicacion.ordencompra.OrdenCompraSQL;

/**
 *
 * @author hugom_000
 */
public class CasaComercialRS {

    
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;          
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
        public Integer total_registros = 0;
        
    
    public CasaComercialRS ( ) throws IOException, SQLException  {
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();              
    }
   
    
    
    public ResultSet  list ( Integer page ) throws Exception {

            statement = conexion.getConexion().createStatement();      
            
            String sql = SentenciaSQL.select(new CasaComercial());       


            this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            sql = sql + BasicSQL.limite_offset(page, lineas);

            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
    
    
   
    public ResultSet  search ( Integer page, String busqueda ) throws Exception {

            statement = conexion.getConexion().createStatement();                  
            String sql = new CasaComercialSQL().search(busqueda);
            
            this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            sql = sql + BasicSQL.limite_offset(page, lineas);
            resultset = statement.executeQuery(sql);     
            
            conexion.desconectar();                
            return resultset;                 
            
    }
   
    


    
    public ResultSet  pagomes (Integer casacomercial, Integer agno, Integer mes, Integer page ) 
            throws Exception {

            
            statement = conexion.getConexion().createStatement();             
            
            String sql = new CasaComercialSQL().pagomes (casacomercial, agno, mes );
    
            
            this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            sql = sql + BasicSQL.limite_offset(page, lineas);            
            
            resultset = statement.executeQuery(sql);     
            
            conexion.desconectar();                
            return resultset;                      
            
    }


    
    
    public ResultSet  expedidasfechas (Integer casacomercial, 
            String fechadesde, String fechahasta, Integer page ) 
            throws Exception {

            
            statement = conexion.getConexion().createStatement();             
            
            String sql = new CasaComercialSQL().expedidasfechas (casacomercial, fechadesde, fechahasta );
    
            
            this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            sql = sql + BasicSQL.limite_offset(page, lineas);            
            
            resultset = statement.executeQuery(sql);     
            
            conexion.desconectar();                
            return resultset;                      
            
    }

    
    public ResultSet  expedidasfechas_suma (Integer casacomercial,
            String fechadesde, String fechahasta ) 
            throws Exception {

            
            statement = conexion.getConexion().createStatement();             
            
            String sql = new CasaComercialSQL().expedidasfechas_suma (casacomercial, 
                    fechadesde, fechahasta );
    
            
            resultset = statement.executeQuery(sql);     
            
            conexion.desconectar();                
            return resultset;                      
            
    }

    
    
    
    public ResultSet  pagoporfechas (Integer casacomercial, 
            String  fechadesde, String  fechahasta, Integer page ) 
            throws Exception {

            
            statement = conexion.getConexion().createStatement();             
            
            String sql = new CasaComercialSQL().pagoporfechas (casacomercial, fechadesde, fechahasta );
    
            
            this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            sql = sql + BasicSQL.limite_offset(page, lineas);            
            
            resultset = statement.executeQuery(sql);     
            
            conexion.desconectar();                
            return resultset;                      
            
    }    
    
    


    
    public ResultSet  pagoporfechas_suma (Integer casacomercial, 
            String  fechadesde, String  fechahasta ) 
            throws Exception {

            
            statement = conexion.getConexion().createStatement();             
            
            String sql = new CasaComercialSQL().pagoporfechas_suma (casacomercial, fechadesde, fechahasta );
    
            
            //this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            //sql = sql + BasicSQL.limite_offset(page, lineas);            
            
            resultset = statement.executeQuery(sql);     
            
            conexion.desconectar();                
            return resultset;                      
            
    }
    
    
    
    
}
