/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordencompra;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.util.FileXls;
import py.com.base.aplicacion.sociodescuento.SocioDescuentoRS;

/**
 *
 * @author hugo
 */
@WebServlet(name = "OrdenCompraXLS_lista", 
        urlPatterns = {"/ordencompra/lista.xls"})
public class OrdenCompraXLS_lista extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) 
            throws IOException, SQLException, Exception
 {
        //response.setContentType("text/html;charset=UTF-8");
        
     
     
        
            String strToken = "";
        
            strToken = request.getHeader("token");
            Autentificacion autorizacion = new Autentificacion();
            if (autorizacion.verificar(strToken))
            {  
            
        
                Integer aa = 0;
                Integer mm = 0;

                aa = Integer.parseInt( request.getParameter("aa").toString() ) ;       
                mm = Integer.parseInt( request.getParameter("mm").toString() ) ;

                FileXls filexls = new FileXls();
                
                filexls.Iniciar(request);
                filexls.folder = "/files";                
                filexls.name = "/base.xls";                
        
                
                
                ArrayList<String> cabecera = new ArrayList<String>();
                cabecera.add("ordencompra");
                cabecera.add("fecha");
                cabecera.add("socio");
                cabecera.add("cedula");
                cabecera.add("nombre_apellido");
                cabecera.add("casa_comercial");
                cabecera.add("credito");
                cabecera.add("cantidad_cuotas");
                cabecera.add("monto_cuota");
                cabecera.add("cuotas_pagadas");
                cabecera.add("credito_saldo");                
                filexls.setCabecera(cabecera);
        
                
                
                
                ArrayList<String> campos = new ArrayList<String>();
                campos.add("ordencompra");
                campos.add("fecha");
                campos.add("socio");
                campos.add("cedula");
                campos.add("nombre_apellido");
                campos.add("nombre");
                campos.add("credito");
                campos.add("cantidad_cuotas");
                campos.add("monto_cuota");
                campos.add("cuotas_pagadas");
                campos.add("credito_saldo");
                                
                filexls.setCampos(campos);                
                
                
                OrdenCompraRS rs = new OrdenCompraRS();
                            
                ResultSet resultset = rs.list_mes_agno_xls ( mm, aa );

                
                
                filexls.gen(resultset);                
                filexls.newFileStream();
                
                ServletContext context = getServletContext();

                response.setHeader("token", autorizacion.encriptar());
                filexls.getServeltFile(request, response, context);
                filexls.close();
                
                
                
            }
            else{   
                //System.out.println("no autorizado");                
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
            

            
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(OrdenCompraXLS_lista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
