/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.aplicacion.ordencompra;



import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.seguridad.Autentificacion;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

/**
 *
 * @author hugo
 */

@WebServlet(name = "OrdenCompraRPT_lista", 
        urlPatterns = {"/OrdenCompra/Reporte/lista.pdf"})


public class OrdenCompraRPT_lista extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        
        
        
            HttpSession sesion = request.getSession();        
            Persistencia persistencia = new Persistencia();   
            OrdenCompra ordencompra = new OrdenCompra();
            
            
            
        
        try { 
        
                Integer par_mes = 0;
                par_mes = Integer.parseInt( request.getParameter("mes").toString() ) ;  
            
                Integer par_aa = 0;
                par_aa = Integer.parseInt( request.getParameter("aa").toString() ) ;  
            
                
                
                Autentificacion autorizacion = new Autentificacion();
                String strToken =  (String) sesion.getAttribute("sesiontoken");
                
                
                //if (autorizacion.verificar(strToken))
                if (true)
                {                      
        
                    
                    String archivo = "ordenes_compras";            
                    String archivo_jrxml = archivo+".jrxml";
                    response.setHeader("Content-disposition","inline; filename="+archivo+".pdf");
                    response.setContentType("application/pdf");

            //response.setContentType("image/jpeg");            
            //Response.Status status  = Response.Status.OK;



                    Conexion cnx = new Conexion();
                    cnx.conectar();
                    String url =  request.getServletContext().getRealPath("/WEB-INF")+"/jasper/";
                    url = url + archivo_jrxml;

                    
                    
                    // parametros
                    Map<String, Object> parameters = new HashMap<String, Object>();

                    parameters.put("par_aa", par_aa );            
                    parameters.put("par_mes", par_mes );            
                    
                    
                    
String report_path = request.getServletContext().getRealPath("/WEB-INF")+"/jasper/";;
report_path = report_path.replace("\\", "/") ;

                    parameters.put("report_path", report_path );    
                    
                    
                
                    //ordencompra = (OrdenCompra) persistencia.filtrarId(ordencompra, par_ordencompra);  
                
                    /*
                    Convercion conversion = new Convercion();            
                    parameters.put("par_monto_letras", 
                            conversion.numeroaLetras(ordencompra.getCredito()).toUpperCase()   );
                    */
                    
                    
                    
                    JasperReport report = JasperCompileManager.compileReport(url);            
                    JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, cnx.getConexion());


                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    byte[] reportePdf = JasperExportManager.exportReportToPdf(jasperPrint);

                    response.setContentLength(reportePdf.length);

                    servletOutputStream.write(reportePdf, 0, reportePdf.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();    

                    
                    response.setStatus(HttpServletResponse.SC_ACCEPTED);
                }
                else
                {  
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }                
                
//                autorizacion.actualizar();

            
            
        }         
        catch (JRException ex) 
        {
            Logger.getLogger(OrdenCompraRPT_lista.class.getName()).log(Level.SEVERE, null, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (Exception ex) {
            Logger.getLogger(OrdenCompraRPT_lista.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {          
            sesion.invalidate();
        }

            
            
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(OrdenCompraRPT_lista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(OrdenCompraRPT_lista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
