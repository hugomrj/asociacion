/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordencompra;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;
import py.com.base.aplicacion.ordencomprapago.OrdenCompraPago;
import py.com.base.aplicacion.ordencomprapago.OrdenCompraPagoDAO;
import py.com.base.aplicacion.ordencomprapagoseguro.OrdenCompraPagoSeguroDAO;

/**
 *
 * @author hugom_000
 */
public class OrdenCompraDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public OrdenCompraDAO ( ) throws IOException  {
    }
      
    
    public List<OrdenCompra>  list (Integer page) {
                
        List<OrdenCompra>  lista = null;        
        try {                        
                        
            OrdenCompraRS rs = new OrdenCompraRS();            
            
            
            lista = new Coleccion<OrdenCompra>().resultsetToList(
                    new OrdenCompra(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      
    
    
    
    public List<OrdenCompra>  list_mes_agno ( Integer mes, Integer agno,  Integer page ) {
                
        List<OrdenCompra>  lista = null;        
        try {                        
                        
            OrdenCompraRS rs = new OrdenCompraRS();            
            
            
            lista = new Coleccion<OrdenCompra>().resultsetToList(
                    new OrdenCompra(),
                    rs.list_mes_agno(mes, agno, page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      
    
    public List<OrdenCompra>  list_mes_agno_search ( Integer mes, Integer agno,  Integer page,
            String buscar ) {
                
        List<OrdenCompra>  lista = null;        
        try {                        
                        
            OrdenCompraRS rs = new OrdenCompraRS();            
            
            
            lista = new Coleccion<OrdenCompra>().resultsetToList(
                    new OrdenCompra(),
                    rs.list_mes_agno_search(mes, agno, page, buscar)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<OrdenCompra>  search (Integer page, String busqueda) {
                
        List<OrdenCompra>  lista = null;
        
        try {                       
                        
            OrdenCompraRS rs = new OrdenCompraRS();
            lista = new Coleccion<OrdenCompra>().resultsetToList(
                    new OrdenCompra(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
    
    
    
    public List<OrdenCompra>  nombreci (Integer page, String busqueda) {
                
        List<OrdenCompra>  lista = null;
        
        try {                       
                        
            OrdenCompraRS rs = new OrdenCompraRS();
            lista = new Coleccion<OrdenCompra>().resultsetToList(
                    new OrdenCompra(),
                    rs.nombreci(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
    
    
    
    public Long restarSaldo (Integer orden, Long montopago ) throws Exception {
        
        Long res = -1L;
    
        Persistencia persistencia = new Persistencia();
        OrdenCompra ordencompra = new OrdenCompra();
        ordencompra = (OrdenCompra) persistencia.filtrarId(ordencompra, orden);
        
        Long montosaldo = ordencompra.getCredito_saldo();        
        res = montosaldo - montopago;
    
        return  res;
    }

    
    

    public Long actualizarSaldo (Integer orden, Long montopago, Integer numerocuota ) 
            throws Exception {
        
        Long res = -1L;
        OrdenCompraRS rs = new OrdenCompraRS();        
        
        ResultSet resultset = rs.actualizarSaldo(orden, montopago, numerocuota);
        
        if (resultset.next() ){
            res = Long.parseLong( resultset.getString("credito_saldo") );
        }
            
        return  res;
    }
    
    
    
    
    public void cierreMensual (Integer mes, Integer agno, Integer cierreid ) 
            throws Exception {
        
            OrdenCompraRS  ordencompraRS = new OrdenCompraRS();
            
            ResultSet rs = ordencompraRS.noCompletadas(mes, agno);
        
            
            OrdenCompraPago ordencomprapago = new OrdenCompraPago();
            OrdenCompraPagoDAO ordencompradao = new OrdenCompraPagoDAO();
            
            OrdenCompraPagoSeguroDAO ordencomprapagosegurodao 
                    = new OrdenCompraPagoSeguroDAO();
            
            //10.000
            ordencomprapagosegurodao.setMonto_seguro();
            
            while (rs.next()){
    
                Integer ordenNro =   Integer.parseInt( rs.getString("ordencompra")) ;
                Long montocuota =   Long.parseLong( rs.getString("monto_cuota")) ;
                Integer socio = Integer.parseInt( rs.getString("socio")) ;
                
                ordencomprapago = ordencompradao.cargarCuotaCierreMes(
                        ordenNro, montocuota, mes, agno, cierreid  );
                
                ordencomprapagosegurodao.agregarSeguro(
                        ordencomprapago,socio );                
                
            }
            
            System.out.println("fin cierreMensual");
            
        
    }
    
    




    
     
}
