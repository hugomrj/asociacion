/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordencompra;

import java.io.IOException;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */

public class OrdenCompraSQL {
    
        
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new OrdenCompra(), busqueda);        
        
        return sql ;             
    }        
    
    
        
    public String nombreci ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("OrdenCompra");
        
            readerSQL.fileExt = "nombreci.sql";

            sql = readerSQL.get(  busqueda );                      

        return sql ;             
    }        
    
    
    
    
    
    public String consulta_casacomercial  (Integer casacomericial ) 
            throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("OrdenCompra");

            readerSQL.fileExt = "por_casacomercial.sql";

            sql = readerSQL.get(  casacomericial );                      
        
        return sql ;                     
    }

    
    
    
    public String consulta_casacomercial_socio  (Integer socio ) 
            throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("OrdenCompra");

            readerSQL.fileExt = "consulta_casacomercial_socio.sql";

            sql = readerSQL.get(  socio );                      

        return sql ;                     
    }

    
    
    public String  list_mes_agno (Integer mes, Integer agno ) 
            throws IOException {

        String sql = "";
        
        ReaderT readerSQL = new ReaderT("OrdenCompra");
        readerSQL.fileExt = "list_mes_agno.sql";

        sql = readerSQL.get(  mes, agno );                      
    
        return sql ;                     
    }

    
    public String  list_mes_agno_xls (Integer mes, Integer agno ) 
            throws IOException {

        String sql = "";
        
        ReaderT readerSQL = new ReaderT("OrdenCompra");
        readerSQL.fileExt = "list_mes_agno_xls.sql";

        sql = readerSQL.get(  mes, agno );                      
    
        return sql ;                     
    }
    
    
        
    public String  list_mes_agno_search (Integer mes, Integer agno, String buscar ) 
            throws IOException {

        String sql = "";
        
        ReaderT readerSQL = new ReaderT("OrdenCompra");
        readerSQL.fileExt = "list_mes_agno_search.sql";

        sql = readerSQL.get(  mes, agno, buscar );    
        
        return sql ;                     
    }
    
    
    
    
    
        
    public String  actualizarSaldo (Integer orden, Long montopago, Integer  numerocuota ) 
            throws IOException {

        String sql = "";
        
        ReaderT readerSQL = new ReaderT("OrdenCompra");
        readerSQL.fileExt = "actualizarSaldo.sql";

        sql = readerSQL.get(  orden, montopago, numerocuota );    
        
        return sql ;                     
    }
    
    
    
        
    public String  noCompletadas ( Integer mes, Integer agno  ) 
            throws IOException {

        String sql = "";
        
        ReaderT readerSQL = new ReaderT("OrdenCompra");
        readerSQL.fileExt = "noCompletadas.sql";

        sql = readerSQL.get( mes, agno  );    
        
        return sql ;                     
    }
    
    
        

    
    
    
    
    
    
}
