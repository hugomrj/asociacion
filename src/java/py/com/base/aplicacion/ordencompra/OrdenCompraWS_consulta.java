/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.aplicacion.ordencompra;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;


/**
 * REST Web Service
 * @author hugo
 */





@Path("/consulta/ordenescompras")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class OrdenCompraWS_consulta {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new Gson();          
    private Response.Status status  = Response.Status.OK;
    
    OrdenCompra com = new OrdenCompra();       
                         
    public OrdenCompraWS_consulta() {
    }

    
    

        
    
    
    
    @GET        
    @Path("/casacomercial/{casacomercial}"  )    
    public Response consulta003 (            
            @PathParam ("casacomercial") Integer casacomercial,
            @QueryParam("page") Integer page,
            @HeaderParam("token") String strToken
            ) {
        
            if (page == null) {                
                page = 1;
            }        

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                String json = "[]";
        
                OrdenCompraJSON ordencomprajson = new OrdenCompraJSON();
                        
                JsonArray jsonarray = ordencomprajson.consulta_casacomercial( casacomercial, page );

                json = jsonarray.toString();        
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity( json )
                        .header("token", autorizacion.encriptar())     
                        .header("total_registros", ordencomprajson.total_registros )
                        .build();                       
            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();     
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    

    

    
    
    @GET        
    @Path("/socio/{socio}"  )    
    public Response casacomercial_socio (            
            @PathParam ("socio") Integer socio,
            @QueryParam("page") Integer page,
            @HeaderParam("token") String strToken
            ) {
        
        
            if (page == null) {                
                page = 1;
            }        
                

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                String json = "[]";
        
                OrdenCompraJSON ordencomprajson = new OrdenCompraJSON();
                        
                JsonArray jsonarray = ordencomprajson.consulta_casacomercial_socio( socio, page );

                json = jsonarray.toString();        
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity( json )
                        .header("token", autorizacion.encriptar())     
                        .header("total_registros", ordencomprajson.total_registros )
                        .build();                       
            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();     
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    

    
    
    
    
    
    
    @GET       
    //@Path("/search/{opt : (.*)}") 
    @Path("/nombreci/") 
    public Response nombreci ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page,              
            @MatrixParam("q") String q
            ) {
        
        
            if (page == null) {                
                page = 1;
            }
            if (q == null){            
                q = "";                
            }
            
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                OrdenCompraDAO dao = new OrdenCompraDAO();                
                List<OrdenCompra> lista = dao.nombreci(page, q);                
                String json = gson.toJson( lista );     
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();                       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();        
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    

    
    
    
    
    
    
}