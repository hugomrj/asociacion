/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordencompra;

import java.util.Date;
import py.com.base.aplicacion.casacomercial.CasaComercial;
import py.com.base.aplicacion.socio.Socio;

/**
 *
 * @author hugom_000
 */
public class OrdenCompra {
 
    private Integer ordencompra;
    private Date fecha;
    private CasaComercial casacomercial;
    private Socio socio;
    private Long credito;
    private Integer cantidad_cuotas;
    private Long monto_cuota;
    private Integer cuotas_pagadas;
    private Long credito_saldo; 
    private String user_add;
    private String concepto; 

    public Integer getOrdencompra() {
        return ordencompra;
    }

    public void setOrdencompra(Integer ordencompra) {
        this.ordencompra = ordencompra;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public CasaComercial getCasacomercial() {
        return casacomercial;
    }

    public void setCasacomercial(CasaComercial casacomercial) {
        this.casacomercial = casacomercial;
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    public Long getCredito() {
        return credito;
    }

    public void setCredito(Long credito) {
        this.credito = credito;
    }

    public Integer getCantidad_cuotas() {
        return cantidad_cuotas;
    }

    public void setCantidad_cuotas(Integer cantidad_cuotas) {
        this.cantidad_cuotas = cantidad_cuotas;
    }

    public Long getMonto_cuota() {
        return monto_cuota;
    }

    public void setMonto_cuota(Long monto_cuota) {
        this.monto_cuota = monto_cuota;
    }

    public Integer getCuotas_pagadas() {
        return cuotas_pagadas;
    }

    public void setCuotas_pagadas(Integer cuotas_pagadas) {
        this.cuotas_pagadas = cuotas_pagadas;
    }

    public Long getCredito_saldo() {
        return credito_saldo;
    }

    public void setCredito_saldo(Long credito_saldo) {
        this.credito_saldo = credito_saldo;
    }

    public String getUser_add() {
        return user_add;
    }

    public void setUser_add(String user_add) {
        this.user_add = user_add;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }
    
    
}


