/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordencomprapago;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;
import py.com.base.aplicacion.ordencompra.OrdenCompraDAO;

/**
 *
 * @author hugom_000
 */
public class OrdenCompraPagoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public OrdenCompraPagoDAO ( ) throws IOException  {
    }
      
    
    public List<OrdenCompraPago>  list (Integer ordencompra) {
                
        List<OrdenCompraPago>  lista = null;        
        try {                        
                        
            OrdenCompraPagoRS rs = new OrdenCompraPagoRS();            
            
            lista = new Coleccion<OrdenCompraPago>().resultsetToList(
                    new OrdenCompraPago(),
                    rs.list(ordencompra)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      
    
    
    
public Integer maxNumeroCuota (Integer ordencompra) 
        throws IOException, SQLException, Exception{

    Integer ret  = 1;
    
    OrdenCompraPagoRS rs = new OrdenCompraPagoRS();
    
    ResultSet resulset = rs.maxNumeroCuota(ordencompra);

    if (resulset.next()){
        ret = resulset.getInt("max_numero_cuota");
        ret++;
    }
    
    return ret;
}  





public OrdenCompraPago cargarCuotaCierreMes  (Integer ordencompra, Long montocuota,
        Integer mes, Integer agno, Integer cierreid )         
        throws IOException, Exception {

        OrdenCompraPago pago = new OrdenCompraPago();                   
                
        pago.setOrdencompra(ordencompra);
                
        pago.setNumero_cuota(
                new OrdenCompraPagoDAO().maxNumeroCuota(
                        ordencompra
                )
        );                

        
        Long monto_saldo = new OrdenCompraDAO().restarSaldo(
                ordencompra,
                montocuota
        );  
        

        pago.setCredito_saldo( monto_saldo  );                    
        pago.setFecha(new Date());
        pago.setMonto_cuota(montocuota);
        pago.setMes(mes);
        pago.setAgno(agno);
        
        
        pago.setId_cierre(cierreid);
        
        
        
        pago = (OrdenCompraPago) persistencia.insert(pago);                

        
        new OrdenCompraDAO().actualizarSaldo(
                ordencompra, monto_saldo, pago.getNumero_cuota()
        );                    

        
        
        
    return pago;
    
}  



      
}
