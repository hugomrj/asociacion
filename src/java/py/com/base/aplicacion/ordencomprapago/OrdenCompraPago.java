/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordencomprapago;

import java.util.Date;

/**
 *
 * @author hugom_000
 */
public class OrdenCompraPago {
    
    private Integer id;
    private Integer ordencompra;
    private Date fecha;
    private Integer numero_cuota;
    private Long monto_cuota;
    private Long credito_saldo;
    private Integer mes;
    private Integer agno;
    private Integer id_cierre;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrdencompra() {
        return ordencompra;
    }

    public void setOrdencompra(Integer ordencompra) {
        this.ordencompra = ordencompra;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getNumero_cuota() {
        return numero_cuota;
    }

    public void setNumero_cuota(Integer numero_cuota) {
        this.numero_cuota = numero_cuota;
    }

    public Long getMonto_cuota() {
        return monto_cuota;
    }

    public void setMonto_cuota(Long monto_cuota) {
        this.monto_cuota = monto_cuota;
    }

    public Long getCredito_saldo() {
        return credito_saldo;
    }

    public void setCredito_saldo(Long credito_saldo) {
        this.credito_saldo = credito_saldo;
    }

    /**
     * @return the mes
     */
    public Integer getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(Integer mes) {
        this.mes = mes;
    }

    /**
     * @return the agno
     */
    public Integer getAgno() {
        return agno;
    }

    /**
     * @param agno the agno to set
     */
    public void setAgno(Integer agno) {
        this.agno = agno;
    }

    public Integer getId_cierre() {
        return id_cierre;
    }

    public void setId_cierre(Integer id_cierre) {
        this.id_cierre = id_cierre;
    }
    
    
    
}


