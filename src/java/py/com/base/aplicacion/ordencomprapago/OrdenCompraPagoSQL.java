/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordencomprapago;

import java.io.IOException;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */

public class OrdenCompraPagoSQL {
        
        
    public String list ( Integer ordencompra )
            throws Exception {
    
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("OrdenCompraPago");
        
            readerSQL.fileExt = "list.sql";

            sql = readerSQL.get(  ordencompra );                      
            
        return sql ;             
    }        
    
    
        
    public String maxNumeroCuota ( Integer ordencompra )
            throws Exception {
    
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("OrdenCompraPago");
        
            readerSQL.fileExt = "maxNumeroCuota.sql";

            sql = readerSQL.get(  ordencompra );                      
            
        return sql ;             
    }        
    
    
    
}
