/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.aplicacion.ordencomprapago;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.sql.SQLException;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import py.com.base.aplicacion.ordencompra.OrdenCompra;
import py.com.base.aplicacion.ordencompra.OrdenCompraDAO;
import py.com.base.aplicacion.ordencomprapagoseguro.OrdenCompraPagoSeguroDAO;


/**
 * REST Web Service
 * @author hugo
 */


@Path("ordenescompraspagos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class OrdenCompraPagoWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new Gson();          
    private Response.Status status  = Response.Status.OK;
    
    OrdenCompraPago com = new OrdenCompraPago();       
                         
    public OrdenCompraPagoWS() {
    }

    
    
    
    @GET    
    @Path("/ordencompra/{ordencompra}"  )
    public Response list ( 
            @HeaderParam("token") String strToken,
            @PathParam ("ordencompra") Integer ordencompra
            ) {
        

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                OrdenCompraPagoDAO dao = new OrdenCompraPagoDAO(); 
                
                List<OrdenCompraPago> lista = dao.list(ordencompra); 
                
                String json = gson.toJson( lista );     
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();                       
            }
            else
            {

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                             
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    

    
    
    @POST
    @Path("/ordencompra/{ordencompra}"  )
    public Response add( 
            @HeaderParam("token") String strToken,
            @PathParam ("ordencompra") Integer ordencompra,
            String json ) {
                     
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();    

                
                Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
                OrdenCompraPago req = gsonf.fromJson(json, OrdenCompraPago.class);                   
                
                req.setOrdencompra(ordencompra);
                
                
                req.setNumero_cuota(
                        new OrdenCompraPagoDAO().maxNumeroCuota(
                                req.getOrdencompra()
                        )
                );                
                
                
                Long monto_saldo = new OrdenCompraDAO().restarSaldo(
                        ordencompra,
                        req.getMonto_cuota()
                );    
                
                
                //Long monto_saldo  = 1L;
                
                if (monto_saldo <  0){

                    return Response
                            .status(Response.Status.BAD_GATEWAY)
                            .entity("error saldo negativo  " + monto_saldo)
                            .header("token", autorizacion.encriptar())
                            .build();                    
                }
                else
                {
                    
                    req.setCredito_saldo( monto_saldo  );                    
                    
                    req.setId_cierre(-1);
                            
                            
                    this.com = (OrdenCompraPago) persistencia.insert(req);
                    new OrdenCompraDAO().actualizarSaldo(
                            ordencompra, monto_saldo, req.getNumero_cuota()
                    );                    

                    
                    // filtrar orden de compra
                    OrdenCompra ordencompraObj = new OrdenCompra();
                    ordencompraObj = (OrdenCompra) persistencia.filtrarId(
                            ordencompraObj, ordencompra);  
                    
                    
                    // agregar seguro de cuota              
                    
                    OrdenCompraPagoSeguroDAO ordencomprapagosegurodao 
                            = new OrdenCompraPagoSeguroDAO();
                    
                    ordencomprapagosegurodao.setMonto_seguro();
                    ordencomprapagosegurodao.agregarSeguro(
                            this.com, ordencompraObj.getSocio().getSocio());
                    
                   

                    json = gson.toJson(this.com);
                    
                    
                    if (this.com == null){
                        this.status = Response.Status.NO_CONTENT;
                    }

                    return Response
                            .status(this.status)
                            .entity(json)
                            .header("token", autorizacion.encriptar())
                            .build();       
                }
            }
            else
            {                 
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();             
            }
        }     
        
        
        
        catch (SQLException ex) {   
            return Response
                    .status(Response.Status.BAD_GATEWAY)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();    
        }        
                        
        
        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }    
    }    
 
    
    
         
    

    
}