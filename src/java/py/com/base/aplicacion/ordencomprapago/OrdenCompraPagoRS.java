/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordencomprapago;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.sql.BasicSQL;
import nebuleuse.ORM.sql.SentenciaSQL;
import nebuleuse.ORM.xml.Global;

/**
 *
 * @author hugom_000
 */
public class OrdenCompraPagoRS {
    
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;          
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
        public Integer total_registros = 0;
        
  
        
    public OrdenCompraPagoRS ( ) throws IOException, SQLException  {
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();              
    }
   
    
    
    
    public ResultSet  list ( Integer ordencompra ) throws Exception {

            statement = conexion.getConexion().createStatement();      
            
            String sql = new OrdenCompraPagoSQL().list(ordencompra);

            this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            
            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
    
        
    public ResultSet  maxNumeroCuota ( Integer ordencompra ) throws Exception {

            statement = conexion.getConexion().createStatement();      
            
            String sql = new OrdenCompraPagoSQL().maxNumeroCuota(ordencompra);
            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
    
    
}
