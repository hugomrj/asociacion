/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.base.aplicacion.socio;

import java.util.Date;
import py.com.base.aplicacion.direccionsueldo.DireccionSueldo;
import py.com.base.aplicacion.relacionlaboral.RelacionLaboral;

/**
 *
 * @author hugom_000
 */
public class Socio {


    private Integer socio;
    private Integer cedula;
    private String nombre_apellido;
    private String telefono;
    private String celular;
    private String direccion_particular;
    private String direccion_laboral;
    private String dependencia;
    private RelacionLaboral  relacion_laboral;
    private Date ingreso_funcionario;
    private DireccionSueldo  direccion_sueldo;
    private Boolean habilitado;
    private Integer gestion_administrativa;
    

    public Integer getSocio() {
        return socio;
    }

    public void setSocio(Integer socio) {
        this.socio = socio;
    }

    public Integer getCedula() {
        return cedula;
    }

    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    public String getNombre_apellido() {
        return nombre_apellido;
    }

    public void setNombre_apellido(String nombre_apellido) {
        this.nombre_apellido = nombre_apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getDireccion_particular() {
        return direccion_particular;
    }

    public void setDireccion_particular(String direccion_particular) {
        this.direccion_particular = direccion_particular;
    }

    public String getDireccion_laboral() {
        return direccion_laboral;
    }

    public void setDireccion_laboral(String direccion_laboral) {
        this.direccion_laboral = direccion_laboral;
    }

    public RelacionLaboral getRelacion_laboral() {
        return relacion_laboral;
    }

    public void setRelacion_laboral(RelacionLaboral relacion_laboral) {
        this.relacion_laboral = relacion_laboral;
    }

    public Date getIngreso_funcionario() {
        return ingreso_funcionario;
    }

    public void setIngreso_funcionario(Date ingreso_funcionario) {
        this.ingreso_funcionario = ingreso_funcionario;
    }

    public String getDependencia() {
        return dependencia;
    }

    public void setDependencia(String dependencia) {
        this.dependencia = dependencia;
    }

    public DireccionSueldo getDireccion_sueldo() {
        return direccion_sueldo;
    }

    public void setDireccion_sueldo(DireccionSueldo direccion_sueldo) {
        this.direccion_sueldo = direccion_sueldo;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public Integer getGestion_administrativa() {
        return gestion_administrativa;
    }

    public void setGestion_administrativa(Integer gestion_administrativa) {
        this.gestion_administrativa = gestion_administrativa;
    }
            
   

}
