/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.aplicacion.socio;



import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.util.Convercion;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import py.com.base.aplicacion.cierremensual.CierreMensual;
import py.com.base.aplicacion.cierremensual.CierreMensualDAO;

/**
 *
 * @author hugo
 */

@WebServlet(name = "Socio_ReporteExtractoActual", 
        urlPatterns = {"/Socio/Reporte/ExtractoActual.pdf"})


public class Socio_ReporteExtractoActual extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        
            HttpSession sesion = request.getSession();        
            
        
        try { 
        
            
                Integer par_socio = 0;
                par_socio = Integer.parseInt( request.getParameter("socio").toString() ) ;
                
                Integer par_agno = 0;
                par_agno = Integer.parseInt( request.getParameter("agno").toString() ) ;
                
                Integer par_mes = 0;
                par_mes = Integer.parseInt( request.getParameter("mes").toString() ) ;
                                
                String par_usuario = "";                
                                
                
                if (par_agno == 0 &&  par_mes == 0 &&  par_socio != 0 ){
    
                    // seleccionar ultimo mes agno de 
                    CierreMensual cierremensual = new CierreMensualDAO().ultimo();            
                    par_agno = cierremensual.getAgno();
                    par_mes = cierremensual.getMes();                
                    
                }
                
                
                
                Autentificacion autorizacion = new Autentificacion();
                String strToken =  (String) sesion.getAttribute("sesiontoken");
                
                
                
                //if (autorizacion.verificar(strToken))
                if (true)
                {                      

                    par_usuario = autorizacion.token.getNombre();
                    
                    
                    String archivo = "socio_extracto_actual";            
                    String archivo_jrxml = archivo+".jrxml";
                    //response.setHeader("Content-disposition","inline; filename="+archivo+".pdf");                    
                    response.setContentType("application/pdf");
                    
                    
                    String nombreapellido = "";
                    Socio socio = new Socio();
                    Persistencia persistencia = new Persistencia();
                    
                    socio =  (Socio) persistencia.filtrarId(socio, par_socio);  
                    nombreapellido = socio.getNombre_apellido().trim().toUpperCase()
                            .replaceAll(" ", "_").replaceAll(",", "");
                    
        System.out.println( nombreapellido );
                                       
                    
                    response.setHeader("Content-disposition","inline; filename="+nombreapellido+".pdf");
                    

                    Conexion cnx = new Conexion();
                    cnx.conectar();
                    String url =  request.getServletContext().getRealPath("/WEB-INF")+"/jasper/";
                    url = url + archivo_jrxml;

                    
                    
                    // parametros
                    Map<String, Object> parameters = new HashMap<String, Object>();

                    
                    parameters.put("par_socio", par_socio );            
                    parameters.put("par_agno", par_agno );            
                    parameters.put("par_mes", par_mes );            
                    parameters.put("par_usuario", par_usuario );            
                    
                    
String report_path = request.getServletContext().getRealPath("/WEB-INF")+"/jasper/";;
report_path = report_path.replace("\\", "/") ;

                    //parameters.put("report_path", report_path );    
                    
                
                    JasperReport report = JasperCompileManager.compileReport(url);            
                    JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, cnx.getConexion());


                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    byte[] reportePdf = JasperExportManager.exportReportToPdf(jasperPrint);

                    response.setContentLength(reportePdf.length);

                    servletOutputStream.write(reportePdf, 0, reportePdf.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();    

                    
                    response.setStatus(HttpServletResponse.SC_ACCEPTED);
                }
                else
                {  
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    
                    
System.out.println( "no autorizado reporte" );     

                    
                }
                
                
//                autorizacion.actualizar();


            
            
            
        }         
        catch (JRException ex) 
        {
            Logger.getLogger(Socio_ReporteExtractoActual.class.getName()).log(Level.SEVERE, null, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (Exception ex) {
            Logger.getLogger(Socio_ReporteExtractoActual.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {          
            sesion.invalidate();
        }

            
            
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Socio_ReporteExtractoActual.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Socio_ReporteExtractoActual.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
