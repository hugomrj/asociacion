/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.socio;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.sql.BasicSQL;
import nebuleuse.ORM.sql.SentenciaSQL;
import nebuleuse.ORM.xml.Global;

/**
 *
 * @author hugom_000
 */
public class SocioRS {

    
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;          
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
        public Integer total_registros = 0;
        
    
    public SocioRS ( ) throws IOException, SQLException  {
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();              
    }
   
    
    
    public ResultSet  list ( Integer page ) throws Exception {

            statement = conexion.getConexion().createStatement();      
            
            String sql = SentenciaSQL.select(new Socio());       

             sql += " order by socios.socio "   ;
            
            
            this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            sql = sql + BasicSQL.limite_offset(page, lineas);

            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
    
    
   
    public ResultSet  search ( Integer page, String busqueda ) throws Exception {

            statement = conexion.getConexion().createStatement();                  
            String sql = new SocioSQL().search(busqueda);
            sql += " order by socios.socio "   ;
            
            this.total_registros =  BasicSQL.cont_registros(conexion, sql);            
            sql = sql + BasicSQL.limite_offset(page, lineas);
            resultset = statement.executeQuery(sql);     
            
            conexion.desconectar();                
            return resultset;                 
            
    }
   
    
       
    
    public ResultSet  extracto ( Integer socio, Integer agno, Integer mes ) 
            throws Exception {
            
            statement = conexion.getConexion().createStatement();             
            
            String sql = new SocioSQL().extracto (socio, agno, mes );
            
            resultset = statement.executeQuery(sql);     
            
            conexion.desconectar();                
            return resultset;                      
            
    }
    
    
    
    
    public ResultSet  extracto_actual ( Integer socio, Integer agno, Integer mes ) 
            throws Exception {
            
            statement = conexion.getConexion().createStatement();             
            
            String sql = new SocioSQL().extracto_actual (socio, agno, mes );
            
            resultset = statement.executeQuery(sql);     
            
            conexion.desconectar();                
            return resultset;                      
            
    }
    
    
    
    
}
