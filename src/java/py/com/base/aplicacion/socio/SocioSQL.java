/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.socio;

import java.io.IOException;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */

public class SocioSQL {
    
        
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new Socio(), busqueda);        
        
        return sql ;             
    }        
       
    
    
    public String extracto  (Integer socio, Integer agno, Integer mes ) 
            throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("Socio");

            readerSQL.fileExt = "extracto.sql";

            sql = readerSQL.get(  socio, agno, mes );                      

            
        return sql ;                     
    }

    
    
    public String extracto_actual  (Integer socio, Integer agno, Integer mes ) 
            throws IOException {
        
        String sql = "";                                 
        ReaderT readerSQL = new ReaderT("Socio");

            readerSQL.fileExt = "extracto_actual.sql";

            sql = readerSQL.get(  socio, agno, mes );                      

            
        return sql ;                     
    }

    
    

    
}
