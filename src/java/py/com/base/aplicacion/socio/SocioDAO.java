/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.socio;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class SocioDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public SocioDAO ( ) throws IOException  {
    }
      
    
    public List<Socio>  list (Integer page) {
                
        List<Socio>  lista = null;        
        try {                        
                        
            SocioRS rs = new SocioRS();            
            
            
            lista = new Coleccion<Socio>().resultsetToList(
                    new Socio(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<Socio>  search (Integer page, String busqueda) {
                
        List<Socio>  lista = null;
        
        try {                       
                        
            SocioRS rs = new SocioRS();
            lista = new Coleccion<Socio>().resultsetToList(
                    new Socio(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    

    
}
