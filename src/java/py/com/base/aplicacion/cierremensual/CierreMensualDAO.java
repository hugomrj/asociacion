/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.cierremensual;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class CierreMensualDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public CierreMensualDAO ( ) throws IOException  {
    }
      
    
    public List<CierreMensual>  list (Integer page) {
                
        List<CierreMensual>  lista = null;        
        try {                        
                        
            CierreMensualRS rs = new CierreMensualRS();            
            
            
            lista = new Coleccion<CierreMensual>().resultsetToList(
                    new CierreMensual(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public CierreMensual ultimo() throws Exception {      

        CierreMensual cierreMensual = new CierreMensual();
          
          
        ResultSet rs = new CierreMensualRS().ultimo();
        
        rs.next();
        
        cierreMensual.setMes( rs.getInt("mes") );
        cierreMensual.setAgno(rs.getInt("agno") );
        
          
        return cierreMensual;          
      
    }
                 
        
    
    /*
    public List<ServicioFijoDescuento>  search (Integer page, String busqueda) {
                
        List<ServicioFijoDescuento>  lista = null;
        
        try {                       
                        
            ServicioFijoDescuentoRS rs = new ServicioFijoDescuentoRS();
            lista = new Coleccion<ServicioFijoDescuento>().resultsetToList(
                    new ServicioFijoDescuento(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
*/

    
}
