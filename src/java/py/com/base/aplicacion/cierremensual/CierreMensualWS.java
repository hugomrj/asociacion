/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.aplicacion.cierremensual;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import py.com.base.aplicacion.ordencompra.OrdenCompraDAO;
import py.com.base.aplicacion.serviciofijodescuento.ServicioFijoDescuento;
import py.com.base.aplicacion.serviciotemporaldescuento.ServicioTemporalDescuentoDAO;
import py.com.base.configuracion.conceptoservicio.ConceptoServicio;


/**
 * REST Web Service
 * @author hugo
 */


@Path("cierremensual")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class CierreMensualWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new Gson();          
    private Response.Status status  = Response.Status.OK;
    
    CierreMensual com = new CierreMensual();       
                         
    public CierreMensualWS() {
    }

    
    
    
    @GET    
    public Response list ( 
            @HeaderParam("token") String strToken,
            @QueryParam("page") Integer page) {
        
            if (page == null) {                
                page = 1;
            }

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                CierreMensualDAO dao = new CierreMensualDAO(); 
                
                List<CierreMensual> lista = dao.list(page); 
                
                String json = gson.toJson( lista );     
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .header("total_registros", dao.total_registros )
                        .build();                       
            }
            else
            {

                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                             
                
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }      
    }    
    
      
    
    

    //  cierre 
    @POST
    public Response add( 
            @HeaderParam("token") String strToken,
            String json ) {
                
     
        
        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();  

System.err.println(json);                
                
                // tabla  cierre mensual

                Gson gsonf = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
                CierreMensual req = gsonf.fromJson(json, CierreMensual.class);                   

        
                JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);
                req.setMes(  jsonObject.get("sel_mes").getAsJsonObject().get ("sel_mes").getAsInt()   );                
                
                req.setFecha(  new Date() );
                                
                this.com = (CierreMensual) persistencia.insert( req );    
                
                
                // tabla servicios fijos
                // filtrar servicio  --  cuota socual   -- cod 2
                
                Integer cuotasocial = 2;
                
                ServicioFijoDescuento serviciofijodescuento = new ServicioFijoDescuento();
                
                ConceptoServicio serv = new ConceptoServicio();
                serv = (ConceptoServicio) persistencia.filtrarId( serv, cuotasocial );                  
                
                
                serviciofijodescuento.setMonto(  serv.getMonto_fijo() );                               
                serviciofijodescuento.setServicio(serv);
                serviciofijodescuento.setAgno(this.com.getAgno());
                serviciofijodescuento.setMes(this.com.getMes());
                serviciofijodescuento.setFecha(  new Date() );
                
                persistencia.insert(serviciofijodescuento);    
                
                
                
                
                // aca guardar los conceptos temporales
            
                ServicioTemporalDescuentoDAO temporal = new ServicioTemporalDescuentoDAO();
            
                temporal.procedimiento_cierre_servicio(this.com);
            
                
                
                
                // guardar  cuotas de ordenes de compras
                
                OrdenCompraDAO ocDAO = new OrdenCompraDAO();              
                ocDAO.cierreMensual(this.com.getMes(),  this.com.getAgno(), this.com.getId() );
                
                json = gson.toJson(this.com);    
                

                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;
                }
                
                return Response
                        .status(this.status)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else
            {                 
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();             
            }
        
        }     
        
        
        catch (SQLException ex) {   
            
            return Response
                    .status(Response.Status.BAD_GATEWAY)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();    
        }        
        
        
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    
 

        

    
}