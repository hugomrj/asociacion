/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.sociodescuento;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.util.FileXls;

/** 
 * @author hugo
 */
@WebServlet(name = "SocioDescuentoXLS", 
        urlPatterns = {"/socio/descuento.xls"})
public class SocioDescuentoXLS extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) 
            throws IOException, SQLException, Exception
 {
        //response.setContentType("text/html;charset=UTF-8");
        
     
            String strToken = "";
        
            strToken = request.getHeader("token");
            Autentificacion autorizacion = new Autentificacion();
            if (autorizacion.verificar(strToken))
            {  
            
        
                    Integer aa = 0;
                    Integer mm = 0;
                    Integer rl = 0;
                    Integer sue = 0;
                    
                    
                    aa = Integer.parseInt( request.getParameter("aa").toString() ) ;       
                    mm = Integer.parseInt( request.getParameter("mm").toString() ) ;       
                    rl = Integer.parseInt( request.getParameter("rl").toString() ) ;       
                    sue = Integer.parseInt( request.getParameter("sue").toString() ) ;       
     

                FileXls filexls = new FileXls();
                
                filexls.Iniciar(request);
                filexls.folder = "/files";                
                filexls.name = "/base.xls";                
        
                
                
                ArrayList<String> cabecera = new ArrayList<String>();
                cabecera.add("socio");
                cabecera.add("cedula");                
                cabecera.add("nombre_apellido");                
                cabecera.add("servicio_descripcion");              
                cabecera.add("credito");   
                cabecera.add("credito_saldo");                
                cabecera.add("monto_descuento");                
                cabecera.add("mes");                
                cabecera.add("agno");                                
                cabecera.add("relacion_laboral");                
                cabecera.add("direccion_sueldo");                
                filexls.setCabecera(cabecera);
        
                
                
                ArrayList<String> campos = new ArrayList<String>();
                campos.add("socio");
                campos.add("cedula");                
                campos.add("nombre_apellido");                
                campos.add("servicio_descripcion");                
                campos.add("credito");                
                campos.add("credito_saldo"); 
                campos.add("monto_descuento");                
                campos.add("mes");                
                campos.add("agno");                                         
                campos.add("relacion_laboral");                
                campos.add("direccion_sueldo");                
                filexls.setCampos(campos);                
                
                
                
                SocioDescuentoRS rs = new SocioDescuentoRS();     
                ResultSet resultset = rs.xls_all_relacion(aa, mm, rl, sue);
                
                               
                
                filexls.gen(resultset);                
                filexls.newFileStream();
                
                
                ServletContext context = getServletContext();
                

                response.setHeader("token", autorizacion.encriptar());
                
                filexls.getServeltFile(request, response, context);
                
                filexls.close();
                
                
                
            }
            else{   
                //System.out.println("no autorizado");                
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
            
            
            
            
                
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(SocioDescuentoXLS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
