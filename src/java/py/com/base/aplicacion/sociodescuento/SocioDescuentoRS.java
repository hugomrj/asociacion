/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.sociodescuento;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.ORM.sql.BasicSQL;
import nebuleuse.ORM.sql.SentenciaSQL;
import nebuleuse.ORM.xml.Global;

/**
 *
 * @author hugom_000
 */
public class SocioDescuentoRS {
    
        
        Conexion conexion = new Conexion();
        Statement  statement ;
        ResultSet resultset;          
        Integer lineas = Integer.parseInt(new Global().getValue("lineasLista"));
        public Integer total_registros = 0;
        
    
    public SocioDescuentoRS ( ) throws IOException, SQLException  {
        conexion.conectar();  
        statement = conexion.getConexion().createStatement();              
    }
   
    

    
    
    public ResultSet  xls_all ( Integer agno, Integer mes ) throws Exception {

            statement = conexion.getConexion().createStatement();      

            String sql = new SocioDescuentoSQL().xls_all( agno, mes);

            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }



    
    public ResultSet  xls_all_relacion ( Integer agno, Integer mes, Integer rl, Integer sue) 
            throws Exception {

            statement = conexion.getConexion().createStatement();      
            
            String sql = new SocioDescuentoSQL().xls_all_relacion( agno, mes, rl, sue);
            
            
            resultset = statement.executeQuery(sql);     
            conexion.desconectar();                
            return resultset;                 
            
    }
             
    
        
}
