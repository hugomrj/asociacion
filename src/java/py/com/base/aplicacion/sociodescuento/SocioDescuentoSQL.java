/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.sociodescuento;

import py.com.base.aplicacion.relacionlaboral.*;
import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class SocioDescuentoSQL {
    
    
    
    public String xls_all ( Integer agno, Integer mes )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("SocioDescuento");
        reader.fileExt = "xls_all.sql";
        
        sql = reader.get(  agno, mes);    
        
        return sql ;      
    }
    
    
    public String xls_all_relacion ( Integer agno, Integer mes, Integer rl, Integer sue)
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("SocioDescuento");
  //      reader.fileExt = "xls_all_relacion.sql";
        reader.fileExt = "xls_all_relacion_consolidado.sql";
  

        sql = reader.get(  agno, mes, rl, sue);    
        

        return sql ;      
    }
    
    
    
    
    
    
    
}
