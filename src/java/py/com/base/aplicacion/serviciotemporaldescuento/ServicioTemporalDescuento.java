/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.serviciotemporaldescuento;


import java.util.Date;
import py.com.base.configuracion.conceptoservicio.ConceptoServicio;

/**
 *
 * @author hugo
 */
public class ServicioTemporalDescuento {
    
    private Integer id;
    private ConceptoServicio servicio;
    private Date fecha;
    private Integer agno;
    private Integer mes;
    private Long monto;
    private Integer cuota;
    private Integer cuotas;

    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ConceptoServicio getServicio() {
        return servicio;
    }

    public void setServicio(ConceptoServicio servicio) {
        this.servicio = servicio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getAgno() {
        return agno;
    }

    public void setAgno(Integer agno) {
        this.agno = agno;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Long getMonto() {
        return monto;
    }

    public void setMonto(Long monto) {
        this.monto = monto;
    }

    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    public Integer getCuotas() {
        return cuotas;
    }

    public void setCuotas(Integer cuotas) {
        this.cuotas = cuotas;
    }
    
    
    
}
