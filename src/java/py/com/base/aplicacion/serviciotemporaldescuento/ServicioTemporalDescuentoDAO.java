/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.serviciotemporaldescuento;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;
import py.com.base.aplicacion.cierremensual.CierreMensual;
import py.com.base.aplicacion.serviciofijodescuento.ServicioFijoDescuento;
import py.com.base.aplicacion.sociodescuentoservicio.SocioDescuentoServicioDAO;
import py.com.base.configuracion.conceptoservicio.ConceptoServicio;
import py.com.base.configuracion.conceptoservicio.ConceptoServicioDAO;

/**
 *
 * @author hugom_000
 */
public class ServicioTemporalDescuentoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public ServicioTemporalDescuentoDAO ( ) throws IOException  {
    }
      
    
    public void procedimiento_cierre_servicio (  CierreMensual cierre ) throws Exception {
                
        
            Integer servicio_id = 4;

            ServicioTemporalDescuento temporal = new ServicioTemporalDescuento();

            
            ConceptoServicioDAO dao = new ConceptoServicioDAO();
            ConceptoServicio servicio = dao.verificacion_cantidad_actual(servicio_id);
            
            if (servicio != null ){


                temporal.setMonto(  servicio.getMonto_fijo() );                               
                temporal.setServicio(servicio);
                temporal.setAgno(cierre.getAgno());
                temporal.setMes(cierre.getMes());
                temporal.setFecha(  new Date() );
                
                temporal.setCuotas(servicio.getCantidad());
                temporal.setCuota( servicio.getActual() );
                
                
                temporal = (ServicioTemporalDescuento) persistencia.insert(temporal);    
    
                // aca falta cargar  socio descuento servicio dao
                 new SocioDescuentoServicioDAO().insertarLote(temporal.getId());
                
                
            }
            

            


        
        
        
    }      
      

    
    

    
}
