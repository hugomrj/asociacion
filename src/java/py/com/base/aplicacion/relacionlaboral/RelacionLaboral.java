/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.relacionlaboral;

/**
 *
 * @author hugom_000
 */
public class RelacionLaboral {

    
    private Integer relacion_laboral;
    private String descripcion;    

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getRelacion_laboral() {
        return relacion_laboral;
    }

    public void setRelacion_laboral(Integer relacion_laboral) {
        this.relacion_laboral = relacion_laboral;
    }

    
    
}
