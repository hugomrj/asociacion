/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.tipodescuento.gestionadministrativa;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Map;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.RegistroMap;
import py.com.base.aplicacion.socio.SocioRS;


public class GestionAdministrativaJSON  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public GestionAdministrativaJSON ( ) throws IOException  {
    
    }
      
    
    
    
    

    public JsonArray  extracto (Integer socio, Integer agno, Integer mes ) {
        
        Map<String, String> map = null;        
        JsonArray jsonarray = new JsonArray();      
        RegistroMap registoMap = new RegistroMap();     
        Gson gson = new Gson();               
        
        
        try 
        {   
            
            
            SocioRS rs = new SocioRS();            
            ResultSet resulset = rs.extracto(  socio, agno, mes );                
          
            
            
            
            while(resulset.next()) 
            {  
                map = registoMap.convertirHashMap(resulset);     
                JsonElement element = gson.fromJson(gson.toJson(map)  , JsonElement.class);        
                jsonarray.add( element );
            }                    
                        
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonarray ;         
        }
    }      
          
    

        
}
