/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.configuracion.tipodescuento;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class TipoDescuentoDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public TipoDescuentoDAO ( ) throws IOException  {
    }
      

    public List<TipoDescuento>  all () {
                
        List<TipoDescuento>  lista = null;        
        try {                        
                        
            TipoDescuentoRS rs = new TipoDescuentoRS();            
            lista = new Coleccion<TipoDescuento>().resultsetToList(
                    new TipoDescuento(),
                    rs.all()
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
    
    
    
    
    public List<TipoDescuento>  list (Integer page) {
                
        List<TipoDescuento>  lista = null;        
        try {                        
                        
            TipoDescuentoRS rs = new TipoDescuentoRS();            
            lista = new Coleccion<TipoDescuento>().resultsetToList(
                    new TipoDescuento(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    
    
    public List<TipoDescuento>  search (Integer page, String busqueda) {
                
        List<TipoDescuento>  lista = null;
        
        try {                       
                        
            TipoDescuentoRS rs = new TipoDescuentoRS();
            lista = new Coleccion<TipoDescuento>().resultsetToList(
                    new TipoDescuento(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
        
    
    
}






