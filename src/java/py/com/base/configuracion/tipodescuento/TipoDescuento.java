/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.configuracion.tipodescuento;

/**
 *
 * @author hugo
 */
public class TipoDescuento {
    
    private Integer tipo_descuento;
    private String descripcion;    

    public Integer getTipo_descuento() {
        return tipo_descuento;
    }

    public void setTipo_descuento(Integer tipo_descuento) {
        this.tipo_descuento = tipo_descuento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
}
