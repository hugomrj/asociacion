/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.configuracion.conceptoservicio;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nebuleuse.ORM.Coleccion;
import nebuleuse.ORM.Persistencia;

/**
 *
 * @author hugom_000
 */
public class ConceptoServicioDAO {
    

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public ConceptoServicioDAO ( ) throws IOException  {
    }
      
    
    public List<ConceptoServicio>  list (Integer page) {
                
        List<ConceptoServicio>  lista = null;        
        try {                        
                        
            ConceptoServicioRS rs = new ConceptoServicioRS();            
            
            
            lista = new Coleccion<ConceptoServicio>().resultsetToList(
                    new ConceptoServicio(),
                    rs.list(page)
            );                        
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    public List<ConceptoServicio>  search (Integer page, String busqueda) {
                
        List<ConceptoServicio>  lista = null;
        
        try {                       
                        
            ConceptoServicioRS rs = new ConceptoServicioRS();
            lista = new Coleccion<ConceptoServicio>().resultsetToList(
                    new ConceptoServicio(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    




    public List<ConceptoServicio>  tipo_all ( Integer tipo ) {
                
        List<ConceptoServicio>  lista = null;        
        try {                        
                        
            ConceptoServicioRS rs = new ConceptoServicioRS();            
            lista = new Coleccion<ConceptoServicio>().resultsetToList(
                    new ConceptoServicio(),
                    rs.tipo_all( tipo )
            );                        
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
          
        
    
    

    public ConceptoServicio  verificacion_cantidad_actual ( Integer servicio_id ) {

        ConceptoServicio servicio = new ConceptoServicio();
        
        try {                  
            
            servicio = (ConceptoServicio) persistencia.filtrarId( servicio, servicio_id );
        
            if (servicio.getCantidad()  > servicio.getActual() ){
    
                servicio.setActual( servicio.getActual() + 1);
                servicio = (ConceptoServicio) persistencia.update(servicio);

            }
            else{
                servicio = null;
            }
            
        
        } catch (Exception ex) {
            Logger.getLogger(ConceptoServicioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            return servicio;
        }
        
        
        
        
                  
        
    }      
          
    
    
    
    
    
    
    
    
}
