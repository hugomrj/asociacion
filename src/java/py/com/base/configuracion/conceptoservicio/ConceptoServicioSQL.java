/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.configuracion.conceptoservicio;

import nebuleuse.ORM.sql.ReaderT;
import nebuleuse.ORM.sql.SentenciaSQL;

/**
 *
 * @author hugom_000
 */
public class ConceptoServicioSQL {
    
        
    public String search ( String busqueda )
            throws Exception {
    
        String sql = "";                                 
        sql = SentenciaSQL.select(new ConceptoServicio(), busqueda);        
        
        return sql ;             
    }        
       
    
    
    public String tipo_all ( Integer tipo )
            throws Exception {
    
        String sql = "";                                 
        
        ReaderT reader = new ReaderT("ConceptoServicio");
        reader.fileExt = "all.sql";
        
        sql = reader.get(  tipo );    
        
        return sql ;      
    }
        
    
    
    
    
}
