/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.configuracion.conceptoservicio;

import py.com.base.configuracion.tipodescuento.TipoDescuento;


/**
 *
 * @author hugom_000
 */
public class ConceptoServicio {


    private Integer servicio;
    private String descripcion;
    private TipoDescuento tipo_descuento;
    private Long monto_fijo;
    private Integer cantidad;
    private Integer actual;

    public Integer getServicio() {
        return servicio;
    }

    public void setServicio(Integer servicio) {
        this.servicio = servicio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoDescuento getTipo_descuento() {
        return tipo_descuento;
    }

    public void setTipo_descuento(TipoDescuento tipo_descuento) {
        this.tipo_descuento = tipo_descuento;
    }

    public Long getMonto_fijo() {
        return monto_fijo;
    }

    public void setMonto_fijo(Long monto_fijo) {
        this.monto_fijo = monto_fijo;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getActual() {
        return actual;
    }

    public void setActual(Integer actual) {
        this.actual = actual;
    }


    
}
